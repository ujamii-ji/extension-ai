<!--
SPDX-FileCopyrightText: 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Feature list

This list may serve as general progress tracking for AI importing.

## High importance

Items and subitems are ordered by urgency.

- Coordinate systems, pages and other namedview properties
  - [ ] Correctly set viewbox and viewport attributes.
  - [ ] Write coordinate transformation functions to transform Canvas and Drawing coordinates into SVG user units
- Layers
  - [x] Layer name, locked / visible style
  - [x] Objects are created in the correct layer
- Paths (Bezier curves)
  - [ ] Correct node and handle positions relative to canvas
- Live shapes 
  - [ ] Convert live shapes to the equivalent SVG object, in roughly the same position
  - [ ] If the syntax is not as expected, fall back to just interpreting the path(s) of fill/stroke
- Styles
  - [ ] solid fill (RGB)
  - [ ] Linear and radial gradients
  - [ ] line style, except for markers and stroke offset
  - [ ] `NamedStyle`s with up to one fill and one stroke
- Raster images
  - [ ] Place embedded raster images in the right position
- Text
  - [ ] Text elements are created and contain the correct text
  - [ ] Text elements are placed in roughly the correct position

## Medium importances

Subitems are ordered by urgency.

- Coordinate systems, pages and other namedview properties
  - [ ] Pages
  - [ ] Guides (at least linear ones)
  - [ ] Grid
  - [ ] Full support for large-scale canvas
- Live shapes
  - [ ] Support all attributes of live shapes, all are rendered correctly
- Bezier Fallbacks
  - [ ] For all currently unknown objects, try to simply draw the bezier fallback: e.g. Live paint objects, Vector Brushes, LPEs
- Styles
  - [ ] `NamedStyle`s with more than one fill or more than one stroke
  - [ ] Non-Anon `NamedStyle`s are created as CSS classes and applied as such
  - [ ] midpoint of gradients
  - [ ] Markers
  - [ ] Patterns
  - [ ] Stroke offset
  - [ ] Blend modes
  - [ ] SVG filters
  - [ ] Set CMYK color profile (if possible)
  - [ ] Specify CMYK colors (incl. spot colors)
- Mesh gradients
  - [ ] Support basic mesh gradients
- Text 
  - [ ] "Default Paragraph / Character style" and other global styles are created as CSS classes
  - [ ] Font name / Style / Weight are parsed
  - [ ] Correct text element type is created
  - [ ] Text style is reasonably well approximated
- Raster images
  - [ ] Other attributes of raster images: scaling, cropping, style
- Object properties
  - [ ] ID, Name and Locked state are correctly set for all objects
- Clip / Mask
  - [ ] Clips and masks work on all implemented elements
- Symbols
  - [ ] Symbols are rendered in the correct position and with the correct style
- Path effects
  - [ ] Power stroke
  - [ ] Mirror effect
  - [ ] Radially tiled clones
  - [ ] Path with Mesh deformation
  - [ ] Path with curve deformation
- Document metadata
  - [ ] Author, title, copyright
## Low importance

In no particular order. These features are usually either tiny convenience features or a combination of hard to implement / rarely used.

- Coordinate systems, pages and other namedview properties
  - [ ] OpenToView 
- Paths 
  - [ ] `sodipodi:nodetypes` attribute
- Layers
  - [ ] Layer color
  - [ ] Layer expanded
- Styles
  - [ ] Split dash style for "Align dashes" attribute
  - [ ] Approximate gradient-on-stroke by mesh gradient
- Mesh gradients 
  - [ ] Split mesh gradients to support those with more than one path segment on its edge
- Path effects
  - [ ] Path with Mesh deformation
- Paths
  - [ ] κ-Curves: would need to be implemented as LPE first, and then probably reverse-engineered from the Bezier data
- Text
  - [ ] Old point text
  - [ ] Approximate multi-column / multi-row flowtexts