# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the grammar of the Adobe Illustrator format."""

import pytest
import pyparsing as pp
from inkai.errors import *


class SpecialMatch:
    """Special matches."""

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name


ERROR = SpecialMatch("ERROR")  # used to test that a result does not match
MATCH = SpecialMatch("MATCH")  # used to test that a result matches
PATH_SPEC_RECT_NO_GRADIENT = """0 g
268 631 m
268 649 L
245 649 L
245 631 L
268 631 L
B
"""  # from AI Specification page 73 - modified to not include the gradient
PATH_SPEC_CIRCLE_NO_GRADIENT = """0 g
237.5 636.4792 m
244.9198 636.4792 250.935 641.8612 250.935 648.5 c
250.935 655.1388 244.9198 660.5208 237.5 660.5208 c
230.0802 660.5208 224.065 655.1388 224.065 648.5 c
224.065 641.8612 230.0802 636.4792 237.5 636.4792 c
b
"""  # from AI Specification page 73 - modified to not include the gradient

"simple_ai_with_newlines_line_2601"
TEXT_EXAMPLE_2 = """0 To
1 0 0 1 940.207 -352.6484 0 Tp
TP
1 0 0 1 940.207 -352.6484 Tm
0 Tr
0 Tc
(\\167\\040\\155\\157\\155) Tx
1 0 Tk
TO
"""
TEXT_EXAMPLE_3 = """1 0 0 1 940.207 -352.6484 0 Tp
TP
1 0 0 1 940.207 -352.6484 Tm
0 Tr
0 Tc
(\\167\\040\\155\\157\\155) Tx
1 0 Tk
"""
TEXT_EXAMPLE_4 = """1 0 0 1 940.207 -352.6484 Tm
0 Tr
0 Tc
(\\167\\040\\155\\157\\155) Tx
1 0 Tk
"""


@pytest.mark.parametrize(
    "parser_name,string,expected_result",
    [
        (
            "document_structuring_convention",
            "%!PS-Adobe-3.0\n",
            [{"DSC": "3.0"}],
        ),  # from extension-ai/tests/data/ai$ grep -a -roE '%!PS-Adobe-.+'
        (
            "document_structuring_convention",
            "%!PS-Adobe-M EPSF-N\n",
            [{"DSC": "M EPSF-N"}],
        ),
        ("document_structuring_convention", "%!PS-Adobe-M\n", [{"DSC": "M"}]),
        ("header_comment_key", "%%Title:", ["Title"]),
        ("header_comment_key", "%AI5_FileFormat", ["FileFormat"]),
        ("header_comment_key", "%%AI8_CreatorVersion", ["CreatorVersion"]),
        # skip headers with + for now as we do not need them
        ("header_comment_plus", "%%+ procset Adobe_Illustrator_AI5 1.0 0\n", ["+"]),
        ("header_comment_plus", "%%+ Minion-Regular\n", ["+"]),
        ("header_comment_key", "%%EndComments\n", ERROR),
        (
            "header_comment_plus",
            "%%+ Options: 1 16 0 1 0 1 1 1 0 1 1 1 1 18 0 0 0 0 0 0 0 0 -1 -1\n",
            ["+"],
        ),
        ("header_comment_value", "100", ["100"]),
        ("header_comment_value", "7.0", ["7.0"]),
        ("header_comment_value", "(LayeredGradients.ai)", ["LayeredGradients.ai"]),
        (
            "text",
            "(LayeredGradients.ai)",
            ["LayeredGradients.ai"],
        ),
        ("header_comment_value", "(space in name.ai)", ["space in name.ai"]),
        (
            "text",
            "(space in name.ai)",
            ["space in name.ai"],
        ),
        ("header_comment_value", "100 99 88", ["100", "99", "88"]),
        (
            "header_comment_value",
            "1 1 (AI6 Default Color Separation Set)",
            ["1", "1", "AI6 Default Color Separation Set"],
        ),
        ("header_comment_value", "(5/9/96) (3:57 PM)", ["5/9/96", "3:57 PM"]),
        ("header_comment_value", "Color", ["Color"]),
        ("header_comment_value", "(12)", ["12"]),
        ("header_comment_value", "1x2", ["1x2"]),
        # Test that the header comments match
        ("header_comment", "%%For: Hello\n", [{"For": ["Hello"]}]),
        (
            "header_comment",
            "%%NoColon value1 value2\n",
            [{"NoColon": ["value1", "value2"]}],
        ),
        (
            "header_comment",
            "%%+ Minion-Regular\n",
            [{"+": []}],
        ),  # temporary: + headers are skipped
        ("header_comment", "%AI1_one_percent\n", [{"one_percent": []}]),
        ("header_comment", "%AI1_one_percent_space \n", [{"one_percent_space": []}]),
        ("header_comment", "%%AI12_TwoPercent: x\n", [{"TwoPercent": ["x"]}]),
        ("positive_int", " 12 12", ERROR),
        ("positive_int", " 12", ["12"]),
        ("positive_int", " 12.2", ERROR),
        ("int", "1", ["1"]),
        ("int", "+100000", ["+100000"]),
        ("int", "-1234567890", ["-1234567890"]),
        ("int", " 2-3", ERROR),
        ("float", "12.0", ["12.0"]),
        ("float", "-1234567890.1234567890", ["-1234567890.1234567890"]),
        ("float", "+22.22", ["+22.22"]),
        ("float", "+22.2+2", ERROR),
        ("float", "+22.2.2", ERROR),
        ("float", "-+22.22", ERROR),
        ("float", "-22..22", ERROR),
        ("float", "22. 22", ERROR),
        ("float", "-.002", ["-.002"]),  # page 35 PostScript
        ("float", "34.5", ["34.5"]),  # page 35 PostScript
        ("float", "-3.62", ["-3.62"]),  # page 35 PostScript
        ("float", "123.6e10", ["123.6e10"]),  # page 35 PostScript
        ("float", "1E-5", ["1E-5"]),  # page 35 PostScript
        ("float", "-1.", ["-1."]),  # page 35 PostScript
        ("float", "0.0", ["0.0"]),  # page 35 PostScript
        ("positive_number", "12", ["12"]),
        ("positive_number", "12.3", ["12.3"]),
        ("positive_number", "-12.3", ERROR),
        ("positive_number", "-3", ERROR),
        ("positive_number", "- 3", ERROR),
        ("number", " 333", ["333"]),
        ("number", " -12.43", ["-12.43"]),
        ("number", "1+1", ERROR),
        ("number", "+ 1", ERROR),
        ("zero_to_one", "-1", ERROR),
        ("zero_to_one", "-0", ERROR),
        ("zero_to_one", "0", ["0"]),
        ("zero_to_one", "1", ["1"]),
        ("zero_to_one", "0.", ["0."]),
        ("zero_to_one", "1.", ["1."]),
        ("zero_to_one", "1.000", ["1.000"]),
        ("zero_to_one", ".92", [".92"]),
        ("zero_to_one", "0.123", ["0.123"]),
        ("zero_to_one", "0 .123", ERROR),
        ("color", "0.3 g\n", MATCH),
        ("color", "0.3 G\n", MATCH),
        ("color", "0.1 0.2 0.3 0.4 k\n", MATCH),
        ("color", "0.1 0.2 0.3 0.4 K\n", MATCH),
        ("color", "0.45 0 0.25 0 (PANTONE 570 CV) 0 x\n", MATCH),  # page 60
        ("color", "0.1 0.2 0.3 0.4 color 0.5 X\n", MATCH),
        ("color", "0.1 0.2 0.3 Xa\n", MATCH),
        ("color", "0.1 0.2 0.3 XA\n", MATCH),
        ("color", "0.1 0.2 0.3 my-color 0.4 1 Xx\n", MATCH),
        ("color", "0.1 0.2 0.3 (rgb color) 0.4 1 XX\n", MATCH),
        ("color", "0.1 0.2 0.3 0.4(cmyk color) 0.5 0 Xx\n", MATCH),
        ("color", "0.1 0.2 0.3 0.4 (cmyk color) 0.5 0 XX\n", MATCH),
        ("color_name", "color", ["color"]),
        ("color_name", "my-color", ["my-color"]),
        ("color_name", "(my color with space)", ["my color with space"]),
        ("text", "Thisisatextstring", ["Thisisatextstring"]),  # page 36 PostScript
        (
            "text",
            "(This is a text string with spaces)",
            ["This is a text string with spaces"],
        ),  # page 36 PostScript
        (
            "text",
            "(This is a text string (with parentheses))",
            ["This is a text string (with parentheses)"],
        ),  # page 36 PostScript
        (
            "text",
            "(This is a special character \\262 using the \\\\ mechanism)",
            ["This is a special character \u0106 using the \\ mechanism"],
        ),  # page 36 PostScript
        ("text", "(\\r)", ["\r"]),
        ("text", "a()", ["a()"]),
        ("text", "()b", ["()b"]),
        ("text", "c(x)d", ["c(x)d"]),
        ("text", "()", [""]),
        ("path_object", PATH_SPEC_RECT_NO_GRADIENT, MATCH),
        ("path_object", PATH_SPEC_CIRCLE_NO_GRADIENT, MATCH),
        (
            "object_tag",
            "/kAISpecialPathTag (Segment 1) XT\n",
            ["kAISpecialPathTag", "Segment 1"],
        ),
        ("object_tag", "/clock_minute_hand () XT\n", ["clock_minute_hand", ""]),
        (
            "procset_init",
            "Adobe_packedarray /initialize get exec\n",
            ["Adobe_packedarray"],
        ),
        ("procset_init", "1 2 3 4 5 /initialize get exec\n", ["1", "2", "3", "4", "5"]),
        ("procset", "1 2 3 4 5 /initialize get exec\n", ["1", "2", "3", "4", "5"]),
        (
            "procset",
            "%%IncludeResource: procset Adobe_cmykcolor 1.1 0\n",
            ["Adobe_cmykcolor", "1.1", "0"],
        ),
        ("skip_line", "asd\n", MATCH),
        ("skip_line", "\r\n", MATCH),
        ("newline", "\r", MATCH),
        ("newline", "\r\n\n\r", MATCH),
        ("encoding_pair", "39/quotesingle", MATCH),
        ("encoding_pairs", "130/quotesinglbase/florin/quotedblbase/ellipsis\n", MATCH),
        (
            "encoding_pairs",
            "172/logicalnot/hyphen/registered/macron/ring\n/plusminus/twosuperior/threesuperior/acute/mu 183/periodcentered/cedilla\n",
            MATCH,
        ),
        ("font_type", "AdobeType", MATCH),
        ("font_type", "TrueType", MATCH),
        (
            "re_encoding",
            "%AI3_BeginEncoding: _MyriadPro-Regular MyriadPro-Regular\n[/_MyriadPro-Regular/MyriadPro-Regular 0 0 1 TZ\n%AI3_EndEncoding AdobeType\n",
            MATCH,
        ),
        (
            "pattern",
            "%AI3_BeginPattern: (no vegetation)\n... todo ...\n%AI3_EndPattern\n",
            MATCH,
        ),
        ("object_locking", "0 A\n", ["0"]),
        ("paint_style", "0.911711 0.786862 0.619532 0.974487 k\n", MATCH),
        (
            "path_mask",
            "0.911711 0.786862 0.619532 0.974487 k\n1378 -420 m\n1378 -788 L\n681 -788 L\n681 -420 L\n1378 -420 L\nF\n",
            MATCH,
        ),
        (
            "path_geometry",
            "1378 -420 m\n1378 -788 L\n681 -788 L\n681 -420 L\n1378 -420 L\n",
            MATCH,
        ),
        ("path_render", "F\n", MATCH),
        ("path_render", "b\n", MATCH),
        ("paint_style", "0 g\n", MATCH),
        ("overprint", "0 O\n", ["0"]),
        ("overprint", "1 R\n", ["1"]),
        ("paint_style", "1 R\n", MATCH),
        (
            "object",
            "0 O\n0 0 0 0 k\n0 J 0 j 1 w 10 M []0 d\n680.5 -788.5 m\n680.5 -419.5 L\n1378.5 -419.5 L\n1378.5 -788.5 L\n680.5 -788.5 L\nf\n",
            MATCH,
        ),
        ("path_attributes", "0 J 0 j 1 w 10 M []0 d\n", MATCH),
        ("path_attributes", "0 J 0 j 1 w 4 M []0 d\n", MATCH),
        ("text_operator", "jashdkhasdjThk jd T", MATCH),
        ("To_operator", "0 To\n", MATCH),
        ("Tp_operator", "1 0 0 1 940.207 -352.6484 0 Tp\n", MATCH),
        ("TP_operator", "TP\n", MATCH),
        ("Tm_operator", "1 0 0 1 940.207 -352.6484 Tm\n", MATCH),
        ("Tr_operator", "0 Tr\n", MATCH),
        ("Tc_operator", "0 Tc\n", MATCH),
        ("Tk_operator", "(\\167\\040\\155\\157\\155) Tx 1 0 Tk\n", MATCH),
        ("TO_operator", "TO\n", MATCH),
        ("Xu_operator", "jhkjh Xu\n", MATCH),
        ("text_object", TEXT_EXAMPLE_2, MATCH),
        ("text_at_a_point", TEXT_EXAMPLE_3, MATCH),
        ("text_runs", TEXT_EXAMPLE_4, MATCH),
        ("proc_set_termination", "Adobe_Illustrator_AI3 /terminate get exec\r", MATCH),
        (
            "page_trailer",
            "%%PageTrailer\ngsave annotatepage grestore showpage\n",
            MATCH,
        ),
        (
            "document_trailer",
            "%%Trailer\nAdobe_Illustrator_AI3 /terminate get exec\nAdobe_pattern_AI3 /terminate get exec\n",
            MATCH,
        ),
        (
            "path_object",
            "0.911711 0.786862 0.619532 0.974487 k\n1378 -420 m\n1378 -788 L\n681 -788 L\n681 -420 L\n1378 -420 L\nF\n",
            MATCH,
        ),
        (
            "path_object",
            "1 D\n1379 -419 m\n680 -419 L\n680 -789 L\n1379 -789 L\n1379 -419 L\n1379 -419 L\nf\n",
            MATCH,
        ),
        (
            "path_object",
            "0 O\n0 0 0 0 k\n0 J 0 j 1 w 10 M []0 d\n680.5 -788.5 m\n680.5 -419.5 L\n1378.5 -419.5 L\n1378.5 -788.5 L\n680.5 -788.5 L\nf\n",
            MATCH,
        ),
        (
            "path_mask",
            "0 O\n0 0 0 0 k\n0 J 0 j 1 w 10 M []0 d\n680.5 -788.5 m\n680.5 -419.5 L\n1378.5 -419.5 L\n1378.5 -788.5 L\n680.5 -788.5 L\nf\n",
            MATCH,
        ),
        ("undocumented_procset", "userdict /_useSmoothShade false put\r\n", MATCH),
        ("undocumented_procset", "%userdict /_useSmoothShade false put\r\n", ERROR),
        ("undocumented_procset", "0 AE\r\n", ERROR),
        ("unknown_script_header", "%BeginLayer\r\n", ERROR),
        ("data_hierarchy_section", "%_/ArtDictionary :\n", MATCH),
        (
            "data_hierarchy_section",
            "%_/XMLUID : (Sublayer_1_x5F_2) ; (AI10_ArtUID) ,\n",
            MATCH,
        ),
        ("data_hierarchy_section", "%_;\n", MATCH),
        ("data_hierarchy_section", "%_\n", MATCH),
        ("data_hierarchy_section", "%_\n%_\n", MATCH),
        ("data_hierarchy_section", "%_; (xmlnode-attributes) ,\n", MATCH),
        ("data_hierarchy_section", "%_1 /Int (xmlnode-nodetype) ,\n", MATCH),
        ("data_hierarchy_section", "%_(base) /String (xmlnode-nodevalue) ,\n", MATCH),
        ("data_hierarchy_section", "%_; (id) ,\n", MATCH),
        (
            "data_hierarchy_section",
            "%_(SVG_ThisElement) /String (xmlnode-nodename) ,\n",
            MATCH,
        ),
        ("unkown_binary_operator", "1 AE\n", MATCH),
        ("object", "1 A\n0 Xw\n", MATCH),
        ("object", "0 AE\n", MATCH),
        ("object", "LB\n", ERROR),
        ("object", "%AI5_BeginLayer\n", ERROR),
        ("unspecified_object_operator", "0 Xw\nLB\n", ERROR),
        ("unspecified_object_operator", "9 () XW\n", MATCH),
        ("data_hierarchy_section", "0 Xw\n", ERROR),
        ("skip_line", "\r\n\r\n", ERROR),
        ("undocumented_paint_operator", "0 XR\n", MATCH),
    ],
)
@pytest.mark.filterwarnings("ignore::inkai.errors.NotWorthImplementingWarning")
def test_valid_case(ai, string, parser_name, expected_result):
    """Parse a few cases and see that the result is clean and matches."""
    parser = ai[parser_name]
    try:
        result = parser.parse_string(string, parse_all=True).as_list()
    except pp.ParseException:
        if expected_result is not ERROR:
            print(f"{parser} -> {string[:100]}")
            raise
    else:
        assert (
            expected_result is MATCH or result == expected_result
        ), f'{parser} parsing "{string}"'


@pytest.mark.parametrize(
    "parser_name,file_name,expected_result",
    [
        ("object", "simple_ai_with_newlines_line_2571", MATCH),
        ("object", "simple_ai_with_newlines_line_2572", MATCH),
        ("composite_object", "simple_ai_with_newlines_line_2572", MATCH),
        ("group_object", "simple_ai_with_newlines_line_2572", MATCH),
        ("compound_path", "simple_ai_with_newlines_line_2582", MATCH),
        ("composite_object", "simple_ai_with_newlines_line_2582", MATCH),
        #        ("script_body", "ai_spec_page_104", MATCH),
        ("encoding_pairs", "many_encoding_pairs", MATCH),
        ("text_object", "simple_ai_with_newlines_line_2601", MATCH),
        ("header_comment_data_hex", "header_data_hex", MATCH),
        ("resource", "procset_begin_resource", MATCH),
        ("procset", "procset_begin_resource", MATCH),
        ("procset", "procset_begin_procset", MATCH),
        ("layer", "one_layer", MATCH),
        ("layer", "nested_layers", MATCH),
        ("data_hierarchy_section", "XML_definitions", MATCH),
        ("data_hierarchy_section", "document_data", MATCH),
        ("parsed_data_hierarchy_section", "document_data", MATCH),
    ],
)
@pytest.mark.filterwarnings("ignore::inkai.errors.NotWorthImplementingWarning")
def test_valid_case_from_data_file(data, ai, parser_name, file_name, expected_result):
    """Parse a few cases and see that the result is clean and matches."""
    string = data.grammar[file_name].read(False)
    test_valid_case(ai, string, parser_name, expected_result)


# This is the header from the simple_cs.ai
HEADER_SIMPLE_CS = "header_simple_cs"
# This is the example header from the specification, page 15
HEADER_AI6_SPEC = "header_specification_page_15"


@pytest.mark.parametrize(
    "header_name,key,expected_value",
    [
        (HEADER_SIMPLE_CS, "Creator", ["Adobe", "Illustrator(R)", "11.0"]),
        (HEADER_SIMPLE_CS, "For", ["Adam Belis", ""]),
        (HEADER_SIMPLE_CS, "Canvassize", ["16383"]),
        (HEADER_SIMPLE_CS, "BoundingBox", ["595", "-789", "1444", "-219"]),
        (
            HEADER_SIMPLE_CS,
            "DocumentProcessColors",
            "Cyan Magenta Yellow Black".split(),
        ),
        (HEADER_SIMPLE_CS, "FileFormat", ["7.0"]),
        (HEADER_SIMPLE_CS, "NumLayers", ["1"]),
        (HEADER_AI6_SPEC, "NumLayers", ["2"]),
        (HEADER_AI6_SPEC, "TemplateBox", ["306", "396", "306", "396"]),
        (HEADER_AI6_SPEC, "For", ["John Doe", "John Doe Design"]),
        (HEADER_AI6_SPEC, "FileFormat", ["2.0"]),
        (
            HEADER_AI6_SPEC,
            "ColorSeparationSet",
            ["1", "1", "AI6 Default Color Separation Set"],
        ),
    ],
)
def test_header_contains(ai, data, header_name, key, expected_value):
    """Check that the parsed header contains the elements"""
    header_string = data.grammar[header_name].read(binary=False)
    header = ai.header.parse_string(header_string)[0]
    assert key in header, f"Expecting {key} to be one of: {list(header)}."
    value = header[key]
    assert (
        expected_value == value
    ), f"Expecting value {expected_value} for key {key}, not {value}."


@pytest.mark.parametrize(
    "parser_name,name",
    [
        ("todo", "todo"),
        ("zero_to_one", "zero_to_one"),
    ],
)
def test_names_of_parsers(ai, parser_name, name):
    """parsers should have the right name when debugging"""
    parser = ai[parser_name]
    assert parser.name == name


AI_SPEC_EXAMPLE_PATTERN_PAGE_36 = """%AI3_BeginPattern: (no vegetation)
(no vegetation) 6.4 6.4 113.5 103 [
%AI3_Tile
(0 O 0 R 0.06 0.09 0.23 0 (PANTONE 468 CV) 0 x 0.06 0.09 \
0.23 0 (PANTONE 468 CV) 0 X) @
(
%AI6_BeginPatternLayer
800 Ar
0 J 0 j 3.6 w 4 M []0 d
%AI3_Note:
0 D
0 XR
111.7 8.2 m
111.7 101.2 L
8.2 101.2 L
8.2 8.2 L
111.7 8.2 L
f
%AI6_EndPatternLayer
) &
(0 O 0 R 0.18 0.3 0.56 0 (PANTONE 465 CV) 0 x 0.18 0.3 0.56 \
0 (PANTONE 465
CV) 0 X) @
(
%AI6_BeginPatternLayer
800 Ar
0 J 0 j 3.6 w 4 M []0 d
%AI3_Note:
0 D
0 XR
111.7 8.2 m
111.7 101.2 L
8.2 101.2 L
8.2 8.2 L
111.7 8.2 L
s
%AI6_EndPatternLayer
) &
] E
%AI3_EndPattern
"""


@pytest.mark.parametrize(
    "parser,string,warning_type,match",
    [
        (
            "text_style",
            "warning! Tr\n",
            NotWorthImplementingWarning,
            "Text operators are deprecated, so we did not implement them. If you think, you need them, please open an issue!",
        ),
        (
            "pattern",
            AI_SPEC_EXAMPLE_PATTERN_PAGE_36,
            NotWorthImplementingWarning,
            "Patterns are not implemented, yet.",
        ),
    ],
)
def test_send_one_warning_that_this_is_not_going_to_be_implemented(
    ai, parser, string, warning_type, match
):
    """Capture a few warnings.

    See also https://docs.pytest.org/en/7.1.x/how-to/capture-warnings.html
    """
    with pytest.warns(warning_type, match=match) as record:
        ai[parser].parse_string(string)
    assert len(record) == 1
