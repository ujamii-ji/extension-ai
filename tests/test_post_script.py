# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Test the Post Script parsing."""
import pytest
from inkai.parse_actions import PostScript


@pytest.mark.parametrize(
    "ps_text,python_text",
    [
        ("\\\\", "\\"),
        ("\\262", "\u0106"),
    ],
)
def test_escape_mechanism(ps_text, python_text):
    """The escape mechanism is mentioned on page 36 of the PostScript specification.

        (This is a special character \\262 using the \\ mechanism)

    Several escapes are possible. We need to check and test with the "text" in grammar.py.
    """
    text = PostScript.unescape_text(ps_text)
    assert text == python_text
