# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test parsing and generating paths."""
import pytest
import inkex


PATH_SPEC_RECT_NO_GRADIENT = """268 631 m
268 649 L
245 649 L
245 631 L
268 631 L
B
"""  # from AI Specification page 73 - modified to not include the gradient
PATH_SPEC_CIRCLE_NO_GRADIENT = """237.5 636.4792 m
244.9198 636.4792 250.935 641.8612 250.935 648.5 c
250.935 655.1388 244.9198 660.5208 237.5 660.5208 c
230.0802 660.5208 224.065 655.1388 224.065 648.5 c
224.065 641.8612 230.0802 636.4792 237.5 636.4792 c
b
"""  # from AI Specification page 73 - modified to not include the gradient


@pytest.mark.parametrize(
    "token,ai_path_string,svg_path_string",
    [
        ("move_to", "237.5 636.4792 m", "M 237.5 636.4792"),
        ("move_to", "268 631 m", "M 268 631"),
        ("path_operator", "268 649 L", "L 268 649"),
        ("path_operator", "225 200 L", "L 225 200"),
        (
            "path_operator",
            "224.065 641.8612 230.0802 636.4792 237.5 636.4792 c",
            "C 224.065 641.8612 230.0802 636.4792 237.5 636.4792",
        ),
    ],
)
def test_svg_path_attribute_d(ai, token, ai_path_string, svg_path_string):
    """This checks the path after some actions have run."""
    ai[token].parse_string(ai_path_string + "\n")
    assert ai.actions.path.d == svg_path_string


@pytest.mark.parametrize(
    "token,ai_path_string,svg_path_string",
    [
        ("move_to", "237.5 636.4792 m", "s"),
        ("move_to", "268 631 m", "s"),
        ("path_operator", "268 649 L", "c"),
        ("path_operator", "225 200 l", "s"),
        ("path_operator", "224.065 641.8612 230.0802 636.4792 237.5 636.4792 c", "s"),
    ],
)
def test_svg_path_attribute_sodipodi_nodetypes(
    ai, token, ai_path_string, svg_path_string
):
    """This checks the path after some actions have run."""
    ai[token].parse_string(ai_path_string + "\n")
    assert ai.actions.path.sodipodi_nodetypes == svg_path_string


@pytest.mark.parametrize(
    "ai_path,svg_path",
    [
        (
            PATH_SPEC_RECT_NO_GRADIENT,
            b'<path d="M 268 631 L 268 649 L 245 649 L 245 631 L 268 631" sodipodi:nodetypes="scccc"/>',
        ),
    ],
)
def test_convert_path_from_AI_to_SVG(ai, ai_path, svg_path):
    """Create an SVG path from AI path data."""
    result = ai.path_object.parse_string(ai_path)
    print(result)
    path = result[0]
    path_xml = path.tostring()
    print(f"path_xml = {path_xml}")
    assert path_xml == svg_path


def test_path_operator_N():
    """The path operator is described on page 54 AI Spec."""
    pytest.skip("TODO")


def test_path_operator_n():
    """The path operator is described on page 54 AI Spec."""
    pytest.skip("TODO")


def test_path_operator_gradient_instance():
    """The path operator is described on page 54 AI Spec."""
    pytest.skip("TODO")


def test_path_operator_F():
    """The path operator is described on page 54 AI Spec."""
    pytest.skip("TODO")


PATH_WITHOUT_OPERATOR = """37.7537 26.5107 m
33.0604 26.233 28.0377 22.8297 32.0115 19.4736 C
35.6989 16.2855 41.2802 20.1701 43.074 24.6533 C
41.9529 26.1165 39.887 26.6369 37.7537 26.5107 c
"""


@pytest.mark.parametrize("close_path_operator", ["N", "f", "s", "b"])
def test_path_operator_closing(ai, close_path_operator):
    """Close the path.

    The path operators are described on page 54 AI Spec.
    """
    text = PATH_WITHOUT_OPERATOR + close_path_operator + "\n"
    print(f"AI:\n{text}")
    path_element: inkex.PathElement = ai.path_object.parse_string(text)[-1]
    path = path_element.get_path()
    last_command = path[-1]
    print(f"path = {path}")
    assert isinstance(last_command, inkex.paths.ZoneClose)


def test_path_operator_f():
    """The path operator is described on page 54 AI Spec."""
    pytest.skip("TODO")


def test_path_operator_S():
    """The path operator is described on page 54 AI Spec."""
    pytest.skip("TODO")


def test_path_operator_s():
    """The path operator is described on page 54 AI Spec."""
    pytest.skip("TODO")


def test_path_operator_B():
    """The path operator is described on page 54 AI Spec."""
    pytest.skip("TODO")


def test_path_operator_b():
    """The path operator is described on page 54 AI Spec."""
    pytest.skip("TODO")


@pytest.mark.skip("TODO")
def test():
    """This needs testing:

    - Several Paths after another do not take state from others.
    """
