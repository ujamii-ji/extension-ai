# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the mapping of AI colors to SVG."""
from inkai.color import Color
import pytest


@pytest.mark.parametrize(
    "gray,rgb",
    [
        (1, "#ffffff"),
        (0, "#000000"),
        (1.0, "#ffffff"),
        (0.0, "#000000"),
        (0.5, "#7f7f7f"),
    ],
)
def test_gray(gray, rgb):
    """Map gray values.

    See page 60 AI Spec
    """
    assert str(Color.from_gray(gray).to_rgb()) == rgb


@pytest.mark.parametrize(
    "cmyk,rgb",
    [
        ((1, 1, 1, 1), "#000000"),
        ((1, 1, 0, 0), "#0000ff"),
        ((1, 0, 0, 0), "#00ffff"),
        (
            (0.14, 0, 0.7, 0.58),
            "#476b00",
        ),  # TODO: See https://gitlab.com/inkscape/extras/extension-ai/-/issues/17
    ],
)
def test_cmyk(cmyk, rgb):
    """Map cmyk values.

    See page 60 AI Spec
    You can use Inkscape to add more values.
    """
    assert str(Color.from_cmyk(*cmyk).to_rgb()) == rgb


@pytest.mark.parametrize(
    "cmyk,rgb,alpha",
    [
        ((1, 1, 1, 1, "transparent", 0), "#000000", 0),
        ((1, 1, 0, 0, "transparent blue", 0.58), "#0000ff", 0.58),
        ((1, 0, 0, 0, "xyz", 0.71), "#00ffff", 0.71),
    ],
)
def test_cmyk_named_tinted(cmyk, rgb, alpha):
    """Map cmyk values.

    See page 61 AI Spec
    """
    color = Color.from_cmyk(*cmyk)
    assert str(color.to_rgb()) == rgb
    assert color.name == cmyk[4]
    assert color.alpha == alpha


@pytest.mark.parametrize(
    "ai,rgb",
    [
        ((1, 1, 1), "#ffffff"),
        ((1, 0.5, 0), "#ff7f00"),
        ((0.5, 0.5, 0.5), "#7f7f7f"),
        ((0, 0, 0), "#000000"),
    ],
)
def test_rgb(ai, rgb):
    """Map rgb values.

    See page 61 AI Spec
    You can use Inkscape to add more values.
    """
    assert str(Color.from_rgb(*ai).to_rgb()) == rgb


@pytest.mark.parametrize(
    "rgbna,rgb",
    [
        ((1, 1, 1, "white", 0), "#ffffff"),
        ((1, 0.5, 0, "color", 1), "#ff7f00"),
        ((0.5, 0.5, 0.5, "xyz", 0.3), "#7f7f7f"),
        ((0, 0, 0, "name", 0.1), "#000000"),
    ],
)
def test_rgba(rgbna, rgb):
    """Map rgb values.

    See page 61 AI Spec
    You can use Inkscape to add more values.
    """
    color = Color.from_rgb(*rgbna)
    assert str(color.to_rgb()) == rgb
    assert color.name == rgbna[3]
    assert color.alpha == rgbna[4]


@pytest.mark.parametrize(
    "color_string,rgb,alpha,name,is_stroke",
    [
        ("0 g", "#000000", 1, None, False),
        ("1.0000 G", "#ffffff", 1, None, True),
        ("1 1 0 0 k", "#0000ff", 1, None, False),
        ("0 0 0 1 K", "#000000", 1, None, True),
        ("0 0 0 1 (color name) 1 x", "#000000", 1, "color name", False),
        ("0 0 0 1 black 0.3 X", "#000000", 0.3, "black", True),
        ("1 0 1 Xa", "#ff00ff", 1, None, False),
        ("0 0.5 0 XA", "#007f00", 1, None, True),
        ("0 0 0 1 urx .4 0 Xx", "#000000", 0.4, "urx", False),
        ("0 0.5 0 () 0.3 1 XX", "#007f00", 0.3, "", True),
        ("0 0.5 0 (name) 0.3 1 XX", "#007f00", 0.3, "name", True),
    ],
)
def test_parse_colors(ai, color_string, rgb, alpha, name, is_stroke):
    """Parse the colors and convert them into our inkex."""
    color = ai.color.parse_string(color_string + "\n")[0]
    print(repr(color))
    print("color_name:", ai.color_name.resultsName)
    if name == "":
        pytest.skip(
            "TODO: Wait until https://github.com/pyparsing/pyparsing/issues/470 is resolved."
        )
    assert color.is_stroke() == is_stroke
    assert color.name == name
    assert str(color.to_rgb()) == rgb
    assert color.alpha == alpha
