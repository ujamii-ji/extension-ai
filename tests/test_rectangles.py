# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This adds tests for recognizing rectangles.

See https://gitlab.com/inkscape/extras/extension-ai/-/issues/27
"""

import pytest
import inkex
import math


@pytest.mark.parametrize(
    "file",
    [
        "rounded-1-different",
        "rounded-2-different",
        "rounded-all-different",
        "rounded-equal-rotated",
        "rounded-equal",
    ],
)
def test_there_is_only_a_rectangle_in_the_layer(file, rect):
    rectangle = rect(file).rect
    assert (rectangle.tag_name == "rect") ^ (
        rectangle.get("sodipodi:type") == "rect"
    ), "This is either a rect or a path looking like a rect"


@pytest.mark.parametrize(
    "file,angle",
    [
        ("rounded-1-different", 0),
        ("rounded-2-different", 0),
        ("rounded-all-different", 0),
        ("rounded-equal-rotated", 5.38268506183307),
        ("rounded-equal", 0),
    ],
)
def test_rotation(file, angle, rect):
    """Test the rotation of the file.

    AI uses radial, we use degrees.
    """
    rectangle = rect(file).rect
    deg = math.degrees(angle)
    if deg == 0:
        assert rectangle.get("transform") == None
    else:
        assert rectangle.get("transform") == inkex.Transform(f"rotate({deg})")


@pytest.mark.parametrize(
    "file,expected_tag_name",
    [
        ("90-deg-corners", "rect"),
        ("rounded-1-different", "path"),
        ("rounded-2-different", "path"),
        ("rounded-all-different", "path"),
    ],
)
def test_expected_tag_name(rect, file, expected_tag_name):
    """Check width and height of the rectangle."""
    assert rect(file).rect.tag_name == expected_tag_name


@pytest.mark.parametrize(
    "file,width,height",
    [
        ("90-deg-corners", 244, 180),
        ("rounded-equal", 230, 180),
    ],
)
def test_rect_width_and_height(rect, file, width, height):
    """Check width and height of the rectangle."""
    r = rect(file).rect
    assert r.tag_name == "rect"
    assert float(r.get("height", 0)) == height
    assert float(r.get("width", 0)) == width


@pytest.mark.parametrize(
    "file,rx,ry",
    [
        ("90-deg-corners", 0, 0),
        ("rounded-equal", 0, 47.5),
    ],
)
def test_rect_radius(rect, file, rx, ry):
    """Check the radius of the rectangle."""
    r = rect(file).rect
    assert r.tag_name == "rect"
    assert float(r.get("rx", 0)) == rx
    assert float(r.get("ry", 0)) == ry


@pytest.mark.parametrize(
    "file,x,y",
    [
        ("90-deg-corners", 174, -308),
        ("rounded-equal", 30, -218),
    ],
)
def test_rect_position(rect, file, x, y):
    """Check the x/y position of the rectangle.

    The SVG transform is not applied, yet.
    """
    r = rect(file).rect
    assert r.tag_name == "rect"
    assert float(r.get("x")) == x
    assert float(r.get("y")) == y


def test_fill_and_stroke_of_the_rect():
    pytest.skip("TODO")
    assert False, "The color and stroke must be transferred."


@pytest.mark.parametrize(
    "file",
    [
        "rounded-1-different",
        "rounded-2-different",
        "rounded-all-different",
    ],
)
def test_irregular_rectangle_has_path_effect(rect, file):
    """The rectangle has a path effect."""
    r = rect(file).rect
    assert r.get("inkscape:path-effect") == "#rectangle-path-effect-1"


@pytest.mark.parametrize(
    "file,radius",
    [
        ("rounded-1-different", ("81.0", "47.5", "47.5", "47.5")),
        ("rounded-2-different", ("81.0", "47.5", "18.5", "47.5")),
        ("rounded-all-different", ("81.0", "47.5", "18.5", "98.99982")),
    ],
)
def test_path_effect_parameters(rect, file, radius):
    """Check the parameters of the path effects.

    <inkscape:path-effect
       effect="fillet_chamfer"
       id="path-effect4262"
       is_visible="true"
       lpeversion="1"
       satellites_param="F,0,0,1,0,12.93932,0,1 @ F,0,0,1,0,19.098553,0,1 @ F,0,0,1,0,0,0,1 @ F,0,0,1,0,32.144292,0,1"
       unit="px"
       method="auto"
       mode="F"
       radius="0"
       chamfer_steps="1"
       flexible="false"
       use_knot_distance="true"
       apply_no_radius="true"
       apply_with_radius="true"
       only_selected="false"
       hide_knots="false" />
    """
    p = rect(file).path_effect
    assert p.tag_name == "inkscape:path-effect"
    assert p.get_id() == "rectangle-path-effect-1"
    properties = dict(
        effect="fillet_chamfer",
        is_visible="true",
        lpeversion="1",
        satellites_param=f"F,0,0,1,0,{radius[0]},0,1 @ F,0,0,1,0,{radius[1]},0,1 @ F,0,0,1,0,{radius[2]},0,1 @ F,0,0,1,0,{radius[3]},0,1",
        unit="px",
        method="auto",
        mode="F",
        radius="0",
        chamfer_steps="1",
        flexible="false",
        use_knot_distance="true",
        apply_no_radius="true",
        apply_with_radius="true",
        only_selected="false",
        hide_knots="false",
    )
    for key, value in properties.items():
        assert (
            p.get(key) == value
        ), f"path_effect.set({repr(key)}, {repr(value)}):\n    {repr(value)}\n == {repr(p.get(key))}"


def test_todo_check_the_rotation():
    """Path effects may operate on unrotated objects.

    Make sure that the rotation is done in a way that allows
    the path effect to work.
    Maybe we need to roatate the path around its centre before.

    <path
       sodipodi:type="rect"
       inkscape:path-effect="#path-effect4262"
       ...
       transform="rotate(23.48139)" />
    """
    pytest.skip("TODO")
