# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the parse actions alone and in conjuction with the BaseParser."""
import pytest
from pyparsing import ParseResults
from inkai.modular_parser import Parser as ParserBase
from inkai.modular_parser import ParseActions as ParseActionsBase
import inkai.modular_parser.errors as error


@pytest.mark.parametrize("name", ["t1", "t3"])
def test_attach_parse_actions(name, test_parse_actions, mock):
    test_parse_actions.attach_to(name, mock)
    mock.set_parse_action.assert_called()


@pytest.mark.parametrize("name", ["_t1", "T3", "nonexistent_action"])
def test_attach_parse_actions(name, test_parse_actions, mock):
    test_parse_actions.attach_to(name, mock)
    mock.set_parse_action.assert_not_called()


@pytest.mark.parametrize(
    "parser_name,string,expected",
    [
        ("t1", "a", [0]),
        ("t2", "123", ["123"]),
        ("nested", "a a a", ["[0, 0, 0]"]),
        ("named_result_2", "x!", [("x", "found")]),
    ],
)
def test_base_parser(test_parser, parser_name, string, expected):
    """Make sure that the parse actions are triggered."""
    parser = test_parser[parser_name]
    result = parser.parse_string(string, parse_all=True).as_list()
    assert result == expected


@pytest.mark.parametrize(
    "string,flag,a",
    [
        ("1 a", "1", "a"),
        ("2 A", "2", "A"),
        ("0 a", "0", "a"),
        ("1 A", "1", "A"),
    ],
)
def test_matching(test_parser, string, flag, a, test_parse_actions):
    """Test the match_methods for Regex."""
    test_parser.operator_a.parse_string(string)
    assert test_parse_actions.a == a
    assert test_parse_actions.flag == flag


class UnusedParseMethod(ParseActionsBase):
    def parse_this_will_be_an_error(self, p: ParseResults):
        """The parser does not have this parser as an attribute."""


class UnusedMatchMethod(ParseActionsBase):
    def match_this_will_be_an_error(self, p: ParseResults):
        """The parser does not have this parser as an attribute."""


class UnusedName(ParseActionsBase):
    """The parser does not have this name as an attribute."""

    names = ["nonexistent_parser_name"]


@pytest.mark.parametrize(
    "parser_class,error_type,error_message",
    [
        (
            UnusedParseMethod,
            error.UnusedParseMethod,
            "UnusedParseMethod.parse_this_will_be_an_error was not attached to a parser. Maybe, the parser was renamed.",
        ),
        (
            UnusedMatchMethod,
            error.UnusedMatchMethod,
            "UnusedMatchMethod.match_this_will_be_an_error was not attached to a parser. Maybe, the parser was renamed.",
        ),
        (
            UnusedName,
            error.UnusedParserName,
            "UnusedName.names includes 'nonexistent_parser_name' that was not attached to a parser. Maybe, the parser was renamed.",
        ),
    ],
)
def test_parse_actions_without_a_match_raise_an_error(
    parser_class, error_type, error_message
):
    """If a parse action should be attached but is not, an error is raised."""
    with pytest.raises(error.UnusedParseAction) as e:
        ParserBase(parse_actions=[parser_class()])
    assert isinstance(e.value, error_type)
    assert e.value.message == error_message
