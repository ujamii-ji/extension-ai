# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Palette parsing tests for AI2020"""


def test_Xz_color(ai, data):
    """Check that we can parse the Xz color type"""
    color = "0.911711275577545 0.786861956119537 0.619531512260437 0.974486887454987 0 0 0 ([Passermarken]) 0 1 Xz\n"
    ai.color.parse_string(color, parse_all=True)


def test_Xz_color_cell(ai, data):
    """Check that we can parse the Xz color type as part of a palette cell"""
    color = """0.911711275577545 0.786861956119537 0.619531512260437 0.974486887454987 0 0 0 ([Passermarken]) 0 1 Xz
([Passermarken])
Pc
"""
    ai.palette_cell.parse_string(color, parse_all=True)


def test_pattern_reference_def(ai, data):
    """Test that we can read a pattern reference"""
    color = """(Laub) 0 0 1 1 0 0 0 0 0 [1 0 0 1 0 0] p\n"""
    ai.color.parse_string(color, parse_all=True)


def test_palette_gradient_ref(ai, data):
    """Test that we can read a gradient reference in the palette"""
    color = """Pc
Bb
2 (Wei�, Schwarz) 0 0 0 1 1 0 0 1 0 0 1 Bg
0 BB
(Wei�, Schwarz)
Pc
"""
    ai.palette_cell.parse_string(color, parse_all=True)


def test_palette(ai, data):
    """Test that we can read a simple AI2020-style palette"""
    color = """%AI5_BeginPalette
0 0 Pb
0.911711275577545 0.786861956119537 0.619531512260437 0.974486887454987 0 0 0 ([Passermarken]) 0 1 Xz
([Passermarken])
Pc
PB
%AI5_EndPalette
"""
    ai.color_palette.parse_string(color, parse_all=True)


def test_palette_extraction(ai, data):
    """Check that we can read text elements"""
    header_string = data.grammar["palette"].read(binary=False)
    header = ai.color_palette.parse_string(header_string)[0]
