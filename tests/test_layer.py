# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This contains tests for layers and objects that exist outside of them.
"""
import pytest
import inkex
from inkex import SvgDocumentElement, BaseElement


LAYER_TAG = "{http://www.w3.org/2000/svg}g"
DEFAULT_LAYER_NAME = "Layer 1"


def test_the_default_layer(svgs):
    """The content should be inside of a layer, so there must be one!"""
    layers = list(svgs.inkscape_minimized_ai3.iter(LAYER_TAG))
    assert len(layers) == 1
    layer = layers[0]
    assert layer.label == DEFAULT_LAYER_NAME
    paths = [element for element in layer.iter() if "path" in element.tag]
    assert len(paths) == 1, "There should be content!"


def test_flip_object_into_place(svgs):
    """AI files have the origin at the bottom left, SVG at the top left.

    This checks that the SVG transforms the content correctly.
    Check out this documentation for any questions about how this
    should be done:

        https://inkscape.gitlab.io/extensions/documentation/authors/units.html
    """
    svg: SvgDocumentElement = svgs.inkscape_minimized_ai3
    assert svg.get("viewBox") == "0 0 128 128"
    assert svg.get_viewbox() == [0, 0, 128, 128]
    layer = svg.get_current_layer()
    print(list(svg.iter()))
    layer = list(svg.iter())[2]
    assert layer.tag.endswith("g"), "This is a layer."
    assert layer.transform == inkex.Transform("translate(0, 128) scale(1, -1)")


def test_layer_is_current_layer(svgs):
    """We want to use svg.get_current_layer()"""
    svg: SvgDocumentElement = svgs.inkscape_minimized_ai3
    assert isinstance(svg, SvgDocumentElement)
    elements = list(svg.iter())
    print(*(f"{e}:{id(e)}" for e in elements))
    expected_layer = elements[2]
    assert expected_layer.tag.endswith("g"), "This is a layer."
    assert expected_layer.get_id() == "g1"
    assert svg.namedview.current_layer == "g1"
    assert svg.getElementById("g1") is expected_layer
    assert svg.getElementById("g1", "svg:g") is expected_layer
    assert svg.get_current_layer() is not None
    assert (
        svg.get_current_layer() is expected_layer
    ), f"{svg.get_current_layer()}:{id(svg.get_current_layer())} == {expected_layer}:{id(expected_layer)}"


@pytest.mark.parametrize(
    "file_name,layer_name, comment",
    [
        ("inkscape_minimized_ai3", "Layer 1", "default layer name for Inkscape"),
        ("inkscape_minimized_ai7", "svg2", "layer name in line 2900"),
    ],
)
def test_name_of_current_layer(svgs, file_name, layer_name, comment):
    """The layers should have a certain name."""
    svg = svgs[file_name]
    layer = svg.get_current_layer()
    assert layer.get("inkscape:label") == layer_name, comment


SVG = "svg"


@pytest.mark.parametrize(
    "id,name,parent_name",
    [
        ("Layer_2", "Layer 2", "svg"),
        ("Sublayer_2_x5F_1", "Sublayer 2_1", "Layer 2"),
        ("Sublayer_2_x5F_1_x5F_1", "Sublayer 2_1_1", "Sublayer 2_1"),
        ("Layer_1", "Layer 1", "svg"),
        ("Sublayer_1_x5F_2", "Sublayer 1_2", "Layer 1"),
        ("Sublayer_1_x5F_1", "Sublayer 1_1", "Layer 1"),
    ],
)
def test_layers_and_sub_layers(svgs, id, name, parent_name):
    """It is possible to create layers within layers.

    Test the structure mentioned in Issue 24
        https://gitlab.com/inkscape/extras/extension-ai/-/issues/24

    - Layer 2
      - Sublayer 2_1
        - Sublayer 2_1_1
    - Layer 1
      - Group
        - Path
        - Path
      - Sublayer 1_2
      - Sublayer 1_1
    """
    svg: SvgDocumentElement = svgs.layers_and_sublayers_extracted
    svg.set("inkscape:label", SVG)
    expected_layer: BaseElement = svg.getElementById(id)
    assert expected_layer is not None
    assert expected_layer.get("inkscape:label") == name
    assert expected_layer.get("id") == id
    parent = list(expected_layer.ancestors())[0]
    assert parent is not expected_layer, "We choose the right ancestor."
    assert parent.get("inkscape:label") == parent_name
