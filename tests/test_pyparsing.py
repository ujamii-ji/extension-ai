# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This includes some tests for pyparsing and its edge cases.

You can execute this file to measure speed of different parsers.
"""
import pyparsing as pp
import pytest
from inkai.benchmark.pyparsing import commands, measure_caching, measure_command


def assert_X_is_present(grammar, X):
    """Check that X is present and has a certain value."""
    parse_results = grammar.parse_string("a")
    print(parse_results, list(parse_results.items()))
    assert "X" in parse_results, f"value = {repr(X)}"
    assert parse_results["X"] == X or X is None


@pytest.mark.parametrize(
    "value",
    [
        "x",
        "",  # removes named value
        True,
        False,
        1,
        0,
        None,  # consistent with test_no_parse_action
        b"",
        b"a",
    ],
)
def test_with_parse_action(value):
    """Check which values are in the named result."""
    print("value =", repr(value))
    grammar = (
        (pp.Suppress("a") + pp.ZeroOrMore("x"))
        .add_parse_action(lambda p: value)
        .set_results_name("X")
    )
    if value == "":
        pytest.skip(
            "TODO: Wait until https://github.com/pyparsing/pyparsing/issues/470 is resolved."
        )
    assert_X_is_present(grammar, value)


def test_no_parse_action():
    """Do not add a parse result."""
    grammar = (pp.Suppress("a") + pp.ZeroOrMore("x")).set_results_name("X")
    assert_X_is_present(grammar, None)


def test_parse_action_can_replace_values():
    a = pp.Char("a").add_parse_action(lambda p: 10)
    many = pp.OneOrMore(a)

    def we_see_10(p):
        assert len(p) and all(v == 10 for v in p)

    many.add_parse_action(we_see_10)
    x = many.parse_string("a a a")
    assert x.as_list() == [10, 10, 10]


@pytest.mark.parametrize("string", ["a  a a    a a z"])
@pytest.mark.parametrize("command", commands)
def test_match_of_command_at_the_end(command, string):
    print(command)
    match: pp.ParseResults = command.parse_string(string, parse_all=True)
    assert match.as_list() == string.split()


def test_benchmark():
    """Measure the commands shortly to see the benchmark works."""
    measure_command(commands[0], seconds=0.001, steps=1)
    measure_caching(count=1)
