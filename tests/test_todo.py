# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test how TODOs behave."""
from inkai.errors import *
import pytest


@pytest.mark.parametrize("parser", ["todo", "not_done"])
@pytest.mark.skip(
    "We can not have a TODO that consumes a string or is empty. Best, we raise warnings for unknown but implemented tokens. Otherwise, we might get a match and it fails."
)
def test_todo(ai, parser):
    """Check how todos behave."""
    with pytest.raises(ThisSectionInTheFileIsNotUnderstoodYet) as e:
        ai[parser].parse_string("asd")
    assert e.value.name == parser
    assert len(e.value.parse_results) != 0
    assert e.value.name in str(e.value)
