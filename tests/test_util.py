# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This tests the utility functions."""
import pytest
from inkai.util import to_pretty_xml


@pytest.mark.parametrize(
    "xml_string,expected_output",
    [
        (b"<svg><g></g></svg>", b"<svg>\n  <g/>\n</svg>\n"),
    ],
)
def test_xml_becomes_pretty(xml_string, expected_output):
    """Test thet the xml is nicely formatted."""
    output = to_pretty_xml(xml_string)
    print(repr(output))
    print(repr(expected_output))
    assert output == expected_output
