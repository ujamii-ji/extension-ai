# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This tests loading full documents.
"""
import pytest
from inkai.extraction import extract_ai_privatedata
from inkai.errors import *
from pyparsing import Regex, ParseException


def add_steps(l):
    """Add the step index to the tuples inside."""
    return [(i,) + t for i, t in enumerate(l)]


@pytest.mark.parametrize(
    "step,document,line_start,parser_name,line_end",
    add_steps(
        [
            ("simple_v3_with_newlines", 1, "document_structuring_convention", 2),
            ("simple_v3_with_newlines", 1, "prolog", 38),
            ("simple_v3_with_newlines", 47, "encoding_pairs", 62),
            ("simple_v3_with_newlines", 46, "font_encoding", 66),
            ("simple_v3_with_newlines", 38, "setup", 2571),
            ("simple_v3_with_newlines", 2583, "path_mask", 2590),
            ("simple_v3_with_newlines", 2582, "mask", 2599),
            ("simple_v3_with_newlines", 2571, "object", 2600),
            ("simple_v3_with_newlines", 2582, "object", 2599),
            ("simple_v3_with_newlines", 2600, "object", 2632),
            ("simple_v3_with_newlines", 2632, "page_trailer", 2634),
            ("simple_v3_with_newlines", 2634, "document_trailer", 2642),
            ("simple_v3_with_newlines", 1, "document", 2643),
            ("layers_and_sublayers_extracted", 43, "setup", 28389),
            ("layers_and_sublayers_extracted", 28389, "layer", 28460),
            ("layers_and_sublayers_extracted", 28425, "group_object", 28454),
            ("layers_and_sublayers_extracted", 28427, "path_object", 28441),
            ("layers_and_sublayers_extracted", 28399, "layer", 28411),
            ("layers_and_sublayers_extracted", 28480, "layer", 28492),
            ("layers_and_sublayers_extracted", 28483, "object_definitions", 28490),
            ("layers_and_sublayers_extracted", 28488, "object_definitions", 28490),
            ("layers_and_sublayers_extracted", 28488, "object", 28490),
            (
                "layers_and_sublayers_extracted",
                28489,
                "unspecified_object_operator",
                28490,
            ),
            ("layers_and_sublayers_extracted", 28470, "layer", 28494),
            ("layers_and_sublayers_extracted", 1, "document", 28500),
        ]
    ),
)
@pytest.mark.filterwarnings("ignore::inkai.errors.NotWorthImplementingWarning")
def test_load_document_and_parse_it_with_the_grammar(
    ai, data, step, line_start, parser_name, line_end, document
):
    """The grammar should be able to match the document.

    This parses it step by step.
    line_start and line_stop match the line numbers on the side of your editor
    """
    #    if document != "layers_and_sublayers_extracted":
    #        pytest.skip()
    raw = extract_ai_privatedata(data.ai[document].read())
    skipped = ai.skip_line * (line_start - 1)
    skipped_text = (skipped + Regex("[^\r\n]*")).parse_string(raw)
    print(f"{ai[parser_name]} starts at line {line_start}:")
    print(skipped_text[0])
    parser = skipped + ai[parser_name]
    try:
        result = parser.parse_string(raw, parse_all=True)
    except MissingFeature:
        raise
    except ParseException as e:
        assert e.lineno == line_end, "Explanation:\n" + e.explain(None)
