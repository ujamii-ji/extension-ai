# SPDX-FileCopyrightText: 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Pytest's conftest to configure common fixtures."""

import os
import sys
import pytest
import pyparsing as pp
import unittest.mock
from typing import List
import subprocess
from collections import OrderedDict
import traceback
from pprint import pprint
import inkex

HERE = os.path.dirname(__file__) or "."
# This is suggested by https://docs.python-guide.org/writing/structure/.
sys.path.insert(0, os.path.abspath(os.path.join(HERE, "..")))
from inkai.modular_parser import Parser as ParserBase
from inkai.modular_parser import ParseActions as ParseActionsBase
from inkai.parser.data_hierarchy import DataHierarchyParser
from inkai.extraction import extract_ai_privatedata
from inkai.parser.ai import AdobeIllustratorParser
import inkai
from inkex import SvgDocumentElement, load_svg
from inkai.parse_actions.common.ids import IdGenerator
from inkai.util import to_pretty_xml


@pytest.fixture()
def ids() -> IdGenerator:
    """Create an IdGenerator"""
    return IdGenerator()


ParseActionsBase.warn_if_the_file_is_needed = False


class TestParseActions(ParseActionsBase):
    names = ["named_result"]

    a = ""

    def parse_named_result_2(self, parse_results):
        return (parse_results["named_result"][0], "found")

    def parse_t1(self, parse_results):
        """a --> 0"""
        return 0

    #    def parse_t3(self, parse_results):
    #        """Not used."""
    #        return 1

    def parse_nested(self, parse_results):
        return str(list(parse_results))

    def match_operator_a_operator_a(self, parse_results):
        self.a = "a"
        assert parse_results.operator == "a"
        self.flag = parse_results.flag

    def match_operator_a_operator_A(self, parse_results):
        self.a = "A"
        assert parse_results.operator == "A"
        self.flag = parse_results.flag


class TestParser(ParserBase):
    def create_tokens(self):
        """Create tokens for testing."""
        self.t1 = pp.Char("a")
        self.t2 = pp.Word(pp.nums)
        self.nested = pp.OneOrMore(self.t1)
        self.named_result = pp.Char("x")
        self.named_result_2 = self.named_result + "!"
        self.operator_a = pp.Regex(
            r"(?P<flag>[012])\s+(?P<operator>a|A)", as_match=True
        )


@pytest.fixture()
def test_parse_actions():
    """Test parse actions."""
    return TestParseActions()


@pytest.fixture()
def test_parser(test_parse_actions):
    """Test parser with parse_actions attached."""
    return TestParser([test_parse_actions])


class ShortFileAccess:
    """Fixture for easy file access.

    a = ShortFileAccess("/")
    a.opt.file -> content
    """

    ignore_files = [".license", ".ai.txt"]

    def __init__(self, path):
        """Create a nice file access for a path."""
        self.__path = path

    def __getattr__(self, name):
        """Return a file content or a folder."""
        return self[name]

    def __getitem__(self, name):
        """Return a file content or a folder."""
        assert not os.path.isfile(self.__path)
        entries = os.listdir(self.__path)
        possible_entries = [
            entry
            for entry in entries
            if not any(entry.endswith(extension) for extension in self.ignore_files)
        ]
        chosen_entries = [entry for entry in possible_entries if name in entry]
        if not chosen_entries:
            raise FileNotFoundError(
                f"{name} does not exist in {self.__path}.\nChoose one in here: {', '.join(possible_entries)}"
            )
        chosen_entry = min(chosen_entries, key=len)
        new_path = os.path.join(self.__path, chosen_entry)
        return self.__class__(new_path)

    def __repr__(self):
        return f"ShortFileAccess({self.__path})"

    def read(self, binary=True):
        """Read the contents of the file."""
        assert os.path.isfile(self.__path)
        with open(self.__path, ("rb" if binary else "r")) as file:
            return file.read()

    @property
    def path(self):
        """The path of the file or directory."""
        return self.__path


@pytest.fixture(scope="module")
def data():
    """Access to the data directory."""
    return ShortFileAccess(HERE).data


@pytest.fixture(scope="module")
def ai_module():
    """Cached version of the parser so it does not need to be reconstructed."""
    return AdobeIllustratorParser()


@pytest.fixture()
def ai(ai_module):
    """Return a parser to check the grammar implementation with."""
    ai_module.reset()
    return ai_module


@pytest.fixture()
def mock():
    """Return a unittest mock object."""
    return unittest.mock.Mock()


@pytest.fixture()
def cmd():
    """Execute this extension on the command line with the given arguments."""
    cwd = os.path.join(HERE, "..")

    def cmd(arguments: List[str]) -> subprocess.Popen:
        """Execute this extension on the command line with the given arguments."""
        command = [sys.executable, inkai.__name__]
        command.extend(arguments)
        print("cmd: ", "'" + "' '".join(command) + "'")
        return subprocess.Popen(
            command,
            cwd=cwd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE,
        )

    return cmd


# ----------------------------------------------------------------------
#
# SVG access
#

EXPECTED_STATUS = [
    [],
    [],
    [],
    [
        "flowtext.ai",
        "inkscape_minimized_ai10.ai",
        "inkscape_minimized_ai9.ai",
        "inkscape_minimized_ai_cc.ai",
        "inkscape_minimized_ai_cc_current.ai",
        "inkscape_minimized_ai_cs.ai",
        "inkscape_minimized_ai_cs2.ai",
        "inkscape_minimized_ai_cs3.ai",
        "inkscape_minimized_ai_cs4.ai",
        "inkscape_minimized_ai_cs5.ai",
        "inkscape_minimized_ai_cs6.ai",
        "simple_cc_legacy.ai",
        "simple_cs.ai",
        "simple_cs2.ai",
        "simple_cs3.ai",
        "simple_cs4.ai",
        "simple_cs5.ai",
        "simple_cs6.ai",
        "simple_v10.ai",
        "simple_v2020.ai",
        "simple_v2020_no_pdf_data.ai",
        "simple_v3_jp.ai",
        "simple_v8.ai",
        "simple_v9.ai",
    ],
    [
        "circle.ai",
        "cmyk_rectangle.ai",
        "empty_cs.ai",
        "empty_cs2.ai",
        "inkscape_minimized_ai3.ai",
        "inkscape_minimized_ai7.ai",
        "irregular_closed_shape.ai",
        "irregular_line.ai",
        "layers_and_sublayers.ai",
        "layers_and_sublayers_extracted.ai",
        "line_broken_in_middle.ai",
        "rectangle.ai",
        "rectangle_with_rounded_corners.ai",
        "rgb_rectangle.ai",
        "simple_v3.ai",
        "simple_v3_with_newlines.ai",
        "rectangle-rounded-corners-all-different.ai",
    ],
]

DUPLICATED_FILES = []
AI_FILES = OrderedDict()  # parameter: path
for dirpath, dirnames, filenames in os.walk(HERE):
    for filename in filenames:
        if filename.endswith(".ai"):
            if filename in AI_FILES:
                DUPLICATED_FILES.append(filename)
            AI_FILES[filename] = os.path.join(dirpath, filename)


@pytest.fixture()
def duplicated_files() -> List[str]:
    """A list of duplicated file names."""
    return DUPLICATED_FILES


class StepwiseFileProcessing:
    """Process AI files in steps."""

    _all_processes = set()

    def __init__(self, name):
        """Create a stepwise process."""
        self.name = name
        self.path = AI_FILES[name]
        self._extract = None
        self._parse = None
        self._tried_extract = False
        self._tried_parse = False
        self._tried_last_step = False
        self._all_processes.add(self)
        self.error = None

    def run_extract(self):
        """Step 1: Extract data."""
        if self._extract is not None:
            return self._extract
        if self._tried_extract:
            self.skip()
        self._tried_extract = True
        try:
            with open(self.path, "rb") as file:
                self._extract = extract_ai_privatedata(file.read())
                assert "%%EOF" in self._extract
        except:
            self.on_error()

    extract = property(run_extract)

    _parser = None

    @classmethod
    def get_parser(cls) -> AdobeIllustratorParser:
        """Return a parser to use for the steps."""
        if cls._parser is None:
            cls._parser = AdobeIllustratorParser()
        return cls._parser

    def run_parse(self):
        """Step 2: Parse the data."""
        self._tried_last_step = True
        if self._parse is not None:
            return self._parse
        self.extract
        if self._tried_parse:
            self.skip()
        self._tried_parse = True
        try:
            self._parse = self.get_parser().parse_string(self.extract)
        except:
            self.on_error()
        return self._parse

    parse = property(run_parse)

    def on_error(self):
        """What happens if a step has an error."""
        self.error = ty, err, tb = sys.exc_info()
        traceback.print_exception(ty, err, tb)
        if isinstance(err, pp.ParseException):
            error: pp.ParseException = err
            print(error.explain())

    def check(self):
        """Raise the exception this process had."""
        if self.had_error:
            raise self.error[1].with_traceback(self.error[2])

    @property
    def had_error(self):
        """Whether there was an error."""
        return bool(self.error)

    def skip(self):
        """Skip the tests and inform the user why."""
        message = f"I skip this test because {self} I skip this test so you know."
        print("skip", message)
        pytest.skip(message)

    @property
    def steps(self):
        """The number of steps taken."""
        return (
            self._tried_extract
            + (self._extract is not None)
            + self._tried_parse
            + (self._parse is not None)
        )

    step_names = [
        "found",
        "failed to extract",
        "extracted",
        "failed to parse",
        "parsed",
    ]

    def run(self, name):
        """Run a step"""
        getattr(self, "run_" + name)()

    @property
    def step_name(self):
        """The name of step we are at."""
        return self.step_names[self.steps]

    def __repr__(self):
        """Return a good representation."""
        return f"I {self.step_name} {self.name} as step {self.steps}."

    @classmethod
    def print_current_status(cls):
        """Print the current status as a dict so it can be copied and pasted."""
        current_status = []
        for process in cls._all_processes:
            while len(current_status) <= process.steps:
                current_status.append([])
            current_status[process.steps].append(process.name)
        for l in current_status:
            l.sort()  # Sort the filenames so we do not have a problem with git.
        print("EXPECTED_STATUS = ", end="")
        pprint(current_status)

    @property
    def last_process_has_been_used(self):
        """Whether the last step has been executed at least once."""
        return self._tried_last_step

    @classmethod
    def has_processed_all(cls) -> bool:
        """Return whether all files have been processed at least a bit."""
        return all(process.last_process_has_been_used for process in cls._all_processes)

    last_process_name = "parse"

    @classmethod
    def get(self, name):
        """Return a StepwiseFileProcessing for the ai file with the name."""
        processes = [process for process in self._all_processes if name in process.name]
        processes.sort(key=lambda process: len(process.name))
        assert len(processes) == 1 or len(processes[0].name) < len(
            processes[1].name
        ), f"Name {name} should only match one of {', '.join(process.name for process in processes)}"
        return processes[0]

    @property
    def svg(self):
        """Return the SVG."""
        svg = self.parse
        if svg is None:
            self.check()
            assert (
                False
            ), "Implementation is wrong: This should never be reached. The SVG is None!"
        print(to_pretty_xml(svg.tostring()).decode("latin-1"))
        return svg.copy()

    @classmethod
    def print_current_status_if_this_is_the_last_process(cls):
        """Test update for convenience."""
        if cls.has_processed_all():
            # Print the current status in the last test when all tests have been run.
            cls.print_current_status()

    @property
    def expected_step_to_get_stuck_at(self) -> int:
        """Normally, processes get stuck at one point or succeed.

        Return the step index.
        """
        for i, filenames in enumerate(EXPECTED_STATUS):
            if self.name in filenames:
                return i
        return -1

    @classmethod
    def get_process_fixture_parameter_list(cls):
        """Return a sorted list for testing."""
        if not cls._all_processes:
            for name in AI_FILES:
                cls(name)
        l = list(cls._all_processes)
        l.sort(key=lambda self: self.name)
        return l


class ProcessedAttribute:
    """Fast access to generated SVG files and such, generated."""

    def __init__(self, attribute: str):
        self.attribute = attribute

    def __getattr__(self, name) -> object:
        """Return the attibute of the processed ai file."""
        processing = StepwiseFileProcessing.get(name)
        assert processing is not None
        return getattr(processing, self.attribute)

    __getitem__ = __getattr__


@pytest.fixture()
def svgs() -> ProcessedAttribute:
    """Return svgs that are cached accross the test runs.

    To generate an SVG from inkscape_minimized_ai3.ai,
    use this code:

        svg = svgs.inkscape_minimized_ai3
    """
    return ProcessedAttribute("svg")


@pytest.fixture(scope="module")
def svg_logo(data) -> SvgDocumentElement:
    """Return the parsed SVG document from the inkscape_minimized.svg file"""
    return load_svg(data.inkscape_logo.inkscape_minimized.read()).getroot()


@pytest.fixture(params=StepwiseFileProcessing.get_process_fixture_parameter_list())
def process(request) -> StepwiseFileProcessing:
    """Return each stepwise processing operation from .ai to .svg."""
    return request.param


@pytest.fixture(params=["extract", "parse"], scope="module")
def processing_step(request) -> str:
    """Return each stepwise processing operation from .ai to .svg."""
    return request.param


class DataObjectForTests:
    """Shortcut for tests."""

    def __init__(self, ai):
        """Create this."""
        self.ai = ai

    def parse_string(self, string, parse_all=False):
        """Parse the string."""
        if string.startswith("%"):
            parser = self.ai.data_hierarchy_section
        else:
            parser = self.ai.data_object
            string = string.replace("\n", " ")
        return parser.parse_string(string, parse_all=parse_all)


@pytest.fixture(params=["ai.data_object", "DataHierarchyParser"])
def data_object(request, ai):
    """Return the parsers for the hierarchical data object."""
    if request.param == "ai.data_object":
        return DataObjectForTests(ai)
    return DataHierarchyParser()


class RectangleTestData:
    """Test data for rectangles."""

    def __init__(self, layer_result: pp.ParseResults):
        self.layer_result = layer_result

    @property
    def layer(self) -> inkex.Layer:
        """The base element of the file."""
        assert len(self.layer_result) == 1
        layer = self.layer_result[0]
        assert len(layer) >= 1
        return layer

    @property
    def rect(self) -> inkex.ShapeElement:
        """The rectangle."""
        return self.layer[0]

    @property
    def path_effect(self) -> inkex.PathEffect:
        """The path effect of the rectangle."""
        return self.layer[1]  # Assume there is a path effect


@pytest.fixture(scope="module")
def _rectangle_cache() -> dict:
    """Cache the parsed rectangles."""
    return {}


@pytest.fixture()
def rect(data, ai, _rectangle_cache) -> RectangleTestData:
    """Return a function to generate rectangles from the grammar.

    This is module scoped to speed up the tests.
    """

    def rect(file: str) -> inkex.ShapeElement:
        """Read out the rectangle."""
        if file in _rectangle_cache:
            return _rectangle_cache[file]
        content = data.rectangle[file].read(binary=False)
        ai.reset()
        layer_result = ai.layer.parse_string(content, parse_all=True)
        _rectangle_cache[file] = result = RectangleTestData(layer_result)
        return result

    return rect
