# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test parsing lines."""
from inkai.parser.lines import LineParser, ReachedEndOfFile
import pytest


class LineReader:
    """Read lines."""

    def __init__(self, lines, newline):
        """Create a new LineReader."""
        self.lines = lines
        self.newline = newline

    def readline(self):
        """Read a line or return an empty string."""
        if not self.lines:
            return ""
        if len(self.lines) == 1:
            return self.lines.pop(0)
        return self.lines.pop(0) + self.newline


@pytest.mark.parametrize("newline", ["\r", "\r\n", "\n"])
@pytest.mark.parametrize("eos", ["\r", "\n", "\r\n", ""])
def test_lines(eos, newline):
    """Test reading lines.

    eos is the end of the string
    newline is what the readline() method assumes to be a newline.
        This tests Windows, Mac, Linux.
    """
    string = "line1\nline2\r\n\rline4" + eos
    lines = string.split(newline)
    reader = LineReader(lines, newline)
    lp = LineParser(reader)
    # no line read, yet
    assert lp.current_line == ""
    assert lp.current_line_end == ""
    assert lp.current_line_number == 0
    # line 1
    assert lp.read_line() == "line1"
    assert lp.current_line == "line1"
    assert lp.current_line_end == "\n"
    assert lp.current_line_number == 1
    # line 2
    assert lp.read_line() == "line2"
    assert lp.current_line == "line2"
    assert lp.current_line_end == "\r\n"
    assert lp.current_line_number == 2
    # line 3
    assert lp.read_line() == ""
    assert lp.current_line == ""
    assert lp.current_line_end == "\r"
    assert lp.current_line_number == 3
    # line 4
    assert lp.read_line() == "line4"
    assert lp.current_line == "line4"
    assert lp.current_line_end == eos
    assert lp.current_line_number == 4
    with pytest.raises(ReachedEndOfFile) as e:
        lp.read_line()
    assert lp.current_line == "line4"
    assert lp.current_line_end == eos
    assert lp.current_line_number == 4
    error = e.value
    assert error.args[0] == "Reached end of file after line 4."
