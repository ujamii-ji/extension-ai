# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This checks all AI files in the project if they can be used 100%.

These tests operate on module scope.
"""
import pytest


def test_there_are_no_duplicated_file_names(duplicated_files):
    """We want to make sure that we do not forget a file."""
    assert (
        not duplicated_files
    ), f"There are two files with these names: {', '.join(duplicated_files)}"


@pytest.mark.filterwarnings("ignore::inkai.errors.NotWorthImplementingWarning")
def test_check_all_ai_files(processing_step, process):
    """Walk though processing the files step by step to see where we are at."""
    print(process)
    process.run(processing_step)
    print(process)
    expected_steps = process.expected_step_to_get_stuck_at
    process.print_current_status_if_this_is_the_last_process()
    assert (
        expected_steps != -1
    ), f"{process.name} is not recorded, yet. Please update EXPECTED_STATUS by running all tests and copying it in from the last test."
    assert (
        expected_steps >= process.steps
    ), f"Congratulations! You made more files work. Please update the EXPECTED_STATUS to include {process.name}."
    assert (
        expected_steps <= process.steps or process.last_process_name != processing_step
    ), f"Your changes break compatibility. {process.name} breaks now: {process}"
    if process.had_error:
        # only files that are without error should show up as green
        process.skip()
