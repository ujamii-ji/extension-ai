# SPDX-FileCopyrightText: 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test that we can extract the private data of different documents"""
import pytest
from inkai.extraction import extract_ai_privatedata


@pytest.mark.parametrize(
    "file_name,version",
    [
        ("simple_v3.ai", "3.2"),
        ("simple_v3_jp.ai", "3.2"),
        ("simple_v8.ai", "8.0"),
        ("simple_v9.ai", "9.0"),
        ("simple_v10.ai", "10.0"),
        ("simple_cs.ai", "11.0"),
        ("simple_cs2.ai", "12.0"),
        ("simple_cs3.ai", "13.0"),
        ("simple_cs4.ai", "14.0"),
        ("simple_cs5.ai", "15.0"),
        ("simple_cs6.ai", "16.0"),
        ("simple_cc_legacy.ai", "17.0"),
        ("simple_v2020.ai", "24.0"),
        ("simple_v2020_no_pdf_data.ai", "24.0"),
    ],
)
def test_data_extraction(data, file_name, version):
    """Test that we can extract the private data of different documents"""
    content = data.ai[file_name].read()
    result = extract_ai_privatedata(content).splitlines()
    creators = [
        f"%%Creator: Adobe Illustrator(R) {version}",
        f"%%Creator: Adobe Illustrator(TM) {version}",
    ]
    assert result[0].strip() == "%!PS-Adobe-3.0"
    assert result[1].strip() in creators


@pytest.mark.parametrize(
    "file_name,substring",
    [
        ("simple_v3.ai", b"%%Creator: Adobe Illustrator(TM) 3.2"),
        ("simple_v3_jp.ai", b"%%Creator: Adobe Illustrator(TM) 3.2"),
        ("simple_cc_legacy.ai", b"%%Creator: Adobe Illustrator(R) 17.0"),
    ],
)
def test_extraction_of_ai_privatedata(cmd, data, file_name, substring):
    """Check that certain files have certain data included."""
    if file_name != "simple_v3.ai":
        pytest.skip("TODO")
    process = cmd(["--debug-ai-data", data.ai[file_name].path])
    stdout, stderr = process.communicate()
    print((stdout[:1000], stderr[:1000]))
    assert process.returncode == 0
    assert substring in stderr
