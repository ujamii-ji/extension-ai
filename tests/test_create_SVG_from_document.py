# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This does not test loading but creating an SVG file from an existing document.
"""
import pytest
from inkex import SvgDocumentElement, BaseElement


def test_read_svg_file(svg_logo: SvgDocumentElement):
    """Check that we can use the svg_logo fixture."""
    assert svg_logo.get("width") == "128"
    assert svg_logo.get("height") == "128"


@pytest.mark.parametrize("attribute", ["width", "height"])
@pytest.mark.filterwarnings("ignore::inkai.errors.NotWorthImplementingWarning")
def test_loading_the_whole_document(svgs, attribute, svg_logo):
    """Check that the whole document passes the grammar.

    This is a fundamental test for this file.
    If this test does not run, the others will fail as well.
    """
    assert svgs.inkscape_minimized_ai3.get(attribute) == svg_logo.get(attribute)


@pytest.mark.parametrize(
    "svg_name,parent_id,child_id",
    [
        ("layers_and_sublayers_extracted", "Layer_1", "g1"),
        ("rectangle", "Layer_1", "rect1"),
        ("rectangle-rounded-corners-all-different", "Layer_1", "rect1"),
        ("rectangle-rounded-corners-all-different", "defs1", "rectangle-path-effect-1"),
    ],
)
def test_svg_structure_parent(svgs, svg_name, parent_id, child_id):
    """This tests whether an element is a child of a parent element."""
    svg: SvgDocumentElement = svgs[svg_name]
    parent: BaseElement = svg.getElementById(parent_id)
    assert parent is not None, f"Expecting element with id {parent_id}."
    child: BaseElement = svg.getElementById(child_id)
    assert child is not None, f"Expecting element with id {child_id}."
    assert (
        child.getparent() is parent
    ), f"Parent element {parent} should have a child {child} in {svg_name}"


@pytest.mark.parametrize(
    "svg_name,id,tag",
    [
        ("layers_and_sublayers_extracted", "g1", "g"),
        ("layers_and_sublayers_extracted", "Layer_1", "g"),
        ("rectangle-rounded-corners-all-different", "defs1", "defs"),
    ],
)
def test_svg_structure_tag(svgs, svg_name, id, tag):
    """This tests the tag name of an element."""
    svg: SvgDocumentElement = svgs[svg_name]
    element: BaseElement = svg.getElementById(id)
    assert element is not None, f"Expecting element with id {id}."
    assert (
        element.tag_name == tag
    ), f"Expecting tag of {id} in {svg_name} to be '{tag}'."


LOCKED_ATTRIBUTE = "sodipodi:insensitive"


@pytest.mark.parametrize(
    "svg_name,id,attribute,expected_value",
    [
        ("layers_and_sublayers_extracted", "Layer_1", LOCKED_ATTRIBUTE, None),
        ("layers_and_sublayers_extracted", "Layer_2", LOCKED_ATTRIBUTE, "true"),
        ("layers_and_sublayers_extracted", "Sublayer_1_x5F_2", LOCKED_ATTRIBUTE, None),
        (
            "layers_and_sublayers_extracted",
            "Sublayer_1_x5F_1",
            LOCKED_ATTRIBUTE,
            "true",
        ),
        ("layers_and_sublayers_extracted", "Sublayer_1_x5F_2", "style", "display:none"),
        ("layers_and_sublayers_extracted", "Layer_1", "style", None),
    ],
)
def test_svg_structure_attribute(svgs, svg_name, id, attribute, expected_value):
    """This tests the attributes of an element with a given id."""
    svg: SvgDocumentElement = svgs[svg_name]
    element: BaseElement = svg.getElementById(id)
    assert element is not None, f"Expecting element with id {id}."
    assert (
        element.get(attribute) == expected_value
    ), f"Expecting attribute {attribute} of {id} in {svg_name} to be '{expected_value}'."


@pytest.mark.parametrize(
    "svg_name,id,number_of_children",
    [
        ("layers_and_sublayers_extracted", "Layer_1", 3),
        ("layers_and_sublayers_extracted", "g1", 2),
        ("layers_and_sublayers_extracted", "Sublayer_1_x5F_2", 0),
        ("layers_and_sublayers_extracted", "Sublayer_1_x5F_1", 0),
        ("layers_and_sublayers_extracted", "Layer_2", 1),
        ("layers_and_sublayers_extracted", "Sublayer_2_x5F_1", 1),
        ("layers_and_sublayers_extracted", "Sublayer_2_x5F_1_x5F_1", 0),
    ],
)
def test_svg_structure_number_of_children(svgs, svg_name, id, number_of_children):
    """This tests the attributes of an element with a given id."""
    svg: SvgDocumentElement = svgs[svg_name]
    element: BaseElement = svg.getElementById(id)
    assert element is not None, f"Expecting element with id {id}."
    assert (
        len(element) == number_of_children
    ), f"Expecting {id} in {svg_name} to have {number_of_children} children."
