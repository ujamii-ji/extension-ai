# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test generating Ids."""
import pytest


def test_generate_new_ids(ids):
    """new id each time"""
    assert ids.new() == "1"
    assert ids.new("a") == "a2"
    assert ids.new("ab") == "ab3"
    assert ids.new() == "4"


@pytest.mark.parametrize("tag", ["path", "rect"])
@pytest.mark.parametrize("index", [0, 2])
def test_set_id(ids, mock, tag, index):
    for i in range(index):
        ids.new()
    mock.tag_name = tag
    mock.get_id.return_value = None
    ids.set(mock)
    assert mock.set_id.called
    mock.set_id.assert_called_once_with(
        f"{tag}{index + 1}",
    )
