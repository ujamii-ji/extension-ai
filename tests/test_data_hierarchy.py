# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This tests the parser for the hirarchical data e.g. inside of BeginDocumentData.

This is documented in docs/specification_amendments/document_data.md.
"""
import pytest
from pyparsing import ParseException, ParserElement
import inkex
from inkai.parse_actions.common import small_explain
from inkai.parse_actions.data_hierarchy import (
    replace_document_data_newlines_with_spaces,
)
from inkai.parser.data_hierarchy import tokenize

EXAMPLE_SECTION = """%_/Array :
%_/Dictionary :
%_(Page 1) /UnicodeString (Name) ,
%_1 /Real (PAR) ,
%_0 /Bool (IsArtboardSelected) ,
%_0 /Int (DisplayMark) ,
%_7231 7651 /RealPoint
%_ (RulerOrigin) ,
%_0 /Bool (IsArtboardDefaultName) ,
%_0 0 /RealPointRelToROrigin
%_ (PositionPoint1) ,
%_(9bd9a3f4-3094-4e20-a475-f003b8aa0da8) /String (ArtboardUUID) ,
%_1920 -1080 /RealPointRelToROrigin
%_ (PositionPoint2) ,
%_; ,
%_/Dictionary :
%_(Page 2)
%_/Binary : /ASCII85Decode ,
%!!n*o5t"%.!Y>>3E,p&@6VU]V9OVBQ#LEGU!#bh@!%@n*@:O@t5u:BOz5t"%.zzzz!!)`D!!*'"!!(J"
%5t"%.zzzzzzzzzzz!!!!+A7]gl!!!#s!!!"@@rQI1!!!%=!!!!LGB@eG
%s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!s8W-!rr<$!~>
%_; (AI12 Document Profile Data) ,
other command"""


def test_match_the_section_to_its_end(ai):
    """Parse the whole section very fast."""
    p = ai.data_hierarchy_section.parse_string(EXAMPLE_SECTION)
    assert p[0] == EXAMPLE_SECTION[: -len("other command")]


XML_NODE = """
/XMLNode :
2 /Int (xmlnode-nodetype) ,
(SourceGraphic) /UnicodeString (xmlnode-nodevalue) ,
(in) /UnicodeString (xmlnode-nodename) ,
/Array :
; (xmlnode-children) ,
/Dictionary :
; (xmlnode-attributes) ,
;
"""

BOOL_EXAMPLE_FALSE = """
/ArtDictionary :
0 /Bool (AI13PatternEnableGuides) ,
;
"""
BOOL_EXAMPLE_TRUE = """
/ArtDictionary : 
1 /Bool (AI13PatternEnableGuides) ,
;
"""
# see https://en.wikipedia.org/wiki/Ascii85#Adobe_version
BINARY_EXAMPLE = """/Binary : /ASCII85Decode ,
87cURD]i,"Ebo80~>
;
"""

BINARY_EXAMPLE_WITH_NEWLINE = """/Binary : /ASCII85Decode ,
87cURD]i,
"Ebo80~>
;
"""

DOCUMENT_DATA_EXAMPLE = """%_/ArtDictionary :
%_123 /Int (test) ,
%_;
"""

ARRAY_EXAMPLE = """ /Array :
/XMLNode : 1 /Int (xmlnode-nodetype) ,  /String (xmlnode-nodevalue) , (variables) /String (xmlnode-nodename) , /Array : ; (xmlnode-children) , /Dictionary : ; (xmlnode-attributes) , ;
 ,
/XMLNode : 1 /Int (xmlnode-nodetype) ,  /String (xmlnode-nodevalue) , (v:sampleDataSets) /String (xmlnode-nodename) , /Array : ; (xmlnode-children) , /Dictionary : /XMLNode : 2 /Int (xmlnode-nodetype) , (&amp;ns_custom;) /String (xmlnode-nodevalue) , (xmlns) /String (xmlnode-nodename) , /Array : ; (xmlnode-children) , /Dictionary : ; (xmlnode-attributes) , ; (xmlns) , /XMLNode : 2 /Int (xmlnode-nodetype) , (&amp;ns_vars;) /String (xmlnode-nodevalue) , (xmlns:v) /String (xmlnode-nodename) , /Array : ; (xmlnode-children) , /Dictionary : ; (xmlnode-attributes) , ; (xmlns:v) , ; (xmlnode-attributes) , ;
 , ;
"""

MATCH = object()


@pytest.mark.parametrize(
    "parser,string,expected_value",
    [
        ("data_object", "/ArtDictionary : ;", {}),
        (
            "data_object",
            "/ArtDictionary : /XMLUID : (Sublayer_1_x5F_1) ; (AI10_ArtUID) , ;",
            {"AI10_ArtUID": "Sublayer_1_x5F_1"},
        ),
        (
            "data_object",
            XML_NODE,
            {
                "xmlnode-nodetype": 2,
                "xmlnode-nodevalue": "SourceGraphic",
                "xmlnode-nodename": "in",
                "xmlnode-children": [],
                "xmlnode-attributes": {},
            },
        ),
        ("data_object", "/Array : ;", []),
        ("data_object", "/Array : 1 /Int , /Dictionary : ; , ;", [1, {}]),
        ("data_object", "/String", ""),
        ("data_object", "(27.1.1) /String", "27.1.1"),
        ("data_object", "/UnicodeString", ""),
        (
            "data_object",
            BOOL_EXAMPLE_TRUE,
            {
                "AI13PatternEnableGuides": True,
            },
        ),
        (
            "data_object",
            BOOL_EXAMPLE_FALSE,
            {
                "AI13PatternEnableGuides": False,
            },
        ),
        (
            "data_object",
            "197.85549500175 231 110.544897207401 77 0 0 /RealMatrix",
            [197.85549500175, 231.0, 110.544897207401, 77.0, 0.0, 0.0],
        ),
        ("data_object", "197.85549500175 /Real", 197.85549500175),
        (
            "data_object",
            "/Dictionary : /NotRecorded , ;",
            {
                "recorded": False,
            },
        ),
        (
            "data_object",
            "/Dictionary : /Recorded , ;",
            {
                "recorded": True,
            },
        ),
        ("data_object", "/Dictionary : ;", {}),
        (
            "data_object",
            "(Abgerundete Ecken 2 Pt.) /Name",
            {"name": "Abgerundete Ecken 2 Pt."},
        ),
        ("data_object", ARRAY_EXAMPLE, MATCH),
        ("data_object", "0 -500 /RealPoint", inkex.Vector2d(0, -500)),
        (
            "data_object",
            "/Document : /Dictionary : /NotRecorded , 1 /Bool (SnapWhileScaling) , ; /NotRecorded , ;",
            [
                {
                    "recorded": False,
                    "SnapWhileScaling": True,
                }
            ],
        ),
    ],
)
def test_parse_data_hierarchy_into_object_structure(
    data_object, string, parser, expected_value
):
    """Create data structures for the data hierarchy."""
    assert_parser_returns_value(data_object, string, expected_value)


@pytest.mark.parametrize(
    "parser,string,expected_value",
    [
        ("parsed_data_hierarchy_section", DOCUMENT_DATA_EXAMPLE, {"test": 123}),
    ],
)
def test_parse_data_hierarchy_into_object_structure_raw(
    ai, string, parser, expected_value
):
    """Create data structures for the data hierarchy."""
    parser = ai[parser]
    assert_parser_returns_value(parser, string, expected_value)


def assert_parser_returns_value(parser, string, expected_value):
    """Check that a parser returns the value desired."""
    try:
        results = parser.parse_string(string, parse_all=True).as_list()
    except ParseException as e:
        print(e.explain())
        print(small_explain(e, string))
        raise
    assert len(results) == 1
    if expected_value is not MATCH:
        result = results[0]
        assert result == expected_value


def test_check_parsing_the_whole_structure_with_gaps_for_debug(ai, data):
    """This will parse the whole structure of e.g. DocumentData
    and show the gaps that are not understood, yet.
    """
    document_data = data.grammar.document_data.read(binary=False)
    source_string = replace_document_data_newlines_with_spaces(document_data)
    assert not "\n" in source_string
    parser: ParserElement = ai.data_object
    count = 0
    last_end = 0

    def print_line(start, end):
        """Print a line for debug."""
        line = source_string[start:end]
        if line and line[0] != " ":
            print("Missing Operator!!!!!!! ", end="")
        print(line)

    for tokens, start, end in parser.scan_string(source_string):
        count += 1
        if last_end != start:
            print_line(last_end, start)
        assert len(tokens) == 1
        print_end = min(end, start + 50)
        print(
            f"MATCH: {source_string[start:print_end]} {('', '...')[print_end != end]}"
        )
        last_end = end
    if last_end != len(source_string):
        print_line(last_end, len(source_string))
    assert count == 1, "We expect to find only one object."


@pytest.mark.parametrize(
    "string,tokens",
    [
        ("", []),
        (
            "197.85549500175 231 110.544897207401 77 0 0 /RealMatrix",
            [
                "197.85549500175",
                "231",
                "110.544897207401",
                "77",
                "0",
                "0",
                "/RealMatrix",
            ],
        ),
        #        ("%_/Document :\n", ["/Document", ":"]), # not for the tokenizer
        #        ("%binary-stuff\n", ["binary-stuff"]), # not for the tokenizer
        (
            "(U.S. Web Coated \(SWOP\) v2) /UnicodeString (/attributes/cm.profile) ,",
            [
                "(U.S. Web Coated \(SWOP\) v2)",
                "/UnicodeString",
                "(/attributes/cm.profile)",
                ",",
            ],
        ),  # line 7345 in document_data.txt
    ],
)
def test_tokenize(string, tokens):
    """Test the tokenization."""
    assert tokenize(string) == tokens


def test_benchmark(data_object):
    """Test that the benchmark works."""
    from inkai.benchmark.data_hierarchy import measure

    measure(data_object, seconds=0.001)


def test_can_decode_binary_data(data):
    """See if we can decode the example data."""
    import base64

    b = data.grammar.document_data_binary.read()
    if b.startswith(b"%"):
        b = b[1:]
    b = b.replace(b"\n%", b"\n").replace(b"\r%", b"\r").strip()
    decoded = base64.a85decode(b, foldspaces=True, adobe=True, ignorechars=b" \r\n\t")
    print(decoded[:10])


@pytest.mark.parametrize(
    "document,expected",
    [
        (BINARY_EXAMPLE, b"Hello World!"),
        (BINARY_EXAMPLE_WITH_NEWLINE, b"Hello World!"),
    ],
)
def test_issue_36_decode_binary_data(document, expected, data_object):
    """Defer decoding of binary data to speed up the parsing.

    See https://gitlab.com/inkscape/extras/extension-ai/-/issues/36
    """
    result = data_object.parse_string(document)
    assert len(result) == 1
    binary = result[0]
    assert binary.decode() == expected
