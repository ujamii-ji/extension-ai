# SPDX-FileCopyrightText: 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Extraction tools to get the AI text based data out of the ai files."""
from typing import Optional
import zlib
from io import BytesIO
from pypdf import PdfReader
from pypdf.constants import PageAttributes
import zstandard

ENCODING = "latin-1"


def extract_ai_privatedata(content: bytes) -> Optional[str]:
    """Parse the contents depending on the AI version.
    AI3-8: AI-Postscript (nothing to do)
    AI9+: PDF with embedded AIPrivateData* dict entries. These are concatenated
          across all pages. The contents always start with a header and a thumbnail.
          What happens afterwards differs from version to version:
      - AI9-10: /FlateDecode'd data after the thumbnail
      - AI CS - CS6, CC Legacy: %AI12_CompressedData followed by zlib-deflated content
      - AI CS2020: %AI24_ZStandard_Data followed by zst-deflated content
        (probably with pre-trained compression dictionary)

    If the file can be parsed, the content is returned as str.
    Otherwise, None is returned."""
    if content.startswith(b"%PDF-"):
        reader = PdfReader(BytesIO(content))

        # Concatenate all AIPrivateData blocks.
        for npage, _ in enumerate(reader.pages):
            page = reader.pages[npage]
            piece_info = page[PageAttributes.PIECE_INFO]
            ilpr = piece_info["/Illustrator"]["/Private"]  # type: ignore

            result = [b""]
            if "/NumBlock" in ilpr:
                keys = [
                    f"/AIPrivateData{b}" for b in range(1, int(ilpr["/NumBlock"]) + 1)
                ]
            else:
                keys = sorted([key for key in ilpr if key.startswith("/AIPrivateData")])
            for key in keys:
                data = ilpr[key]
                result += [data._data]  # pylint: disable=protected-access
        allresult = b"".join(result)
        decompressed = b""
        if b"%AI12_CompressedData" in allresult:
            zlibarchive = allresult.split(b"%AI12_CompressedData")
            assert chr(zlibarchive[1][0]) == "x"
            decompressed = zlib.decompress(zlibarchive[1])
            print("hi")
        elif b"%AI24_ZStandard_Data" in allresult:
            zstarchive = allresult.split(b"%AI24_ZStandard_Data")
            assert chr(zstarchive[1][0]) == "("
            # We have to specify the max output size. This is memory that
            # will definitely be allocated, so don't overdo it. The compression
            # ratio of zst is approx. 2.9 on typical text files, so this
            # seems like a reasonable guess.
            try:
                decompressed = zstandard.decompress(
                    zstarchive[1], max_output_size=len(zstarchive[1]) * 10
                )
            except zstandard.ZstdError:
                print("Unable to decompress data. Try increasing the output size")
                return None
        else:
            # In this case, each block of data is a separate archive.
            decompressed = b""
            for block in result:
                if b"H\x89" not in block:
                    continue  # The first block is some additional metadata which we don't need
                content2 = block[block.index(b"H\x89") :]
                try:
                    decompressed += zlib.decompress(content2)
                except zlib.error:
                    print("Unable to decompress zlib archive.")
                    return None
        return decompressed.decode(ENCODING)

    if content.startswith(b"%!PS-Adobe-3.0"):
        # very old illustrator file, already decompressed
        return content.decode(ENCODING)
    return None
