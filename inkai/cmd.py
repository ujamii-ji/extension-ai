# SPDX-FileCopyrightText: 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Conversion of AI (internal object storage) to SVG.

This is the interface to the command line.
"""
import sys
import inkex
from inkex.localization import inkex_gettext as _
from .extraction import extract_ai_privatedata
from typing import Optional
from .parser import ai
from .util import to_pretty_xml


class AIInput(inkex.InputExtension):
    """Load AI File"""

    # required for virtual env redirection through run_in_virtual_env.py
    bin_stdout = None

    def __init__(self) -> None:
        super().__init__()
        self.raw = b""

    def add_arguments(self, pars):
        """Add command line arguments"""
        # optional arguments, see https://stackoverflow.com/a/15008806/1320237
        pars.add_argument(
            "--debug-ai-data",
            action="store_true",
            dest="ai_data",
            default=False,
            help="Show the AI private data on STDERR that is used to construct the SVG file.",
        )
        pars.add_argument(
            "--pretty",
            action="store_true",
            dest="pretty_print",
            default=False,
            help="Create an SVG file that has several lines and looks pretty to read.",
        )

    def load(self, stream) -> Optional[inkex.SvgDocumentElement]:
        """Load the file. If we are unable to extract the AI data, return the unaltered
        PDF file."""
        raw = stream.read()
        parsed = extract_ai_privatedata(raw)
        if parsed is None:
            inkex.errormsg(_("Unable to extract AI data. Falling back to PDF."))
            return None
        if self.options.ai_data:
            for line in parsed.splitlines():
                print(line)
        return self.parse_ai_data(parsed)

    def parse_ai_data(self, parsed: str) -> inkex.SvgDocumentElement:
        """Translate the contents of the decoded AI private data to SVG."""
        parser = ai.AdobeIllustratorParser()
        svg: inkex.SvgDocumentElement = parser.parse_string(parsed, parse_all=True)
        return self.svg_to_string(svg)

    def svg_to_string(self, svg: inkex.SvgDocumentElement):
        """Convert the SvgDocumentElement to a string.

        This is mostly copied from inkex.elements._svg.SvgDocumentElement.tostring().
        """
        result = svg.tostring()
        if self.options.pretty_print:
            return to_pretty_xml(result)
        return result


def main():
    """Main entry point for the command line."""
    stdout = sys.stdout.buffer  # type: ignore
    # redirect print statements to stderr
    # see https://stackoverflow.com/a/15860430/1320237
    sys.stdout = sys.stderr
    AIInput().run(output=stdout)
    stdout.write(b"\n")


__all__ = ["main", "AIInput"]
