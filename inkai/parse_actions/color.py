# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Parse colors."""

from ..modular_parser import ParseActions
from pyparsing import ParseResults
from inkai.color import Color as color
from typing import Dict, Any


IS_STROKE = "is_stroke"


class Color(ParseActions):
    """Convert Adobe Illustrator Colors to Inkex colors."""

    names = [
        "color_name",
        "gray",
        "cyan",
        "magenta",
        "yellow",
        "black",
        "red",
        "green",
        "blue",
        "tint",
    ]

    def parse_color_value(self, p: ParseResults) -> float:
        """Convert a color to a float."""
        return float(p[0])

    def parse_stroke_color(self, p: ParseResults):
        """Parse the stroke color."""
        p[IS_STROKE] = True

    def parse_color(self, p: ParseResults):
        """Parse a whole color depending on what is given."""

        kw: Dict[str, Any] = {
            "stroke": IS_STROKE in p
        }  # for typing, see https://github.com/python/mypy/issues/5382
        if "color_name" in p:
            kw["name"] = p["color_name"]
        if "tint" in p:
            kw["tint"] = p["tint"]
        if "gray" in p:
            return color.from_gray(p["gray"], **kw)
        elif "red" in p:
            return color.from_rgb(p["red"], p["green"], p["blue"], **kw)
        elif "magenta" in p:
            return color.from_cmyk(
                p["cyan"], p["magenta"], p["yellow"], p["black"], **kw
            )
        elif p[-1] in ["Xz", "p"]:
            return None
        else:
            self.not_implemented(p)


__all__ = ["Color"]
