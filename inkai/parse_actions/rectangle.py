# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Convert SVG paths to rectangles if required."""
from ..modular_parser import ParseActions
from pyparsing import ParseResults
import inkex
from .common import IdGenerator
from pprint import pprint
from math import degrees


class Rectangle(ParseActions):
    """Turn certain paths into rectangles."""

    names = ["object_data", "object_shape"]

    def init(self):
        """Initialize the parse actions."""
        self.rects = IdGenerator()
        self.effects = IdGenerator()

    def make_rectangle_from_parameters(
        self, path: inkex.PathElement, params: dict
    ) -> inkex.BaseElement:
        """Create a rectangle from a set of parameters."""
        width = params["ai::Rectangle::Width"]
        height = params["ai::Rectangle::Height"]
        (x_min, x_max), (y_min, y_max) = path.bounding_box()
        left = (x_min + x_max) / 2 - width / 2
        top = (y_min + y_max) / 2 - height / 2
        angles = [self.get_angle(index, params) for index in range(4)]
        if angles[0] == angles[1] == angles[2] == angles[3]:
            # We can use an SVG rect as all angles are the same.
            rect = inkex.Rectangle.new(left, top, width, height)
            rect.set("rx", 0)
            rect.set("ry", angles[0])
            angle = params.get("ai::Rectangle::Angle", 0)
            if angle != 0:
                # Rotation can be accessed with
                # - object_data["ai::LiveShape"]["ai::Rectangle::Angle"] -> float
                # - object_data["BBAccumRotation"] -> str
                rect.set("transform", inkex.Transform(f"rotate({degrees(angle)})"))
            self.rects.set(rect)
            return [rect]
        # We have irregular corners so we need to use a path effect.
        # TODO
        path.set("sodipodi:type", "rect")
        path_effect = inkex.PathEffect.new(
            effect="fillet_chamfer",
            is_visible="true",
            lpeversion="1",
            satellites_param=f"F,0,0,1,0,{angles[0]},0,1 @ F,0,0,1,0,{angles[1]},0,1 @ F,0,0,1,0,{angles[2]},0,1 @ F,0,0,1,0,{angles[3]},0,1",
            unit="px",
            method="auto",
            mode="F",
            radius="0",
            chamfer_steps="1",
            flexible="false",
            use_knot_distance="true",
            apply_no_radius="true",
            apply_with_radius="true",
            only_selected="false",
            hide_knots="false",
        )
        path_effect.set_id(self.effects.new("rectangle-path-effect-"))
        path.set("inkscape:path-effect", "#" + path_effect.get_id())
        path.set_id(self.rects.new("rect"))
        return [path, path_effect]

    def get_angle(self, index: int, params: dict) -> float:
        """Return the angle at 0 <= index <= 3.

        This returns a positive float.
        If the corner is a 90 degree pointy corner, the return value is 0.
        """
        return round(params.get(f"ai::Rectangle::CornerRadius::{index}"), 8)  # type: ignore

    def parse_object(self, p: ParseResults):
        """Turn an object into a rectangle if this is required."""
        if not p.object_data:
            return
        pprint(p.object_data)
        live_shape = p.object_data.get("ai::LiveShape", {})
        if live_shape.get("ai::LiveShape::HandlerName") == "ai::Rectangle":
            shapes = p.object_shape
            assert shapes, "Expecting a path to turn into a rectangle."
            path: inkex.PathElement = shapes[
                0
            ]  # This is the path we want to turn into a rectangle.
            params = live_shape["ai::LiveShape::Params"]
            if (
                params.get("ai::Rectangle::Clockwise") != True
                or any(
                    params.get(f"ai::Rectangle::CornerType::{i}")
                    not in ("Normal", "Invalid")
                    for i in range(4)
                )
                or any(
                    params.get(f"ai::Rectangle::RoundingType::{i}")
                    not in ("Absolute", "Invalid")
                    for i in range(4)
                )
            ):
                # Tell the user about the file because we did not have any file like that.
                self.we_want_the_file("rectangle type", params)
                return p
            rect = self.make_rectangle_from_parameters(path, params)
            return ParseResults(rect)
        return p


__all__ = ["Rectangle"]
