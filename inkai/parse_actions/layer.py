# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Convert ."""

from ..modular_parser import ParseActions
from pyparsing import ParseResults
from .common import add_all_elements, IdGenerator
import inkex
from typing import List, Optional


class Layer(ParseActions):
    """Add objects to layers."""

    names = ["visible", "enabled", "layer_data"]

    def init(self):
        """Create layers."""
        self.ids = IdGenerator()
        self.all_layers: List[inkex.Layer] = []
        self.stack_of_open_layers: List[
            inkex.Layer
        ] = []  # operates like a stack of layers between BeginLayer and EndLayer
        self._layer_for_objects_outside: Optional[inkex.Layer] = None
        self.last_layer = None

    @property
    def current_layer(self) -> inkex.Layer:
        """Return the current layer we are creating."""
        if not self.stack_of_open_layers:
            if self._layer_for_objects_outside is None:
                self._layer_for_objects_outside = self.new_layer()
            return self._layer_for_objects_outside
        return self.stack_of_open_layers[-1]

    def new_layer(self) -> inkex.Layer:
        """Create a new layer."""
        id = self.ids.new()
        layer = inkex.Layer.new(f"Layer {id}")
        layer.set_id(f"g{id}")
        return layer

    def parse_begin_layer(self, p: ParseResults):
        """A new layer is created."""
        self.end_layer_for_object_without_layer()
        self.stack_of_open_layers.append(self.new_layer())
        if p.visible != "1":
            self.current_layer.set("style", "display:none")
        if p.enabled == "0":
            self.current_layer.set("sodipodi:insensitive", "true")

    def parse_end_layer(self, p: ParseResults):
        """A layer is fininshed."""
        self.last_layer = layer = self.stack_of_open_layers.pop()
        if self.stack_of_open_layers:
            # This is a nested layer
            print("nested layer")
            parent: inkex.Layer = self.stack_of_open_layers[-1]
            parent.add(layer)
        else:
            # this is a layer that goes into the SVG directly.
            print("root layer")
            self.all_layers.append(layer)

    def end_layer_for_object_without_layer(self):
        """The layer for objects outside of layers is finished."""
        if self._layer_for_objects_outside is not None:
            self.all_layers.append(self._layer_for_objects_outside)
            self._layer_for_objects_outside = None

    def parse_layer_name(self, p: ParseResults):
        """The current layer receibes a name."""
        self.current_layer.set("inkscape:label", p[0])
        return p

    def parse_layer(self, p: ParseResults):
        """Return the layer we created."""
        assert self.last_layer is not None, "We should have a layer!"
        return self.last_layer

    def add_elements_to_current_layer(self, p: ParseResults):
        """This adds all elements to the current layer."""
        add_all_elements(self.current_layer, p)

    def parse_object_definitions(self, p: ParseResults):
        """Parse all the objects inside of a layer."""
        self.add_elements_to_current_layer(p)

    def parse_object_outside_of_layer(self, p: ParseResults):
        """Put objects into layers."""
        self.add_elements_to_current_layer(p)
        return self.current_layer

    def parse_layer_data(self, p: ParseResults):
        """Parse the layer data that can be e.g. the id of the layer."""
        data = p.layer_data
        layer_id = data.get("AI10_ArtUID")
        if layer_id is not None:
            self.current_layer.set_id(layer_id)

    def parse_document(self, p: ParseResults):
        """Add layer information to the parse results"""
        self.end_layer_for_object_without_layer()
        p["layers"] = self.all_layers


__all__ = ["Layer"]
