# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Care for the <defs> element in the <svg>."""
from ..modular_parser import ParseActions
from pyparsing import ParseResults
import inkex


class SVGDefs(ParseActions):
    """Capture and place the <defs>."""

    collect = (inkex.PathEffect,)

    def init(self):
        """Initialize with no defs."""
        self.defs = []

    def collect_defs(self, p: ParseResults):
        """Collect all defs."""
        for element in p:
            if isinstance(element, self.collect):
                self.defs.append(element)

    def parse_document(self, p: ParseResults):
        """Add all defs to the SVG."""
        if self.defs:
            svg: inkex.SvgDocumentElement = p[0].getroot()
            defs = inkex.Defs.new(*self.defs)
            defs.set_id("defs1")
            svg.insert(0, defs)
        return p

    parse_object = collect_defs


__all__ = ["SVGDefs"]
