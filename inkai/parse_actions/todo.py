# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Convert ."""

from ..modular_parser import ParseActions
from pyparsing import ParseResults
from .errors import NotWorthImplementingWarning, ThisSectionInTheFileIsNotUnderstoodYet
import warnings


class LoudTODO(ParseActions):
    """Whenever a TODO is hit, this complains."""

    names = ["todo"]

    def parse_text_operator(self, p: ParseResults):
        """We skip these and leave a note to the reader."""
        warnings.warn(
            NotWorthImplementingWarning(
                "Text operators are deprecated, so we did not implement them. If you think, you need them, please open an issue!"
            )
        )
        return p[0]

    def parse_pattern(self, p: ParseResults):
        """We skip these and leave a note to the reader."""
        warnings.warn(NotWorthImplementingWarning("Patterns are not implemented, yet."))
        return p[0]

    def parse_todo(self, p: ParseResults):
        """Notify the user about the problematic match."""
        raise ThisSectionInTheFileIsNotUnderstoodYet(p)


__all__ = ["LoudTODO"]
