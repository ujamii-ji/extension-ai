# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Convert ."""

from ..modular_parser import ParseActions
from pyparsing import ParseResults
from typing import Dict, List, Tuple
import base64
import re
from ..parser.text_element_ai_11 import AI11BasicTextDocumentParser


class TextElementAI11(ParseActions):
    names = ["text_document_encoding", "text_document_type", "text_document_content"]

    def objectify_text_element(self, decoded: bytes):
        """Parse the decoded text element into a Python object structure"""
        return AI11BasicTextDocumentParser(decoded.decode("latin-1")).parse()

    def parse_encoded_text(self, p: ParseResults):
        """Decode the text content using the specified encoding"""
        if p["text_document_encoding"][0] == "ASCII85Decode":
            data = re.sub("(^%)|((\r\n|\n)%?)", "", p["text_document_content"]).replace(
                "\r\n", ""
            )
            decoded = base64.a85decode(data, adobe=True)
        else:
            self.not_implemented(p)
        return [p["text_document_type"], self.objectify_text_element(decoded)]

    def parse_text_element_ai11(self, p: ParseResults):
        """Combine the results of all elements in a BeginTextDocument"""
        # TODO


__all__ = ["TextElementAI11"]
