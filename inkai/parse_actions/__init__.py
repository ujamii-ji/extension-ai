# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Parse actions are performed while parsing the grammar.

These return nicer values that are usable straight away.
"""
from .todo import LoudTODO
from .text_element_ai_11 import TextElementAI11
from .svg_root_document import SVGRootDocument
from .post_script import PostScript
from .path import Path
from .layer import Layer
from .header import Header
from .group import Group
from .color import Color
from .errors import NotWorthImplementingWarning, ThisSectionInTheFileIsNotUnderstoodYet
from .data_hierarchy import DataHierarchy
from .rectangle import Rectangle
from .defs import SVGDefs

__all__ = [
    "LoudTODO",
    "DataHierarchy",
    "TextElementAI11",
    "SVGRootDocument",
    "PostScript",
    "Path",
    "Layer",
    "Header",
    "Group",
    "Color",
    "NotWorthImplementingWarning",
    "ThisSectionInTheFileIsNotUnderstoodYet",
    "Rectangle",
    "SVGDefs",
]
