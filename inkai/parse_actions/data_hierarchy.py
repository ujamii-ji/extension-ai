# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Parse the AI header."""

from ..modular_parser import ParseActions
from pyparsing import ParseResults, ParseException
from .common import small_explain
import base64
import re
import inkex
from typing import List
from ..parser import data_hierarchy


TYPE = "type"

NEWLINE_REGEX = re.compile("(^|\n|\r\n?)%_?")


def replace_document_data_newlines_with_spaces(string):
    """Replace newlines with spaces."""
    return NEWLINE_REGEX.sub(" ", string).strip()


class TypedDict(dict):
    """A dict with certain attributes."""

    def __init__(self, values: dict):
        """Create a new dict with a type."""
        self._type = values.pop(TYPE)
        self.update(values)

    @property
    def type(self):
        """The type of the dict."""
        return self._type


class Point(inkex.Vector2d):
    """Subsclass and create new methods.

    TODO: move the methods to inkex.

    Used for /RealPoint
    """

    def __eq__(self, other):
        """Am I equal to you?"""
        return (
            isinstance(other, inkex.ImmutableVector2d)
            and self.x == other.x
            and self.y == other.y
        )

    def __hash__(self):
        """Compute the hash."""
        return hash(self.x) ^ hash(self.y)


class PointRelToROrigin(Point):
    """Used for /RealPointRelToROrigin."""


class Document(list):
    """For parsing /Document, just pretty with a subclass."""


class Binary:
    """A /Binary object."""

    def __init__(self, data: List[str]):
        """Create a new binary object with some data."""
        self._data = data

    def decode(self) -> bytes:
        """Return the binary data."""
        data = self._data[0] if len(self._data) == 1 else "\n".join(self._data)
        return base64.a85decode(data, adobe=True, foldspaces=True)


FLAGS = {
    "/Recorded": {
        "recorded": True,
    },
    "/NotRecorded": {
        "recorded": False,
    },
}


def is_flag(flag: dict):
    """Whether this one is a flag."""
    return flag in FLAGS.values()


class DataHierarchy(ParseActions):
    """Parse actions to create a objects from DocumentData and other XML-like strings."""

    names = [
        "data_dict_key",
        "data_dict_value",
        "data_dict_type",
        "x",
        "y",
        "data_flag",
        "data_real_point_class",
    ]
    flags = FLAGS

    def parse_data_int(self, p: ParseResults):
        """Parse an int."""
        return int(p[0])

    def parse_data_real(self, p: ParseResults):
        """Parse a float."""
        return float(p[0])

    def parse_data_real_matrix(self, p: ParseResults):
        """Parse a float."""
        return ParseResults([list(map(float, p.as_list()))])

    def parse_data_bool(self, p: ParseResults):
        """Parse a bool."""
        return bool(int(p[0]))

    def parse_data_string(self, p: ParseResults):
        """Parse the /String command."""
        return ParseResults([(p[0] if p else "")])

    def parse_data_dict_entry(self, p: ParseResults):
        """Create a dict with a key and a value."""
        key = p.data_dict_key
        value = p.data_dict_value
        return {key: value}

    def parse_data_dict_type(self, p: ParseResults):
        """Set the type of the dict."""
        return {TYPE: p.data_dict_type}

    def parse_data_flag(self, p: ParseResults):
        """Set a flag."""
        flag = self.flags.get(p.data_flag)
        if flag is None:
            self.not_implemented(p)
        return flag

    def parse_data_name(self, p: ParseResults):
        """A name of an object."""
        return {"name": p[0]}

    def parse_data_dict(self, p: ParseResults):
        """Join several entries together."""
        result = {k: v for d in p for k, v in d.items()}
        return TypedDict(result)

    def parse_data_array(self, p: ParseResults):
        """Parse an array."""
        return ParseResults([p.as_list()])

    def parse_data_binary_encoding(self, p: ParseResults):
        """Check the encoding."""
        if p[0] != "ASCII85Decode":
            self.not_implemented(p)
        return ParseResults([])

    @staticmethod
    def parse_data_binary_data(p: ParseResults):
        """Decode binary data."""
        return Binary(p[:1])

    def parse_benchmark_pyparsing_parsed_data_hierarchy_section(self, p: ParseResults):
        """Decode the whole section and return an object.

        This uses pyparsing.
        """
        section_without_newlines = replace_document_data_newlines_with_spaces(p[0])
        try:
            return self.parser.data_object.parse_string(section_without_newlines)
        except ParseException as e:
            print(small_explain(e, section_without_newlines))
            raise

    def parse_parsed_data_hierarchy_section(self, p: ParseResults):
        """Decode the whole section and return an object.

        This uses inkai.parser.data_hierarchy.
        """
        section = p[0]
        parser = data_hierarchy.DataHierarchyParser()
        return parser.parse_string(section)

    def parse_data_real_point(self, p: ParseResults):
        """Parse a /RealPoint."""
        cls = p.data_real_point_class
        if cls == "/RealPoint":
            return Point(p.x, p.y)
        elif cls == "/RealPointRelToROrigin":
            return PointRelToROrigin(p.x, p.y)
        self.not_implemented(p)

    def parse_data_document(self, p: ParseResults):
        """Parse a /Document."""
        return ParseResults([Document(p)])


__all__ = [
    "DataHierarchy",
    "Binary",
    "Point",
    "PointRelToROrigin",
    "TypedDict",
    "Document",
]
