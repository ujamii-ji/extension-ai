# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Convert ."""

from ..modular_parser import ParseActions
from pyparsing import ParseResults
from inkex import SvgDocumentElement, Transform, Layer
from typing import Dict
from .. import cmd


class SVGRootDocument(ParseActions):
    """Create and configure the SVG root element."""

    names = ["header", "document"]

    default_width = 100
    default_height = 100

    def init(self):
        """Create an SVG document."""
        self.result = cmd.AIInput.get_template(
            width=self.default_width, height=self.default_height
        )
        self.svg: SvgDocumentElement = self.result.getroot()

    def parse_header(self, p: ParseResults):
        """Use the header to get some properties for the SVG."""
        headers: Dict[str, str] = p.header
        llx, lly, urx, ury = map(float, headers["TileBox"])
        self.width = width = int(llx + urx)
        self.height = height = int(lly + ury)
        self.svg.set("width", width)
        self.svg.set("height", height)
        self.svg.set("viewBox", f"0 0 {width} {height}")

    def parse_document(self, p: ParseResults):
        """Create an SVG root element and add properties."""
        layer: Layer
        for layer in p.get("layers", []):
            layer.transform = Transform(f"translate(0, {self.height}) scale(1, -1)")
            self.svg.add(layer)
            self.svg.namedview.set("inkscape:current-layer", layer.get_id())
        return self.result


__all__ = ["SVGRootDocument"]
