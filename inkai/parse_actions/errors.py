# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Error classes and warnings for the parse actions."""

from ..modular_parser.errors import MissingParserFeature


class ThisSectionInTheFileIsNotUnderstoodYet(MissingParserFeature):
    """The place that was reached in the AI file is not understood, yet and needs implementing. It was specially left blank to do it later. Please report the example data."""


class NotWorthImplementingWarning(UserWarning):
    """This feature is not worth implementing.

    We can just ignore it. If you need it, please open an issue.
    """


__all__ = ["NotWorthImplementingWarning", "ThisSectionInTheFileIsNotUnderstoodYet"]
