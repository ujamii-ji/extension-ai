# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Convert ."""

from ..modular_parser import ParseActions
from pyparsing import ParseResults
from inkex import PathElement
import inkex


NODETYPES = "sodipodi:nodetypes"


class Path(ParseActions):
    """Parse actions to build a path object."""

    names = ["x", "y", "path_render"]

    def init(self):
        """Create new Path parse actions."""
        self._d = []
        self.sodipodi_nodetypes = ""
        self.close_path = False

    @property
    def d(self) -> str:
        """Return the d attribute of a path."""
        return " ".join(self._d)

    def parse_path_operator(self, p: ParseResults):
        """Parse path operators.

        See https://gitlab.com/inkscape/extras/extension-ai/-/merge_requests/10#note_1310433295
        """
        operator = p[-1]
        self.sodipodi_nodetypes += "sc"[operator < "Z"]
        self._d += [operator.upper()] + p[:-1]

    parse_move_to = parse_path_operator

    def parse_path_object(self, p: ParseResults):
        """Build a path from the current data."""
        path = inkex.Path(self.d)
        if self.close_path:
            path.close()
        path_element = PathElement.new(path)
        path_element.set(NODETYPES, self.sodipodi_nodetypes)
        self.init()
        return path_element

    def parse_path_render(self, p: ParseResults):
        """Parse the path render - this determines closing, fill and stroke.

        See page 54 AI Spec.
        """
        print(f"path_render = {p.path_render}")
        if p.path_render[0] in ("N", "f", "s", "b"):
            self.close_path = True

    def parse_compound_path(self, p: ParseResults):
        """Merge paths together."""
        path_elements = [path for path in p if isinstance(path, PathElement)]
        print(f"Merging {len(path_elements)} paths into one compound path.")
        if not path_elements:
            return
        compound_path = path_elements[0].path
        compound_nodetypes = path_elements[0].get(NODETYPES)
        for path_element in path_elements[1:]:
            compound_path += path_element.path
            compound_nodetypes += path_element.get(NODETYPES)
        compound_path_element = PathElement.new(compound_path)
        compound_path_element.set(NODETYPES, compound_nodetypes)
        return compound_path_element


__all__ = ["Path"]
