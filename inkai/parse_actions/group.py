# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Convert ."""

from ..modular_parser import ParseActions
from pyparsing import ParseResults
from .common import add_all_elements
import inkex


class Group(ParseActions):
    """Parse actions to create groups of objects."""

    def init(self):
        """Initialize parsing."""
        self.last_id = 0

    def parse_group_object(self, p: ParseResults):
        """Create a group object and include objects inside."""
        self.last_id += 1
        group = inkex.Group()
        group.set_id(f"g{self.last_id}")
        add_all_elements(group, p)
        print(f"new group {group} of size {len(group)}")
        if len(group) == 0:
            return
        if len(group) == 1:
            return group[0]
        return group


__all__ = ["Group"]
