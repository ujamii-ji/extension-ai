# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Parse the AI header."""

from ..modular_parser import ParseActions
from pyparsing import ParseResults


class Header(ParseActions):
    """Parse actions to create a header accessible like a dict."""

    def header(self, name: str, value: ParseResults):
        """Create a mapping of header to name."""
        return {name: value[0]}

    def parse_header_comment_key(self, p: ParseResults):
        """Remove the clutter from a header comment."""
        return p[0]

    def parse_header_comment(self, p: ParseResults):
        """Create a header entry from the parse results of a header line."""
        key = p[0]
        value = p[1:]
        return {key: value}

    def parse_header(self, p: ParseResults):
        """Join several headers together."""
        return {k: v for d in p for k, v in d.items()}

    def parse_document_structuring_convention(self, p: ParseResults):
        """Parse the DSC like a header."""
        return self.header("DSC", p)

    def parse_header_comment_plus(self, p: ParseResults):
        """Parse a + header.

        TODO: They are ignored for now.
        """
        return "+"


__all__ = ["Header"]
