# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Parse the Post Script syntax like text."""

from ..modular_parser import ParseActions
from pyparsing import ParseResults
import re


class PostScript(ParseActions):
    """This set of parse actions are defined in the PostScript Language Document Structuring Conventions Specification"""

    ESCAPE_REGEX = re.compile("\\\\[^0-9]|\\\\[0-9][0-9][0-9]")
    mapping = {"r": "\r", "n": "\n", "t": "\t"}

    def parse_text(self, p: ParseResults):
        """The escape mechanism is mentioned on page 36 of the PostScript specification.

            (This is a special character \262 using the \\ mechanism)

        Several escapes are possible.
        """
        text = p[0]
        return self.unescape_text(text)

    @classmethod
    def unescape_text(cls, text: str):
        """The escape mechanism is mentioned on page 36 of the PostScript specification."""

        def unescape(match):
            """Unescape an escaped character."""
            s = match[0]
            assert s[0] == "\\"
            if s[1] in "0123456789":
                return chr(int(s[1:]))
            else:
                return cls.mapping.get(s[1], s[1])

        return cls.ESCAPE_REGEX.sub(unescape, text)


__all__ = ["PostScript"]
