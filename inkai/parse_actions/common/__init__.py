# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Common functions for all parse actions."""

from inkex import BaseElement
from typing import List
from pyparsing import ParseResults, ParseException
from .ids import IdGenerator


def is_svg_element(element) -> bool:
    """Return whether the object can be used in the SVG."""
    return isinstance(element, BaseElement)


def svg_elements(p: ParseResults) -> List[BaseElement]:
    """Return all the svg elements from parse results."""
    return [element for element in p if is_svg_element(element)]


def add_all_elements(root: BaseElement, children: ParseResults):
    """Add all children to the root."""
    added = set()
    for child in svg_elements(children):
        if child in added:
            continue
        root.add(child)
        added.add(child)
    print(
        f"Adding elements to {root.get('id')}: {', '.join(e.get('id', e.tag_name) for e in added)}"
    )


def small_explain(e: ParseException, string: str, size=60):
    """Explain a parse excepion in a small context of size characters."""
    off = size // 2
    start = max(0, e.col - off)
    end = min(len(string), e.col + off)
    return f"""{string[start:end]}
{" " * (min(e.col, off) - 1) + "^"}"""


__all__ = [
    "add_all_elements",
    "is_svg_element",
    "svg_elements",
    "small_explain",
    "IdGenerator",
]
