# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Generate ids for objects."""
from inkex import BaseElement


class IdGenerator:
    """Create ids of objects."""

    def __init__(self):
        """Create an id generator."""
        self._last_id = 0

    def new(self, prefix="") -> str:
        """Create a new id with a prefix."""
        self._last_id += 1
        return f"{prefix}{self._last_id}"

    def set(self, element: BaseElement):
        """Set the id of an element."""
        element.set_id(self.new(element.tag_name))


__all__ = ["IdGenerator"]
