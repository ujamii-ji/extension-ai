# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Parser errors."""


class ParseException(Exception):
    """The parser has an error."""


class ReachedEndOfFile(ParseException):
    """The parser reached the end of the file."""


class UnexpectedStartOfLine(ParseException):
    """Raised when we the start of the line should be different."""


class UnexpectedToken(ParseException):
    """Raised when we encounter an token that should be something different."""


class UnknownToken(UnexpectedToken):
    """Raised when we encounter a token that we never saw before.

    This means that we need to extend the parser.
    """


__all__ = [
    "ParseException",
    "ReachedEndOfFile",
    "UnknownToken",
    "WrongToken",
    "UnexpectedStartOfLine",
]
