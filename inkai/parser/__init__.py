# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This package contains the parsers for different parts
of the Adobe Illustrator File Format.
"""
# Resolve circular import problems by explicit import
from .data_hierarchy import DataHierarchyParser
from .ai import AdobeIllustratorParser

__all__ = ["DataHierarchyParser", "AdobeIllustratorParser"]
