# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Parser for AI 11 text elements."""

from typing import Dict, List, Tuple, Union


ObjectType = Dict[str, "ValueType"]
ValueType = Union[str, bytes, bool, int, float, List["ValueType"], ObjectType]


class AI11BasicTextDocumentParser:
    """Parses an AI11TextDocument without external libraries for
    performance comparison.

    The grammar of this parser is described in
    docs/specification_amendments/text_documents.md.

    TODO: Write a unit test to compare the output with an equivalent
    pyparsing parser to ensure correctness.

    This is effectively a state machine, where the state is tracked
    by the call stack, which also ensures that the recursive nature
    of the data is properly mapped."""

    def __init__(self, text: str) -> None:
        self.text = text
        self.pos = 0
        self.endpos = len(text)

    def parse(self) -> Dict:
        return self.parse_object()

    @property
    def nextspace(self) -> int:
        """Gets the position of the next space. Runtime of find
        on strings is independent of the length of the string."""
        return self.text.find(" ", self.pos)

    @property
    def current(self) -> str:
        """Get the current character"""
        return self.text[self.pos]

    @property
    def current_word(self) -> Tuple[int, str]:
        """Get the (pos of the next space, substring starting with self.pos until the next space)."""
        space = self.nextspace
        return space, self.text[self.pos : space]

    def increment_if_at_current(self, substring: str) -> bool:
        """Check if a substring occurs at the current
        position. If true, increment position by its length.

        The implementation with
            string.startswith(substring, startposition)
        is constant time in len(string), while
            string[startposition:].startswith(substring)
        is linear time."""
        if self.text.startswith(substring, self.pos):
            self.pos += len(substring)
            return True
        return False

    def increment_if_char_at_current(self, character: str) -> bool:
        """Same as at_current for single characters.

        For single char comparison, this is faster by a factor of 3."""
        if self.current == character:
            self.pos += 1
            return True
        return False

    def unexpected_token(self):
        """Raise an error that an unexpected token was found."""
        import inspect

        raise ValueError(
            f"{inspect.currentframe().f_back.f_code.co_name}: "  # type: ignore
            f"Unexpected token at pos {self.pos}: {self.current}. "
            f"Context: {self.text[self.pos-8:self.pos+8]}."
        )

    def parse_object(self) -> ObjectType:
        """Parse an object. Note that the opening tag has already been
        consumed by parse_value (or is not present on the root object)"""
        result: ObjectType = {}
        while True:
            if self.pos == self.endpos - 2:
                return result
            if self.increment_if_char_at_current(" "):  # Ignore spaces
                continue
            elif self.increment_if_at_current(">>"):
                # Terminate subobject
                return result
            elif self.current == "/":  # This is a flag
                space, key = self.current_word
                self.pos = space
                value = self.parse_value()
                result[key] = value
            else:
                self.unexpected_token()

    def parse_list(self) -> List[ValueType]:
        """Parse a list. Note that the opening tag has already been
        consumed by parse_value"""
        result: List[ValueType] = []
        while True:
            if self.increment_if_char_at_current(" "):  # Ignore spaces
                continue
            elif self.increment_if_char_at_current("]"):
                return result
            else:
                # Only remaining option.
                result.append(self.parse_value())

    def parse_value(self) -> ValueType:
        """Parse a value. Primitive values are parsed in this method;
        lists, strings and subobjects are referred to their own methods"""
        while True:
            if self.increment_if_char_at_current(" "):  # Ignore spaces
                continue
            elif self.increment_if_at_current("<<"):
                return self.parse_object()
            elif self.increment_if_char_at_current("["):
                return self.parse_list()
            elif self.increment_if_char_at_current("("):
                return self.parse_string()
            else:
                result: ValueType
                # The current word can be processed directly.
                # It's a number, datatype or bool.
                space, word = self.current_word
                if word in ("true", "false"):
                    result = word == "true"
                elif word[0] == "/":
                    result = word
                else:
                    try:
                        result = float(word) if "." in word else int(word)
                    except:
                        self.unexpected_token()
                self.pos = space
                return result

    def parse_string(self) -> str:
        """Strings are wrapped in (), and brackets are escaped with \\
        """
        current = self.current
        initial = self.pos
        while True:
            next = self.text[self.pos + 1]
            if next == ")" and current != "\\":
                self.pos += 2
                result = self.text[initial : self.pos - 1]
                if result.startswith("þÿ"):
                    if "\00" in result:
                        try:
                            return (
                                (result[3:] + "\00").encode("latin-1").decode("utf-16")
                            )
                        except:  # Encoding error
                            return result[2:]
                    else:
                        return result[2:]
                else:
                    return result
            # Increment by 1 character.
            current = next
            self.pos += 1


__all__ = ["AI11BasicTextDocumentParser"]
