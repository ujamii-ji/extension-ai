# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Parse line by line."""

from .error import ReachedEndOfFile
import io


class LineParser:
    """Parse line by line."""

    @classmethod
    def from_string(cls, string: str):
        """Create a new LineParser from a string."""
        file = io.StringIO(string)
        return cls(file)

    def __init__(self, file):
        """Create a new linewise parser.

        file is something that has a readline() method.
        """
        self._file = file
        # the current line to process
        self._remaining_line = ""
        self._remaining_line_used = 0
        # the next line to process sometimes
        self._next_line = ""
        self._next_line_used = 0
        # the current line as attributes
        self.current_line = ""
        self.current_line_end = ""
        self.current_line_number = 0

    def read_line(self):
        """Read a line from the file."""
        # prepare, read a line
        if self._next_line:
            self._remaining_line_used = line_start = self._next_line_used
            self._remaining_line = remaining_line = self._next_line
            self._next_line = ""
        if self._remaining_line_used >= len(self._remaining_line):
            self._remaining_line_used = line_start = 0
            self._remaining_line = remaining_line = self._file.readline()
        else:
            remaining_line = self._remaining_line
            line_start = self._remaining_line_used
        if remaining_line == "":
            raise ReachedEndOfFile(
                f"Reached end of file after line {self.current_line_number}."
            )
        # find a new line inside
        r_newline_index = remaining_line.find("\r", line_start)
        n_newline_index = remaining_line.find("\n", line_start)
        if r_newline_index == -1:
            if n_newline_index == -1:
                # EOF
                newline_end = line_end = len(remaining_line)
                newline = ""
            else:
                # \n
                line_end = n_newline_index
                newline_end = n_newline_index + 1
                newline = "\n"
        elif n_newline_index > r_newline_index or n_newline_index == -1:
            line_end = r_newline_index
            # there is a \r and there might be a \n behind it
            if r_newline_index + 1 == len(remaining_line):
                # \rEOF or \r as line end read before \n
                self._next_line = next_line = self._file.readline()
                if next_line:
                    if next_line[0] == "\n":
                        self._next_line_used = 1
                        newline = "\r\n"
                    else:
                        newline = "\r"
                        self._next_line_used = 0
                else:
                    # \rEOF
                    self._next_line_used = 0
                    newline = "\r"
                newline_end = r_newline_index + 1
            elif n_newline_index == r_newline_index + 1:
                # \r\n
                newline_end = r_newline_index + 2
                newline = "\r\n"
            else:
                # \r
                newline_end = r_newline_index + 1
                newline = "\r"
        else:
            # \n
            line_end = n_newline_index
            newline_end = n_newline_index + 1
            newline = "\n"
        # end: set all variables
        self._remaining_line_used = newline_end
        self.current_line = current_line = remaining_line[line_start:line_end]
        self.current_line_end = newline
        self.current_line_number += 1
        return current_line


__all__ = ["LineParser"]
