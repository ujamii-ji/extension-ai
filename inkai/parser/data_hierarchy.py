# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create a simple parser for the data hirarchy."""

from typing import List
from ..parse_actions.post_script import PostScript
from pyparsing import ParseResults
from ..parse_actions.data_hierarchy import (
    Point,
    PointRelToROrigin,
    FLAGS,
    is_flag,
    Binary,
)
from .error import UnknownToken, UnexpectedToken, UnexpectedStartOfLine
from .lines import LineParser
import io
import re
import base64

TokenList = List[str]
TOKEN_REGEX = re.compile(r"[^ \n\r()]+|\((?:[^()]|\\.)*(?<!\\)\)")


def tokenize(string) -> TokenList:
    """Return the tokens."""
    result = TOKEN_REGEX.findall(string)
    return result


class DataHierarchyParsingProcess:
    """Parsing one string/list of tokens."""

    @classmethod
    def from_string(cls, string: str):
        """Create a new DataHierarchyParsingProcess from a string."""
        line_parser = LineParser.from_string(string)
        return cls(line_parser)

    def __init__(self, line_parser: LineParser):
        """Create a new process to parse."""
        self._parser = line_parser
        self._tokens: List[str] = []
        self._index = 0
        self._lines_start_with_percent = False
        self.methods = {
            "/ArtDictionary": self.read_dict,
            "/Dictionary": self.read_dict,
            "/XMLNode": self.read_dict,
            "/XMLUID": self.read_xmluid,
            "/Real": float,
            "/Int": int,
            "/RealPoint": Point,
            "/RealPointRelToROrigin": PointRelToROrigin,
            "/RealMatrix": lambda *tokens: list(map(float, tokens)),
            "/String": self.read_text,
            "/UnicodeString": self.read_text,
            "/Array": self.read_array,
            "/Recorded": lambda: FLAGS["/Recorded"],
            "/NotRecorded": lambda: FLAGS["/NotRecorded"],
            "/Bool": "0".__ne__,
            "/Document": self.read_document,
            "/Name": lambda name: {"name": self.read_text(name)},
            "/Binary": self.read_binary,
        }

    def next_line(self):
        """Return the next line."""
        if self._has_tokens():
            raise UnexpectedToken(
                f"To read a line, I expect all tokens to be used: {self._tokens} {self._error_position}"
            )
        line = self._parser.read_line()
        if line.startswith("%") and self._parser.current_line_number == 1:
            self._lines_start_with_percent = True
        if self._lines_start_with_percent:
            if line[0] != "%":
                raise UnexpectedStartOfLine(
                    f"Line {self.line} should start with % but it does not!"
                )
            return line[1:]
        return line

    def _has_tokens(self) -> bool:
        """Whether there are tokens to use."""
        return len(self._tokens) != self._index

    def read_data_object(self):
        """Read a new data object from the tokens."""
        args: TokenList = []
        token = self.next()
        while not token.startswith("/"):
            args.append(token)
            token = self.next()
        method = self.methods.get(token)
        if method is None:
            return self.default_token(token)
        try:
            return method(*args)  # type: ignore
        except TypeError:
            raise UnexpectedToken(f"Found {args} {token} {self._error_position}")

    @property
    def _error_position(self):
        """Return a message."""
        return f" at token {self.index} in line {self.line}."

    def default_token(self, token: str):
        """Raise an error if we do not know what this is."""
        raise UnknownToken(
            f"{token} is unknown to {self.__class__.__name__} {self._error_position}"
        )

    def _ensure_tokens_are_there(self):
        """Make sure we have tokens.

        This also skips % or %_ at the start to make sure we
        can read the tokens.
        """
        while not self._has_tokens():
            line = self.next_line()
            if line.startswith("_"):
                line = line[1:]
            self._tokens = tokenize(line)  # TODO: always just read one token.
            self._index = 0

    def next(self):
        """Return the next token."""
        self._ensure_tokens_are_there()
        token = self._tokens[self.index]
        self._index += 1
        return token

    @property
    def index(self) -> int:
        """How much of the tokens have been processed."""
        return self._index

    @property
    def line(self) -> int:
        """Return the current line index."""
        return self._parser.current_line_number

    def skip(self, expected_token: str):
        """Skip a token."""
        token = self.next()
        if token != expected_token:
            raise UnexpectedToken(
                f"Expected {expected_token} but found {token} {self._error_position}"
            )

    def is_next(self, token: str):
        """Whether a token is next."""
        self._ensure_tokens_are_there()
        return self._tokens[self._index] == token

    def read_dict(self):
        """Create a dictionary."""
        self.skip(":")
        result = {}
        while not self.is_next(";"):
            value = self.read_data_object()
            if is_flag(value):
                result.update(value)
            else:
                key = self.read_text(self.next())
                result[key] = value
            self.skip(",")
        self.skip(";")
        return result

    def read_text(self, text: str = "") -> str:
        """Return the converted text."""
        if not text:
            return text
        if text[0] != "(" or text[-1] != ")":
            raise UnexpectedToken(
                f"Expected text in () but found {repr(text)} {self._error_position}"
            )
        return PostScript.unescape_text(text[1:-1])

    def read_xmluid(self):
        """Read a /XMLUID."""
        self.skip(":")
        result = self.read_text(self.next())
        self.skip(";")
        return result

    def read_array(self):
        """/Array"""
        result = []
        self.skip(":")
        while not self.is_next(";"):
            result.append(self.read_data_object())
            self.skip(",")
        self.skip(";")
        return result

    def read_document(self):
        """/Document"""
        self.skip(":")
        result = []
        while not self.is_next(";"):
            result.append(self.read_data_object())
            flag = self.read_data_object()
            if not is_flag(flag):
                raise UnexpectedToken(
                    f"Expected flag but got {flag} {self._error_position}"
                )
            self.skip(",")
        self.skip(";")
        return result

    def read_binary(self):
        self.skip(":")
        self.skip("/ASCII85Decode")
        self.skip(",")
        data = []
        line = self.next_line()
        while line[-2:] != "~>":
            data.append(line)
            line = self.next_line()
        data.append(line)
        self.skip(";")
        return Binary(data)


class DataHierarchyParser:
    """A parser for a data hierarchy.

    This provides the same interface as we need from pyparsing."""

    def parse_string(self, string: str, parse_all: bool = False):
        """Parse the data into a hierarchical object structure."""
        file = io.StringIO(string)
        return self.parse_file(file, parse_all=parse_all)

    def parse_file(self, file, parse_all: bool = False):
        """Parse a file."""
        parser = LineParser(file)
        process = DataHierarchyParsingProcess(parser)
        data_object = process.read_data_object()
        return ParseResults([data_object])
