# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This file contains the grammar of the Adobe Illustrator Files.

See also 
- the Specification: http://www.idea2ic.com/File_Formats/Adobe%20Illustrator%20File%20Format.pdf
- the tests: ../tests/test_grammar.py

Each element of the grammar should correspond to the named element of the BNF Syntax as
written in the Specification, pages 135+.

Terms
-----

PS Spec refers to PostScript Language Document Structuring Conventions Specification Version 3.0
AI Spec refers to Adobe Illustrator File Format Specification 23 February 1998

"""
import re
from pyparsing import (
    alphas,
    And,
    Char,
    CharsNotIn,
    Combine,
    Forward,
    Group,
    Literal,
    NotAny,
    nums,
    OneOrMore,
    Opt,
    ParserElement,
    pyparsing_common,
    Regex,
    SkipTo,
    Suppress,
    Word,
    ZeroOrMore,
    NoMatch,
)
from ..modular_parser import Parser
from typing import Optional
from inkex import SvgDocumentElement
from .. import parse_actions


# Set the newline character as part of the grammar.
# see https://pyparsing-docs.readthedocs.io/en/latest/HowToUsePyparsing.html#usage-notes
ParserElement.set_default_whitespace_chars(" \t")


class AdobeIllustratorParser(Parser):
    """Parser for the Adobe Illustrator file format.

    The tokens of the parser are public attributes.
    This way, you can add parse actions to them.
    """

    set_results_name_from = ["todo"]

    class Characters:
        """A shortcut for a set of characters."""

        newline = "\n\r"
        whitespace = " \t"
        colon = ":"

    class Text:
        """A shortcut to access fixed text strings."""

        end_comments = "%%EndComments"
        eof = "%%EOF"
        include_font = "%%IncludeFont:"
        begin_gradient = "%AI5_BeginGradient:"
        end_gradient = "%AI5_EndGradient"

    class Regex:
        """A collection of regular expressions."""

        newline = "(?:\r\n?|\n)+"

    def open_and_close_token(
        self, start: str, inner_parser: ParserElement, stop: Optional[str] = None
    ):
        """Wrap a inner_parser into start and stop commands."""
        if stop is None:
            stop = start.upper()
        return (
            Suppress(start)  # Group Operators - page 63 AI Spec
            + self.newline
            + inner_parser
            + Suppress(stop)  # Group Operators - page 63 AI Spec
            + self.newline
        )

    def parse_string(self, string: str, parse_all: bool = True) -> SvgDocumentElement:
        """Parse a Adobe Illustrator document into an SVG element.

        This also resets the parser so you can use it again.
        """
        parse_result: ParseResults = self.document.parse_string(string, parse_all=parse_all)  # type: ignore
        svg = parse_result.document.getroot()
        self.reset()
        return svg

    def add_default_parse_actions(self):
        """Add a set of default parse actions."""
        self.actions.add(parse_actions.PostScript())
        self.actions.add(parse_actions.DataHierarchy())
        self.actions.add(parse_actions.Header())
        self.actions.add(parse_actions.TextElementAI11())
        self.actions.add(parse_actions.Color())
        self.actions.add(parse_actions.Path())
        self.actions.add(parse_actions.Rectangle())
        self.actions.add(parse_actions.LoudTODO())
        self.actions.add(parse_actions.Layer())
        self.actions.add(parse_actions.Group())
        self.actions.add(parse_actions.SVGRootDocument())
        self.actions.add(parse_actions.SVGDefs())

    def create_tokens(self):
        """Create all tokens."""
        # design pattern: template method
        self.create_todos()
        self.create_characters()
        self.create_numbers()
        self.create_text()
        self.create_procset()
        self.create_font_encoding()
        self.create_colors()
        self.create_variables()
        self.create_data_hierarchy()
        self.create_header()
        self.create_color()
        self.create_gradient_definition()
        self.create_gradient_instance()
        self.create_path()
        self.create_prolog()
        self.create_text_object()
        self.create_placed_art_object()
        self.create_object()
        self.create_layer()
        self.create_pattern_definitions()
        self.create_color_palette()
        self.create_text_ai11()
        self.create_unknown_preamble()
        self.create_script()
        self.create_document()

    def create_todos(self):
        self.todo = NoMatch()
        self.not_done = self.todo  # for testing
        self.graph_object = self.todo

    def create_font_encoding(self):
        """Create tokens for font encoding.

        See Fonts and Encodings on page 32 AI Spec
        """
        # [encodingPairs TE - page 33 AI Spec
        self.character_code = self.positive_int
        # [encodingPairs TE - page 33 AI Spec
        self.character_name = Suppress("/") + Word(alphas)
        # [encodingPairs TE - page 33 AI Spec
        self.encoding_pair = (
            Opt(self.newline)
            + self.character_code
            + OneOrMore(Opt(self.newline) + self.character_name)
        )
        # <encoding pairs> - page 32 AI Spec
        self.encoding_pairs = ZeroOrMore(self.encoding_pair) + self.newline
        # <font type> - page 32 AI Spec
        self.font_type = Literal("AdobeType") ^ "TrueType"
        # %AI3_BeginEncoding: newFontName oldFontName - page 32 AI Spec
        self.old_font_name = self.text
        # %AI3_BeginEncoding: newFontName oldFontName - page 32 AI Spec
        self.new_font_name = self.text
        # <encoding pairs> - page 32 AI Spec
        self.re_encoding = (
            "%AI3_BeginEncoding:"
            + self.new_font_name
            + self.old_font_name
            + self.newline
            + SkipTo("%AI3_EndEncoding")
            + "%AI3_EndEncoding"
            + self.font_type
            + self.newline
        )
        # <font encoding> - page 32 AI Spec
        self.font_encoding = (
            Suppress("[")
            + ZeroOrMore(self.encoding_pairs)
            + "TE"
            + self.newline
            + ZeroOrMore(self.re_encoding)
        )

    def create_numbers(self):
        """Create the formats of numbers that we are using."""
        self.positive_int = Word(nums)
        self.int = Combine(Opt(Word("+-")) + self.positive_int)
        self.exponent = Char("eE") + self.int
        # <real> - page 35 PostScript
        self.float = (
            pyparsing_common.sci_real.set_parse_action(lambda p: None) | self.int
        )
        self.number = self.float
        self.positive_number = And([NotAny("-"), self.number])
        self.zero_to_one = Regex(r"1\.?0*|0(?:\.[0-9]*)?|\.[0-9]+")

    def create_characters(self):
        """Create tokens for the characters."""
        self.optional_colon = Suppress(Regex(":?"))
        self.newline = Suppress(Regex(self.Regex.newline))
        self.whitespace = Suppress(Word(self.Characters.whitespace))
        self.skip_line = Suppress(
            Regex("[^\r\n]*(\r\n?|\n)")
        )  # strict newline matching, no double newlines here!
        self.comma = Suppress(Char(","))
        self.semicolon = Suppress(Char(";"))
        self.colon = Suppress(Char(":"))

    def create_text(self):
        """Create tokens to parse text.

        <text> - see page 36 PS Spec
        """
        self.text_outside_of_brackets = OneOrMore(
            CharsNotIn(self.Characters.whitespace + self.Characters.newline + "()")
        )
        text_in_brackets = Forward()
        self.text_without_brackets = ZeroOrMore(
            CharsNotIn(self.Characters.newline + "()")
        )
        # see for recursion: https://stackoverflow.com/a/33118614/1320237
        text_in_brackets <<= self.text_without_brackets ^ OneOrMore(
            self.text_without_brackets
            + "("
            + text_in_brackets
            + ")"
            + self.text_without_brackets
        )
        self.text_in_brackets = text_in_brackets
        self.text_not_in_brackets_without_brackets = Regex(r"[^ \t\r\n]+")
        self.text_not_in_brackets = (
            ZeroOrMore("(" + self.text_in_brackets + ")")
            + self.text_not_in_brackets_without_brackets
            + ZeroOrMore(
                "("
                + self.text_in_brackets
                + ")"
                + Opt(self.text_not_in_brackets_without_brackets)
            )
        )
        self.text = Combine(
            self.text_not_in_brackets
            ^ (Suppress("(") + self.text_in_brackets + Suppress(")"))
        )

    def create_text_ai11(self):
        """Create the parser elements to find the AI11_BeginTextDocument
        Not part of the AI7 spec."""
        self.begin_text_document = Suppress("%AI11_BeginTextDocument") + self.newline
        self.text_document_encoding = (
            Suppress("/") + self.text + self.comma + self.newline
        )
        self.text_document_type = "/AI11TextDocument" ^ Literal(
            "/AI11UndoFreeTextDocument"
        )
        self.text_document_header = (
            self.text_document_type + self.colon + self.text_document_encoding
        )
        self.text_document_content = Regex(".*?\n(?!%)", re.MULTILINE | re.DOTALL)
        self.ruler_definition = (
            self.number
            + self.number
            + Suppress("/RulerOrigin")
            + self.comma
            + self.newline
        )
        self.end_text_document = Suppress("%AI11_EndTextDocument") + self.newline
        self.encoded_text = (
            self.text_document_header
            + self.text_document_content
            + Opt(self.ruler_definition)
            + self.semicolon
            + self.newline
        )
        self.text_element_ai11 = (
            self.begin_text_document
            + OneOrMore(self.encoded_text)
            + self.end_text_document
        )

    def create_data_hierarchy(self):
        """The BeginDocumentData and other places contain a modified XML structure.

        This adds the parser for that.
        See docs/specification_amendments/document_data.md.

        - Use self.parsed__ to get the values.
        - Use self.data_hierarchy_section to skip the section.
        """
        # Parse a whole section fast in case it is not needed.
        # it can be put into a structure later.
        self.data_hierarchy_section = Regex(
            "%_[^\r\n]*"  # start with %_
            + self.Regex.newline
            + "(?:"  # now, we also get binary data
            + "%[^\r\n]*"
            + self.Regex.newline
            + ")*"
        )
        # inkai.parse_actions get attached to these
        self.parsed_data_hierarchy_section = (
            self.benchmark_pyparsing_parsed_data_hierarchy_section
        ) = self.data_hierarchy_section

        data_object = Forward()
        self.data_XMLUID = Suppress("/XMLUID") + self.colon + self.text + self.semicolon
        self.data_string_id = Suppress(Regex("/(?:Unicode)?String"))
        self.data_string = self.data_string_id | (self.text + self.data_string_id)
        self.data_dict_key = self.text
        self.data_dict_value = data_object
        self.data_dict_entry = self.data_dict_value + self.data_dict_key + self.comma
        self.data_dict_type = Suppress("/") + (
            Literal("ArtDictionary") | "XMLNode" | "Dictionary"
        )
        self.data_flag = Regex("/NotRecorded|/Recorded")
        self.data_dict = (
            self.data_dict_type
            + self.colon
            + ZeroOrMore((self.data_flag + self.comma) | self.data_dict_entry)
            + self.semicolon
        )
        self.data_name = self.text + Suppress("/Name")
        self.data_document = (
            Suppress("/Document")
            + self.colon
            + ZeroOrMore(
                data_object + Suppress(self.data_flag) + self.comma  # is in the data
            )
            + self.semicolon
        )
        self.data_array = (
            Suppress("/Array")
            + self.colon
            + ZeroOrMore(data_object + self.comma)
            + self.semicolon
        )
        self.data_int = self.int + Suppress("/Int")
        self.data_real = self.float + Suppress("/Real")
        self.data_real_matrix = ZeroOrMore(self.float) + Suppress("/RealMatrix")
        self.data_bool = Char("01") + Suppress("/Bool")
        self.data_real_point_class = Regex("/RealPoint(?:RelToROrigin)?")
        self.data_real_point = self.x + self.y + self.data_real_point_class
        # This encoding is documented here:
        # https://en.wikipedia.org/wiki/Ascii85#Adobe_version
        self.data_binary_data = Combine(
            Word(
                "\t \r\n!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz"
            )
            + "~>"
        )
        self.data_binary_encoding = Suppress("/") + Word(alphas + nums)
        self.data_binary = (
            Suppress("/Binary")
            + self.colon
            + self.data_binary_encoding
            + self.comma
            + self.data_binary_data
            + self.semicolon
        )
        self.data_object = data_object << (
            self.data_dict
            | self.data_binary
            | self.data_array
            | self.data_XMLUID
            | self.data_bool
            | self.data_int
            | self.data_real_matrix
            | self.data_real
            | self.data_string
            | self.data_name
            | self.data_real_point
            | self.data_document
        )

    def create_header(self):
        """Create the header tokens.

        See page 14 AI Spec
        """
        # docs/specification_amendments/header.md
        self.header_comment_data_hex = Regex(
            r"%%BeginData\s*:.*Hex.*"
            + self.Regex.newline
            + "((?:"
            + "%[a-fA-F0-9]*"
            + self.Regex.newline
            + ")*)"
            + r"%%EndData"
            + self.Regex.newline
        )
        self.header_comment_key = NotAny(self.Text.end_comments) + Regex(
            r"(?:%%?)(?!EndComments)(?:AI[0-9]+_)?([a-zA-Z][a-zA-Z0-9_]*)[ \t]*(?::?)",
            as_match=True,
        ).set_parse_action(lambda p: p[0][1])
        self.header_comment_plus = "%%+" + self.skip_line
        self.header_comment_value = OneOrMore(self.text + Opt(self.whitespace))
        # put specialized headers first
        self.header_comment = (
            self.header_comment_data_hex
            | (self.header_comment_key + Opt(self.header_comment_value) + self.newline)
            | self.header_comment_plus
        )
        self.header = (
            ZeroOrMore(self.header_comment)
            + Suppress(self.Text.end_comments)
            + self.newline
        )

    def create_procset(self):
        """Move all procset occurrences into one place."""
        # <procset init> - page 31 AI Spec
        self.dict_name = self.proc_set_name = self.text
        # <procset init> - page 31 AI Spec
        procset_init = Forward()
        self.procset_init = procset_init << (
            (
                self.dict_name
                + Suppress("/initialize")
                + Suppress("get")
                + Suppress("exec")
                + self.newline
            )
            ^ (self.dict_name + procset_init)
        )
        self.proc_set_termination = (
            self.proc_set_name
            + Suppress("/terminate")
            + Suppress("get")
            + Suppress("exec")
            + self.newline
        )
        # %%IncludeResource: - page 14 AI Spec
        self.include_resource_name = self.text
        # %%IncludeResource: - page 14 AI Spec
        self.include_resource = (
            Suppress("%%IncludeResource:")
            + Suppress("procset")
            + self.include_resource_name
            + self.number
            + self.number
            + self.newline
        )
        # %%BeginResource ... - page 14 AI Spec
        self.resource = Regex(
            r"%%BeginResource:.*"
            + self.Regex.newline
            + "((?:"
            + r"(?!%%EndResource)"
            + r".*"
            + self.Regex.newline
            + ")*)"
            + "(?:%%EndResource)"
            + self.Regex.newline
        )
        # docs/specification_amendments/procset.md
        self.procset_block = Regex(
            r"%%BeginProcSet:.*"
            + self.Regex.newline
            + "((?:"
            + r"(?!%%EndProcSet)"
            + r".*"
            + self.Regex.newline
            + ")*)"
            + "(?:%%EndProcSet)"
            + self.Regex.newline
        )
        # <procset> - page 14 AI Spec
        self.procset = (
            self.procset_block
            | self.resource
            | self.include_resource ^ self.procset_init
        )

    def create_prolog(self):
        """Create the tokens for the prolog of the document.
        page 14 AI Spec
        The first line of the file is a unique comment that identifies the version of the
        Document Structuring Conventions (DSC) to which the document conforms.
        The first line may also specify that the file conforms to a version of the
        Encapsulated PostScript File format (EPSF). Both numbers must correspond
        to the specific versions used in writing out the file. For example, the comment
        %!PS-Adobe-3.0 EPSF-3.0
        indicates that the file conforms to DSC version 3.0 and EPSF version 3.0.
        """
        self.document_structuring_convention = (
            Suppress("%!PS-Adobe-") + SkipTo(self.newline) + self.newline
        )
        # <prolog> - page 14 AI Spec
        self.prolog = (
            self.document_structuring_convention
            + self.header
            + "%%BeginProlog"
            + self.newline
            + (ZeroOrMore(self.procset))
            + "%%EndProlog"
            + self.newline
        )

    def create_colors(self):
        """Create tokens for certain colors."""
        self.color_name = self.text
        self.color_pattern_name = self.color_name
        self.color_value = self.zero_to_one
        self.gray = self.color_value
        self.cyan = self.color_value
        self.magenta = self.color_value
        self.yellow = self.color_value
        self.black = self.color_value
        self.red = self.color_value
        self.green = self.color_value
        self.blue = self.color_value
        self.tint = self.color_value

    def create_variables(self):
        """Create tokens for variables."""
        # for color
        self.px = self.number
        self.py = self.number
        self.sx = self.number
        self.sy = self.number
        self.angle = self.number
        self.rf = self.number
        self.r = self.number
        self.k = self.number
        self.ka = self.number
        self.a = self.number
        self.b = self.number
        self.c = self.number
        self.d = self.number
        self.tx = self.number
        self.ty = self.number

        # for paths
        self.coordinate = self.number
        self.x = self.coordinate
        self.y = self.coordinate
        self.x1 = self.coordinate
        self.y1 = self.coordinate
        self.x2 = self.coordinate
        self.y2 = self.coordinate
        self.x3 = self.coordinate
        self.y3 = self.coordinate

    def create_color(self):
        """Create the color.

        <color> - page 59
        """
        # <fill color> - page 59
        self.fill_color = self.get_color(True)
        self.stroke_color = self.get_color(False)
        self.color = self.fill_color ^ self.stroke_color

    def get_color(self, is_lower_case):
        """Shortcut to create a <fill color> which is_lower_case or a <stroke color>."""
        conv = lambda c: Char(c.lower() if is_lower_case else c.upper())
        color_xx = Combine("X" + conv("X"))
        color_xy = Combine("X" + conv("Y"))
        # page 61 AI Spec
        rgb = (
            self.red
            + self.green
            + self.blue
            + self.color_name
            + self.tint
            + Char("1")
            + color_xx
        )
        # page 61 AI Spec
        cmyk = (
            self.cyan
            + self.magenta
            + self.yellow
            + self.black
            + self.color_name
            + self.tint
            + Char("0")
            + color_xx
        )
        cmyk2 = (  # TODO: Document color 0 1 0 0 0 Xy in layers_and_sublayers_extracted.ai, line 28432
            self.cyan + self.magenta + self.yellow + self.black + Char("0") + color_xy
        )
        color_xz = self.number * 7 + self.text + self.int * 2 + Combine("X" + conv("Z"))
        return (
            (self.gray + conv("G"))  # page 60
            ^ (
                self.cyan + self.magenta + self.yellow + self.black + conv("K")
            )  # page 60
            ^ (
                self.cyan
                + self.magenta
                + self.yellow
                + self.black
                + self.color_name
                + self.tint  # also gray value but can be neglected
                + conv("X")
            )  # page 60
            ^ (
                Opt(self.cyan + self.magenta + self.yellow + self.black)
                + self.red
                + self.green
                + self.blue
                + "X"
                + conv("A")
            )  # page 61
            ^ rgb
            ^ cmyk
            ^ cmyk2
            ^ color_xz
            ^ (
                self.color_pattern_name
                + self.px
                + self.py
                + self.sx
                + self.sy
                + self.angle
                + self.rf
                + self.r
                + self.k
                + self.ka
                + "["
                + self.a
                + self.b
                + self.c
                + self.d
                + self.tx
                + self.ty
                + "]"
                + conv("P")
            )
        ) + self.newline

    def create_path(self):
        """Create the token to parse path information.

        - path object
        - path mask
        """
        # <note> - page 55
        self.note = CharsNotIn(self.Characters.newline)[0, 254]
        self.ai3_note = "%AI3_NOTE:" + self.note

        # array - page 55 AI Spec
        self.array = Group(Suppress("[") + ZeroOrMore(self.number) + Suppress("]"))

        # setdash - page 55 AI Spec
        self.phase = self.number
        self.set_dash = self.array + self.phase + "d"

        # winding order - page 55 AI Spec
        self.winding_order_flag = Char("01")
        self.winding_order = self.winding_order_flag + "D"

        # setflat - page 55 AI Spec
        self.flatness = self.positive_number
        self.set_flat = self.flatness + "i"

        # setlinejoin - page 55 AI Spec
        self.line_join = Char("012")
        self.set_line_join = self.line_join + "j"

        # setlinecap - page 56 AI Spec
        self.line_cap = Char("012")
        self.set_line_cap = self.line_cap + "J"

        # setmiterlimit - page 56 AI Spec
        self.miter_limit = self.positive_number
        self.set_miter_limit = self.miter_limit + "M"

        # setlinewidth - page 56 AI Spec
        self.line_width = self.positive_number
        self.set_line_width = self.line_width + "w"

        # <path attributes> - page 55 AI Spec
        self.path_attributes = (
            OneOrMore(
                self.ai3_note
                ^ self.set_dash
                ^ self.winding_order
                ^ self.set_flat
                ^ self.set_line_join
                ^ self.set_line_cap
                ^ self.set_miter_limit
                ^ self.set_line_width
            )
            + self.newline
        )
        # <guide operator> = page 66 AI Spec
        self.guide_operator = Char("*")
        # <path render> - page 54 AI Spec
        self.path_render = (
            (
                Char("N")  # (closepath; no fill no stroke) - page 58
                ^ "n"  # (neither fill nor stroke) - page 58 AI Spec
                ^ self.gradient_instance  # (fill with gradient)
                ^ "F"  # (fill) - page 58 AI Spec
                ^ "f"  # (closepath; fill) - page 58 AI Spec
                ^ "S"  # (stroke) - page 58 AI Spec
                ^ "s"  # (closepath; stroke) - page 58 AI Spec
                ^ "B"  # (fill and stroke) - page 58 AI Spec
                ^ "b"  # (closepath; fill and stroke) - page 58 AI Spec
            )
            + Opt(self.guide_operator)
            + self.newline
        )

        # <path operator> - page 54 AI Spec
        self.path_operator = (
            (self.x + self.y + "l")  # straight line - page 57 AI Spec
            ^ (self.x + self.y + "L")  # straight line - page 57 AI Spec
            ^ (
                self.x1 + self.y1 + self.x2 + self.y2 + self.x3 + self.y3 + "c"
            )  # Bézier curve - page 57 AI Spec
            ^ (
                self.x1 + self.y1 + self.x2 + self.y2 + self.x3 + self.y3 + "C"
            )  # Bézier curve - page 57 AI Spec
            ^ (
                self.x2 + self.y2 + self.x3 + self.y3 + "v"
            )  # Bézier curve - page 57 AI Spec
            ^ (
                self.x2 + self.y2 + self.x3 + self.y3 + "V"
            )  # Bézier curve - page 57 AI Spec
            ^ (
                self.x1 + self.y1 + self.x3 + self.y3 + "y"
            )  # Bézier curve - page 57 AI Spec
            ^ (
                self.x1 + self.y1 + self.x3 + self.y3 + "Y"
            )  # Bézier curve - page 57 AI Spec
        ) + self.newline

        # <path geometry> - page 54 AI Spec
        # moveto - page 56 AI Spec
        self.move_to = self.x + self.y + "m" + self.newline
        self.path_geometry = self.move_to + ZeroOrMore(self.path_operator)

        # (fill overprint) - page 63 AI Spec
        self.overprint_fill = Char("01") + Suppress(Char("O")) + self.newline
        # (stroke overprint) - page 63 AI Spec
        self.overprint_stroke = Char("01") + Suppress(Char("R")) + self.newline
        # <overprint> - page 62 AI Spec
        self.overprint = self.overprint_fill ^ self.overprint_stroke

        # TODO: Document
        self.undocumented_paint_operator = Regex(r"[0-9]+\s+XR" + self.Regex.newline)
        self.paint_style = (
            self.color
            ^ self.overprint
            ^ self.path_attributes
            ^ self.undocumented_paint_operator
        )
        # <paint style> - page 54 AI Spec
        self.paint_styles = ZeroOrMore(self.paint_style)
        # <path object> - page 52 AI Spec
        self.path_object = self.paint_styles + self.path_geometry + self.path_render

    def create_text_object(self):
        """Create text objects.

        See page 93 AI Spec.
        """
        until = lambda letter: OneOrMore(
            ZeroOrMore(CharsNotIn(self.Characters.newline + letter)) + letter
        )
        self.text_operator = until("T")
        text_operator = (
            lambda second_letter: self.text_operator
            + Char(second_letter)
            + self.newline
        )
        self.Tr_operator = text_operator("r")
        self.Tf_operator = text_operator("f")
        self.Ts_operator = text_operator("s")
        self.Tz_operator = text_operator("z")
        self.Tt_operator = text_operator("t")
        self.TA_operator = text_operator("A")
        self.TC_operator = text_operator("C")
        self.TW_operator = text_operator("W")
        self.Ti_operator = text_operator("i")
        self.Ta_operator = text_operator("a")
        self.Tq_operator = text_operator("q")
        self.Tl_operator = text_operator("l")
        self.TV_operator = text_operator("V")
        self.Tv_operator = text_operator("v")
        self.Ty_operator = text_operator("y")
        self.TY_operator = text_operator("Y")
        self.TG_operator = text_operator("G")
        self.Tg_operator = text_operator("g")
        self.Xu_operator = until("X") + "u" + self.newline
        self.Tc_operator = text_operator("c")
        self.Tw_operator = text_operator("w")
        self.Tm_operator = text_operator("m")
        self.Td_operator = text_operator("d")
        self.T_star_operator = text_operator("*")
        self.TR_operator = text_operator("R")
        self.Tx_operator = text_operator("x")
        self.Tj_operator = text_operator("j")
        self.T_plus_operator = text_operator("+")
        self.T_minus_operator = text_operator("-")
        self.TK_operator = text_operator("K")
        self.Tk_operator = text_operator("k")
        self.TX_operator = text_operator("X")
        self.To_operator = text_operator("o")
        self.TO_operator = Suppress("TO") + self.newline
        self.text_style = (
            self.Tr_operator
            ^ self.Tf_operator
            ^ self.Ts_operator
            ^ self.Tz_operator
            ^ self.Tt_operator
            ^ self.TA_operator
            ^ self.TC_operator
            ^ self.TW_operator
            ^ self.Ti_operator
            ^ self.Ta_operator
            ^ self.Tq_operator
            ^ self.Tl_operator
            ^ self.TV_operator
            ^ self.Tv_operator
            ^ self.Ty_operator
            ^ self.TY_operator
            ^ self.TG_operator
            ^ self.Tg_operator
            ^ self.Xu_operator
        )
        # <text position> - page 87 AI Spec
        self.text_position = (
            self.Tc_operator
            ^ self.Tw_operator
            ^ self.Tm_operator
            ^ self.Td_operator
            ^ self.T_star_operator
            ^ self.TR_operator
        )
        # <text body> - page 87 AI Spec
        self.text_body = (
            self.Tx_operator
            ^ self.Tj_operator
            ^ self.T_plus_operator
            ^ self.T_minus_operator
        )
        # <overflow text> - page 87 AI Spec
        self.overflow_text = ZeroOrMore(
            self.text_style ^ self.paint_style ^ self.TK_operator
        ) + (self.TX_operator ^ self.T_plus_operator)
        # <text run> - page 86 AI Spec
        self.text_run = OneOrMore(  # differs from spec
            self.text_style
            ^ self.paint_style
            ^ self.text_position
            ^ self.Tk_operator
            ^ self.text_body
        )
        self.text_runs = OneOrMore(self.text_run)
        # <Tp> - page 88
        self.startPt = self.number
        # <Tp> - page 88
        self.Tp_operator = (
            self.a
            + self.b
            + self.c
            + self.d
            + self.tx
            + self.ty
            + self.startPt
            + Suppress("Tp")
            + self.newline
        )
        # <TP> - page 88
        self.TP_operator = Suppress("TP") + self.newline
        # <text at a point> - page 104
        self.text_at_a_point = self.Tp_operator + self.TP_operator + self.text_runs
        # <text along a path> - page 104
        self.text_along_a_path = (
            self.Tp_operator
            + self.path_object
            + self.TP_operator
            + OneOrMore(self.text_run)
            + ZeroOrMore(self.overflow_text)
        )
        # <text area element> - page 86 AI Spec
        self.text_area_element = (
            self.Tp_operator
            + self.path_object
            + self.TP_operator
            + OneOrMore(self.text_run)
        )
        # <text area> - page 106
        self.text_area = OneOrMore(self.text_area_element) + Opt(self.overflow_text)
        # type - page 94 AI Spec
        self.text_type = Char("012")
        # <text object> - page 94 AI Spec
        self.text_object = (
            self.To_operator
            + (self.text_at_a_point ^ self.text_area ^ self.text_along_a_path)
            + self.TO_operator
        )

    def create_object(self):
        """Create object definitions.

        AI Spec pages 52, 137
        """
        object = Forward()  # first
        compound_mask_group = Forward()  # first
        # <A> - page 54 AI Spec
        self.object_locking = Char("01") + Suppress("A") + self.newline
        # identifier - page 67 AI Spec
        self.object_tag_identifier = Suppress("/") + Word(alphas + nums + "_")
        # string - page 67 AI Spec
        self.object_tag_string = self.text
        # <object tag> - page 67 AI Spec
        self.object_tag = (
            self.object_tag_identifier
            + self.object_tag_string
            + Suppress("XT")
            + self.newline
        )
        # <wraparound_objects> - page 93 AI Spec
        self.wraparound_text = self.text_object
        self.wraparound_objects = self.wraparound_text ^ ZeroOrMore(object)
        # <wraparound group> - page 93 AI Spec
        self.wraparound_group = self.open_and_close_token(
            "*w", ZeroOrMore(object) + ZeroOrMore(self.wraparound_objects)
        )
        # <group object> - page 52 AI Spec
        # Group Operators - page 63 AI Spec
        self.group_object = self.open_and_close_token("u", ZeroOrMore(object))
        # <compound path element> - page 59 AI Spec
        compound_path_element = Forward()
        # Group Operators - page 63 AI Spec
        self.compound_group = self.open_and_close_token(
            "u", ZeroOrMore(compound_path_element)
        )
        self.compound_path_element = compound_path_element << (
            self.path_object ^ self.compound_group
        )
        # <compound path> - pages 53, 59 AI Spec
        self.compound_path = self.open_and_close_token(
            "*u", ZeroOrMore(self.compound_path_element)
        )
        # <h> | <H> - page 65 AI Spec
        self.h_operator = Char("hH") + self.newline
        # <W> - page 65 AI Spec
        self.w_operator = Char("W") + self.newline
        # <path mask> - page 64 AI Spec
        self.path_mask = (
            self.paint_styles
            + self.path_geometry
            + Opt(self.h_operator)
            + Opt(self.w_operator)
            + self.path_render
        )
        # <Md> - page 75 Ai Spec
        self.masking_object = object + Suppress("Md") + self.newline
        # <multi layer mask> - page 64, 75 AI Spec
        self.multi_layer_mask = self.open_and_close_token(
            "Mb", ZeroOrMore(object) + self.masking_object
        )
        # <compound mask non-bottom group> - page 64 AI Spec
        self.compound_mask_non_bottom_group = Opt(
            self.object_locking
        ) + self.open_and_close_token("u", OneOrMore(compound_mask_group))
        # <compound mask bottom group> - page 64 AI Spec
        self.compound_mask_bottom_group = Opt(
            self.object_locking
        ) + self.open_and_close_token("q", OneOrMore(self.path_mask))
        # <compound mask group> - page 64 AI Spec
        self.compound_mask_group = compound_mask_group << (
            self.compound_mask_bottom_group ^ self.compound_mask_non_bottom_group
        )
        # <compound path mask element> - page 64 AI Spec
        self.compound_path_mask_element = self.path_mask ^ self.compound_mask_group
        # <compound path mask> - page 64 AI Spec
        self.compound_path_mask = self.open_and_close_token(
            "*u", OneOrMore(self.compound_path_mask_element)
        )
        # <mask> - page 64 AI Spec
        self.mask = self.path_mask ^ self.compound_path_mask ^ self.multi_layer_mask
        # <masked object> - page 64 AI Spec
        self.masked_object = self.mask ^ object  # does not make sense
        # <group with a mask> - page 63 AI Spec
        self.group_with_a_mask = self.open_and_close_token(
            "q", ZeroOrMore(object ^ self.masked_object)
        )
        self.post_script_document = self.todo
        # <composite object> - page 52 AI Spec
        self.composite_object = (
            self.group_object
            | self.group_with_a_mask
            ^ self.compound_path
            ^ self.compound_path_mask
            ^ self.wraparound_group
        )
        # TODO: document
        self.unkown_binary_operator = Regex(
            r"([0-9]+)\s+(Xw|Ae|As|AE|Ap)" + self.Regex.newline, as_match=True
        )
        # TODO: document, seems to close the Xw operator.
        self.unspecified_operator_XW = self.int + self.text + "XW" + self.newline
        # They appear to be in place where we find objects.
        # They might be additional settings.
        self.unspecified_object_operator = (
            self.unkown_binary_operator | self.unspecified_operator_XW
        )
        self.raster_object = self.todo
        self.object_data = self.parsed_data_hierarchy_section
        # move the shape of the object inside a new parser to attach parse actions
        self.object_shape = (
            self.composite_object
            | self.path_object  # A
            ^ self.path_mask
            ^ self.raster_object
            ^ self.text_object
            ^ self.placed_art_object
            ^ self.subscriber_object
            ^ self.graph_object
            ^ self.unspecified_object_operator
        )  # slight difference from spec
        # <object> - page 52 AI Spec
        self.object = object << (
            Opt(self.object_locking)
            + self.object_shape
            + ZeroOrMore(
                self.object_tag  # XT
                ^ self.object_data
                # + Opt(self.post_script_document)
            )
        )
        self.objects = OneOrMore(self.object)

    def create_layer(self):
        """Create the layer definitions.

        See page 71 AI Spec
        """
        # Begin Layer - Lb Operator - page 71++ AI Spec
        self.visible = Char("01")
        self.preview = Char("01")
        self.enabled = Char("01")
        self.printing = Char("01")
        self.dimmed = Char("01")
        self.has_multi_layer_masks = Char("01")
        self.color_index = self.int
        self.red_255 = self.positive_int
        self.green_255 = self.positive_int
        self.blue_255 = self.positive_int
        # <Lb> - page 71 AI Spec
        self.begin_layer = (
            self.visible
            + self.preview
            + self.enabled
            + self.printing
            + self.dimmed
            + self.has_multi_layer_masks
            + self.color_index
            + self.red_255
            + self.green_255
            + self.blue_255
            + Opt(OneOrMore(self.int))  # docs/specification_amendments/layer.md
            + Suppress("Lb")
            + self.newline
        )
        # <Ln> - page 71 AI Spec
        self.layer_name = self.text + Suppress("Ln") + self.newline
        layer = Forward()
        # <object definitions> - page 71 AI Spec, docs/specification_amendments/layer.md
        self.object_definitions = ZeroOrMore(layer | self.object)
        # <LB> - page 71 AI Spec
        self.end_layer = "LB" + self.newline
        self.layer_AE_operator = Regex(r"[01]\s+AE" + self.Regex.newline)
        self.layer_data = self.parsed_data_hierarchy_section
        # <layer> - page 71 AI Spec
        self.layer = layer << (
            Suppress("%AI5_BeginLayer")
            + self.newline
            + self.begin_layer
            + self.layer_name
            + Opt(self.layer_AE_operator)
            + Opt(self.layer_data)
            + self.object_definitions
            + self.end_layer
            + Regex("%AI5_EndLayer-?-?" + self.Regex.newline)
        )

    def create_placed_art_object(self):
        """Create tokens for places art."""
        placed_art_object = Forward()  # first
        # <subscriber ID> - page 107 AI Spec
        self.subscriber_id = self.todo
        # <subscriber object> - page 107 AI Spec
        self.subscriber_object = (
            Suppress("%AI3_Subscriber:")
            + self.subscriber_id
            + self.newline
            + placed_art_object
        )
        # <filename> - page 107 AI Spec
        self.filename = self.text
        # ... included file contents - page 107 AI Spec
        self.inlcuded_file_contents = self.todo
        # <file inline> - page 107 AI Spec
        self.file_inline = (
            Suppress("%%BeginDocument:")
            + self.filename
            + self.newline
            + self.inlcuded_file_contents
        )
        # <file reference> - page 107 AI Spec
        self.file_reference = Suppress("%%IncludeFile:") + self.filename + self.newline
        # <art reference> - page 107 AI Spec
        self.art_reference = self.file_reference ^ self.file_inline
        # <placed art object> - page 107 AI Spec
        self.placed_art_object = placed_art_object << self.open_and_close_token(
            "'", self.art_reference, "~"
        )

    def create_gradient_definition(self):
        """Create the tokens for <gradient def>

        See pages 31, 39 AI Spec
        """
        # rampPoint - page 41 AI Spec
        self.ramp_point = self.text
        # midPoint - page 41 AI Spec
        self.mid_point = self.text
        # colorStyle - page 41 AI Spec
        self.color_style = self.text
        # colorSpec - page 42 AI Spec
        self.color_spec = self.text
        # Color Stops - %_Bs Operator - page 41 AI Spec
        self.gradient_stops_operator = (
            self.color_spec
            + self.color_style
            + self.mid_point
            + self.ramp_point
            + "%_Bs"
            + self.newline
        )
        # <color stops> - page 39 and 41 AI Spec
        self.color_stops = "[" + self.gradient_stops_operator + "+" + self.newline
        # ramp spec - page 43 AI Spec
        self.ramp_spec = self.text
        # ramp spec - page 43 AI Spec
        self.ramp_type = self.text
        # <%_Br> - page 42 AI Spec
        self.gradient_ramps_operator = self.ramp_spec + self.ramp_type + "%_Br"
        # <ramp data> - page 39 AI Spec
        self.ramp_data = "[" + self.gradient_ramps_operator + "+" + self.newline
        # (gradient_name) - page 31 AI Spec
        self.gradient_name = self.text
        # End Gradient Description - page 44
        self.end_gradient_description = Suppress("BD") + self.newline
        # <gradient def> - page 31, 39 AI Spec
        self.gradient_def = (
            Suppress(self.Text.begin_gradient)
            + self.gradient_name
            + self.newline
            + Opt(self.ramp_data)
            + self.color_stops
            + self.end_gradient_description
            + Suppress(self.Text.end_gradient)
            + self.newline
        )
        # Number of Gradients - Bn Operator - page 40 AI Spec
        self.number_of_gradients = self.positive_int + Suppress("Bn") + self.newline
        # <gradient defs> - page 31 AI Spec
        self.gradient_defs = self.number_of_gradients + ZeroOrMore(self.gradient_def)

    def create_gradient_instance(self):
        """Create the tokens for <gradient instance>

        See pages 44ff AI Spec
        """
        # Gradient geometry - page 45 AI spec
        self.gradient_flag = Char("012")
        self.x_origin = self.number
        self.y_origin = self.number
        self.gradient_angle = self.number
        self.gradient_length = self.number
        self.gradient_transform = self.number * 6
        self.gradient_geometry = (
            self.gradient_flag
            + self.gradient_name
            + self.x_origin
            + self.y_origin
            + self.gradient_angle
            + self.gradient_length
            + self.gradient_transform
            + Opt(self.int)
            + Suppress("Bg")
            + self.newline
        )

        # <Bm> and <Bc> - page 49 AI spec, not sure whether we need that
        self.gradient_imaging = (self.gradient_transform + Suppress("Bm")) ^ (
            self.gradient_transform + Suppress("Bc")
        ) + self.newline

        # <Bh> gradient hilight - page 48 AI spec
        self.x_hilight = self.number
        self.y_hilight = self.number
        self.gradient_hilight = (
            self.x_hilight
            + self.x_hilight
            + self.gradient_angle
            + self.gradient_length
            + Suppress("Bh")
            + self.newline
        )

        self.gradient_instance_end = Char("012") + Suppress("BB") + self.newline

        self.gradient_instance = (
            Suppress("Bb")
            + self.newline
            + ZeroOrMore(
                self.gradient_geometry
                ^ self.gradient_hilight
                ^ self.gradient_imaging
                ^ (Literal("f") + self.newline)
            )
            + self.gradient_instance_end
        )

    def create_color_palette(self):
        """Create the color palette.

        Page 78++ AI Spec
        """
        # End Palette - page 79 AI Spec
        self.end_palette = "PB" + self.newline
        # End Palette - page 79 AI Spec
        self.palette_cell = (
            Opt(Suppress("Pc") + self.newline)
            + (self.gradient_instance ^ self.color)
            + Opt(self.text + self.newline)
            + Suppress("Pc")
            + self.newline
        )
        # End Palette - page 79 AI Spec
        self.palette_cell_none = "Pn" + self.newline
        # Begin Palette - topLeftCellIndex - page 78 AI Spec
        self.top_left_cell_index = self.positive_int
        # Begin Palette - selectedIndex - page 78 AI Spec
        self.selected_index = self.positive_int
        # Begin Palette - page 78 AI Spec
        self.begin_palette = (
            self.top_left_cell_index + self.selected_index + "Pb" + self.newline
        )
        self.palette_group = self.number + self.text + self.number + "Pg" + self.newline
        # <color palette> - page 78 AI Spec
        self.color_palette = (
            Suppress("%AI5_BeginPalette")
            + self.newline
            + self.begin_palette
            + ZeroOrMore(
                self.palette_cell_none ^ self.palette_cell ^ self.palette_group
            )
            + self.end_palette
            + Suppress("%AI5_EndPalette")
            + self.newline
        )

    def create_unknown_preamble(self):
        self.fivej_tsume = (
            Literal("%AI55J_Tsume:") + SkipTo(self.newline) + self.newline
        )

        def other_commands(version, type, uscore_before_type=False):
            start = f"%AI{version}_"
            type = f"{'_' if uscore_before_type else ''}{type}"
            return Regex(
                f"{start}Begin{type}.*?{start}End{type}.*?(\\r\\n?|\\n)",
                re.MULTILINE | re.DOTALL,
            )

        self.other_commands = (
            other_commands(3, "Encoding")
            | other_commands(5, "NonPrinting", uscore_before_type=True)
            | other_commands(5, "Palette")
            | other_commands(9, "DocumentData")
        )
        # docs/specification_amendments/procset.md
        # Example: userdict /_useSmoothShade false put
        self.undocumented_procset = Regex(
            r"[^% \t\r\n][^ \t\r\n]*\s+/[^\r\n]*" + self.Regex.newline
        )

        self.unknown_script_header = (
            self.fivej_tsume ^ self.other_commands ^ self.undocumented_procset
        )

    def create_pattern_definitions(self):
        """Create the tokens for patterns.

        page 34 AI Spec
        """
        # <pattern layer> - page 35 AI Spec
        self.pattern_layer = self.todo
        # <pattern layer list> - page 35 AI Spec
        self.pattern_layer_list = ZeroOrMore(self.pattern_layer)
        # pattername - page 35 AI Spec
        self.pattern_name = self.text
        # llx lly urx ury - page 35 AI Spec
        self.llx = self.lly = self.urx = self.ury = self.number
        # E - page 35 AI Spec
        self.pattern_e_operator = (
            self.pattern_name
            + self.llx
            + self.lly
            + self.urx
            + self.ury
            + Suppress("[")
            + self.pattern_layer_list
            + Suppress("]")
            + Suppress("E")
            + self.newline
        )
        # <pattern> - page 35 AI Spec
        self.pattern = (
            Suppress("%AI3_BeginPattern:")
            + self.pattern_name
            + self.newline
            #            + self.pattern_e_operator
            + Regex(f".*?AI3_EndPattern.*?(\\r\\n?|\\n)", re.MULTILINE | re.DOTALL)
        )
        # <pattern defs> - page 34 AI Spec
        self.pattern_defs = OneOrMore(self.pattern)

    def create_script(self):
        """Create the script tokens.

        See page 12, 31 AI Spec
        """
        # in <setup> - page 31 AI Spec
        self.font = self.text
        # in <setup> - page 31 AI Spec
        self.include_font = Suppress("%%IncludeFont:") + self.font + self.newline
        # <setup> - page 31 AI Spec
        self.setup = (
            Suppress("%%BeginSetup")
            + self.newline
            + ZeroOrMore(
                self.include_font
                ^ self.procset_init
                ^ self.font_encoding
                ^ self.unknown_script_header
                ^ self.gradient_defs
                ^ self.color_palette
                ^ self.pattern_defs
                ^ self.text_element_ai11
            )
            + Suppress("%%EndSetup")
            + self.newline
        )
        # <document trailer> - page 31 AI Spec
        self.document_trailer = (
            Suppress("%%Trailer") + self.newline + ZeroOrMore(self.proc_set_termination)
        )
        # <page trailer> - page 31, 114 AI Spec
        self.page_trailer = (
            Suppress("%%PageTrailer")
            + self.newline
            + "gsave"
            + "annotatepage"
            + "grestore"
            + "showpage"
            + self.newline
        )
        # hooks
        self.object_outside_of_layer = self.object
        self.script_body = (
            OneOrMore(self.layer ^ self.object_outside_of_layer)
            + Opt(self.page_trailer)
            + Opt(self.document_trailer)
        )
        # <script> - page 31 AI Spec
        self.script = self.setup + self.script_body + Suppress("%%EOF") + self.newline

    def create_document(self):
        """Create the token for the document."""
        self.document = self.prolog + self.script
