# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This file contains the base class for parse actions.

Parse actions can be attached to a parser.
"""
from .errors import (
    UnusedMatchMethod,
    UnusedParserName,
    UnusedParseMethod,
    ParsedButNotImplemented,
    PleaseGiveUsTheFile,
)
from typing import List, Set, Tuple
from pyparsing import ParserElement, ParseResults
import re
import warnings

MATCH_METHOD_PREFIX = "match_"
PARSE_METHOD_PREFIX = "parse_"


def one_element_of(s: Set[str]) -> str:
    """Return one element of a set."""
    if len(s) == 0:
        raise ValueError("We expect the set to have an element to return.")
    return next(iter(s))


class ParseActions:
    """Base class for parse actions.

    All 'parse_' methods will be added as parse actions.
    All names in self.names will be named results.
    """

    warn_if_the_file_is_needed = True
    names: List[str] = []

    def __init__(self):
        """Create a new parse action."""
        attributes = dir(self)
        self._remaining_match_methods = {
            method_name
            for method_name in attributes
            if method_name.startswith(MATCH_METHOD_PREFIX)
        }
        self._remaining_parse_methods = {
            method_name
            for method_name in attributes
            if method_name.startswith(PARSE_METHOD_PREFIX)
        }
        self._remaining_names = set(self.names)

    def attach_to(self, name, parser: ParserElement):
        """Attach a parse action to a token if it is defined."""
        if name in self._remaining_names:
            parser = parser.set_results_name(name)
            self._remaining_names.remove(name)
        for (
            method_name,
            regex_group_name,
            regex_group_value,
        ) in self.get_match_method_for(name):
            self.attach_match_method_to(
                method_name, regex_group_name, regex_group_value, parser
            )
        parse_method_name = PARSE_METHOD_PREFIX + name
        if parse_method_name in self._remaining_parse_methods:
            parse_method = getattr(self, parse_method_name)
            parser.add_parse_action(parse_method)
            self._remaining_parse_methods.remove(parse_method_name)
        return parser

    def get_match_method_for(self, name: str) -> List[Tuple[str, ...]]:
        """Return the methods that match on the regex.

        Returns: a list of (method name, match group, match group value)
        """
        start = MATCH_METHOD_PREFIX + name + "_"
        return [
            (method_name,) + tuple(method_name[len(start) :].split("_", 1))
            for method_name in self._remaining_match_methods
            if method_name.startswith(start)
        ]

    def attach_match_method_to(
        self,
        method_name: str,
        regex_group_name: str,
        regex_group_value: str,
        parser: ParserElement,
    ):
        """Attach a method that only runs on a regular expression.

        If the specific regex_group_name is in the match and the value
        of the match group equals regex_group_value, then this method is executed.
        """

        def parse_action(p: ParseResults):
            """Execute the method on the selected match only."""
            match: re.Match = p[0]
            assert not isinstance(
                match, str
            ), "Please set as_match in Regex(..., as_match=True) to make this work."
            groups = match.groupdict()
            assert (
                regex_group_name in groups
            ), f"Regex '{parser}' should have a group '(?P<{regex_group_name}>...)' but only these exist: {', '.join(groups)}."
            for k, v in groups.items():
                p[k] = v
            if groups[regex_group_name] == regex_group_value:
                method = getattr(self, method_name)
                return method(p)
            return p

        parser.add_parse_action(parse_action)
        self._remaining_match_methods.remove(method_name)

    @property
    def name(self) -> str:
        """Attribute name for the tests to have easy access to added actions."""
        return self.__class__.__name__.lower()

    def not_implemented(self, p: ParseResults):
        """This is parsed but not implemented, yet.

        This is a shortcut to raise a ParsedButNotImplemented exception.
        """
        raise ParsedButNotImplemented(p)

    def reset(self) -> None:
        """Reset the state of the parser. This calls init().

        Resetting a parser leads to an UnusedParseAction exception if
        - not all names are found
        - not all parse_ methods are attached
        - not all match_ methods are attached.
        """
        if self._remaining_match_methods:
            raise UnusedMatchMethod(
                f"{self.__class__.__name__}.{one_element_of(self._remaining_match_methods)} was not attached to a parser. Maybe, the parser was renamed."
            )
        if self._remaining_names:
            raise UnusedParserName(
                f"{self.__class__.__name__}.names includes '{one_element_of(self._remaining_names)}' that was not attached to a parser. Maybe, the parser was renamed."
            )
        if self._remaining_parse_methods:
            raise UnusedParseMethod(
                f"{self.__class__.__name__}.{one_element_of(self._remaining_parse_methods)} was not attached to a parser. Maybe, the parser was renamed."
            )
        self.init()

    def init(self) -> None:
        """Called before parsing so that the state can be reset.

        This can be overwritten by subclasses to intialize the state
        before each time the parser is used.
        """

    def set_parser(self, parser) -> None:
        """Set the parser to parse actions can access parsers.

        - parser: base.Parser
        """
        self.parser = parser

    def we_want_the_file(self, id: str, *args) -> None:
        """Tell the user that we would like to have this file.

        - id identifies the reason
        """
        enum = "\n - "
        message = f"""This file contains data which we would like to support.
        
        ID: {id}
         - {enum.join(map(repr, args))}
        
        If you can, please report this file with the message mentioned above.
        For that, open an issue here:
            https://gitlab.com/inkscape/extras/extension-ai/-/issues/
        Or send the file to
            niccokunzmann\x40rambler.ru
        
        In case this is called inside of a test, an error is raised:
        all tests should understand the files they are given.
        """
        assert self.warn_if_the_file_is_needed, message
        warnings.warn(PleaseGiveUsTheFile(id, message), category=id, stacklevel=2)


__all__ = ["ParseActions"]
