# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This module contains all the errors that might be raised by this package.

"""
from pyparsing import ParseException, ParseResults


class UnusedParseAction(NameError):
    """The parse action defines an action but this action is not used."""

    @property
    def message(self):
        """The message of the error."""
        return self.args[0]


class UnusedParseMethod(UnusedParseAction):
    """The parse action defines an action but this action is not used.

    This might be a potential mistake:
    All 'parse_' methods should be attached to a parser.
    """


class UnusedMatchMethod(UnusedParseAction):
    """The parse action defines an action but this action is not used.

    This might be a potential mistake:
    All 'match_' methods should be attached to a parser.
    """


class UnusedParserName(UnusedParseAction):
    """The parse action defines an action but this action is not used.

    This might be a potential mistake:
    All parsers in ParseActionsBase.names should exist.
    """


class MissingFeature(NotImplementedError):
    """This feature is missing at this moment."""


class MissingParserFeature(MissingFeature, ParseException):
    """If the parser is incomplete, this can be raised to mark the place."""

    loc = 0
    pstr = ""
    parser_element = None

    def __init__(self, p: ParseResults):
        """Create a nice error message with a parse result"""
        self.name = p.get_name()
        self.parse_results = p
        self.msg = f"Could not use {self.name} {p}"
        super().__init__(self.msg)


class ParsedButNotImplemented(MissingParserFeature):
    """The parsing is done but this particular value is not understood."""


class PleaseGiveUsTheFile(UserWarning):
    """We ask to get this file for a certain reason."""

    def __init__(self, id, message):
        """Create a new warning with a message for a certain id."""
        self.id = id
        self.message = message
        super().__init__(message)


__all__ = [
    "UnusedParseAction",
    "UnusedParseMethod",
    "UnusedMatchMethod",
    "UnusedParserName",
    "MissingFeature",
    "MissingParserFeature",
    "ParsedButNotImplemented",
    "PleaseGiveUsTheFile",
]
