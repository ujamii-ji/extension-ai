# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This file contains conveniece access to parse actions.

This is a private module.
"""
from typing import List
from pyparsing import ParserElement
from .parse_actions import ParseActions


class ParseActionsList:
    """A set of actions for quick access inside the ParserBase.add_default_parse_actions()."""

    def __init__(self, parser):
        """Create a new actions list for a parser.Parser."""
        self._actions: List[ParseActions] = []
        self._parser = parser

    def add(self, parse_action: ParseActions) -> None:
        """Add a new parse action."""
        self._actions.append(parse_action)
        setattr(self, parse_action.name, parse_action)
        parse_action.set_parser(self._parser)

    def attach(self, name: str, parser: ParserElement) -> ParserElement:
        """Attach my actions to a parser."""
        for parse_action in self._actions:
            parser = parse_action.attach_to(name, parser)
        return parser

    def reset(self) -> None:
        """Initialize the parse actions for parsing."""
        for parse_action in self._actions:
            parse_action.reset()


__all__ = ["ParseActionsList"]
