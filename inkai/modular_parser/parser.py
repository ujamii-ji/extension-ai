# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This file contains the base class for creating a complex grammar."""
from ._parse_actions_list import ParseActionsList
from pyparsing import ParserElement
from typing import List


class Parser:
    """Base parser without any tokens or parse actions.

    set_results_name_from - names of attributes of a Parser instance.
        If a token with a name in the list is re-assigned,
        the set_results_name is called with the new name.

    """

    set_results_name_from: List[str] = []

    def __init__(self, parse_actions=[]):
        """Create a new parser to customize.

        You can access all parsers as attributes.
        If you parse more than one object,
        you can reset the state with reset()."""
        self._actions = ParseActionsList(self)
        self.add_default_parse_actions()
        for parse_action in parse_actions:
            self.actions.add(parse_action)
        self.create_tokens()
        self.reset()

    def add_default_parse_actions(self):
        """Add no parse actions by default.

        Replace this method to call self.actions.add() and
        add parse actions that should be attached to the parser by default.
        """

    def create_tokens(self):
        """Create tokens with the parse actions given.

        For this base class, no tokens are created.
        """

    def __setattr__(self, name, token):
        """Set an attribute of the parser.

        Names with _ are not touched.
        Everything else is assumed to be a token.
        This also sets the name of the token.
        """
        if not name.startswith("_"):
            assert not hasattr(
                self, name
            ), f"{self.__class__.__name__} objects cannot have tokens assigned twice: '{name}' is already taken."
            token = token.copy()
            if token.customName in self.set_results_name_from:
                token = token.set_results_name(name)
            token.set_name(name)
            token = self.actions.attach(name, token)
        self.__dict__[name] = token

    def __getitem__(self, name: str) -> ParserElement:
        """Return a parser"""
        # TODO: check for "_"
        return self.__dict__[name]

    @property
    def actions(self) -> ParseActionsList:
        """Actions of the parser."""
        return self._actions

    def reset(self) -> None:
        """Reset the state of the parser."""
        self.actions.reset()


__all__ = ["Parser"]
