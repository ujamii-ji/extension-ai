# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""The modular_parser package decouples parse actions from grammar construction.

This should lead to a cleaner code structure: Parse actions can be re-used and
prioritized. They also do not exist within the construction of the grammar.
This way, the grammar is constructed without the clutter of attaching parse actions
to parsers.

This package is based on pyparsing version 3 and later.
"""
from .errors import *
from .parser import *
from .parse_actions import *
