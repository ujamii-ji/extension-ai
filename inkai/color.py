# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Mapping of the Adobe Illustrator color space to the SVG color space.

See
- 5.4.1 Color Operators in AI Spec
- inkex colors https://inkscape.gitlab.io/extensions/documentation/source/inkex.colors.html
- CMYK color space https://stackoverflow.com/a/23117858/1320237
"""
import inkex.colors
from typing import Optional


class Color:
    """Colors from Adobe Illustrator Documents."""

    def __init__(self, rgba: list, name: Optional[str] = None, stroke: bool = False):
        self._color = inkex.colors.Color(rgba, space="rgba")
        self.name = name
        self._stroke = stroke

    @property
    def alpha(self):
        """The transparency alpha value 0..1"""
        return self._color.alpha

    def to_rgb(self):
        """Return an rgb color."""
        return self._color.to_rgb()

    def is_stroke(self):
        """Whether this is a color for the stroke."""
        return self._stroke

    @classmethod
    def from_cmyk(
        cls,
        cyan: float,
        magenta: float,
        yellow: float,
        black: float,
        name: Optional[str] = None,
        tint: float = 1,
        stroke: bool = False,
    ):
        """Create a color from a CMYK color space."""
        assert 0 <= cyan <= 1
        assert 0 <= magenta <= 1
        assert 0 <= yellow <= 1
        assert 0 <= black <= 1
        assert 0 <= tint <= 1
        return cls(
            cmyk_to_rgb(cyan, magenta, yellow, black) + [tint], name=name, stroke=stroke
        )

    @classmethod
    def from_gray(cls, gray: float, stroke: bool = False):
        """Create a color from a gray space."""
        assert 0 <= gray <= 1
        return cls([float(gray), float(gray), float(gray), 1.0], stroke=stroke)

    @classmethod
    def from_rgb(
        cls,
        red: float,
        green: float,
        blue: float,
        name: Optional[str] = None,
        tint=1.0,
        stroke: bool = False,
    ):
        """Create a color from an RGB space."""
        assert 0 <= red <= 1
        assert 0 <= green <= 1
        assert 0 <= blue <= 1
        return cls(
            [float(red), float(green), float(blue), tint], name=name, stroke=stroke
        )


def cmyk_to_rgb(cyan: float, magenta: float, yellow: float, black: float) -> list:
    """Convert a 0..1 cmyk color into a 0..255 [red, green, blue] list.

    Color conversion is on page 60 AI Spec.
    """
    return [
        1.0 - min(1, cyan + black),
        1.0 - min(1, magenta + black),
        1.0 - min(1, yellow + black),
    ]
