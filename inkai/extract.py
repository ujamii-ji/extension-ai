# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Script to extract ai data.

poetry run python -m inkai.extract file.ai ...

Stores full extracted data in *.ai.txt
Stores text data in *.ai.extracted-text.json

"""
import os
import sys
import json
import argparse
from pathlib import Path
import warnings
from typing import Dict

HERE = os.path.dirname(__file__) or "."
# This is suggested by https://docs.python-guide.org/writing/structure/.
sys.path.insert(0, os.path.abspath(os.path.join(HERE, "..", "..", "..")))

from inkai.extraction import extract_ai_privatedata, ENCODING
from inkai.parser.ai import AdobeIllustratorParser
from inkai.modular_parser import ParseActions
from pyparsing import ParseResults


EXTRACTION_ENDING = ".txt"

parser = argparse.ArgumentParser(
    prog="Inkscape AI extraction tool",
    description="Unzips and additionally extracts (preprocessed) parts of the passed AI files.",
)

parser.add_argument("filename", nargs="*", help="One or more AI files")
parser.add_argument(
    "-n", "--if-newer", action="store_true", help="Only re-extract files if newer"
)
parser.add_argument(
    "-t",
    "--extract-text",
    action="store_true",
    help="Extract ASCII85 decoded text elements as json",
)

args = parser.parse_args()


class ParserElementExtraction(ParseActions):
    """Extract document elements into a file."""

    def set_file(self, file_name: Path):
        """Set the file name that we extract from."""
        self.file_name = file_name
        self.text_file = file_name.with_suffix(".ai.extracted-text.json")
        if os.path.exists(self.text_file):
            os.remove(self.text_file)
        self.text_printed = False
        self.data: Dict[str, object] = {}

    def parse_encoded_text(self, p: ParseResults):
        """Parse an encoded text and add it to the extracted text file."""
        self.data[p[0]] = p[1]
        return p

    def write_file(self):
        if not args.extract_text:
            return
        if not self.text_printed:
            print(f"\ttext in {self.text_file}")
            self.text_printed = True
        with open(self.text_file, "a") as file:
            json.dump(self.data, file, indent=4)


def main(files=args.filename):
    """Extract the files."""

    extraction = ParserElementExtraction()
    parser = AdobeIllustratorParser(parse_actions=[extraction])

    returncode = 0

    did_something = False
    for file_name in map(Path, files):
        print("Found file:", file_name)
        if file_name.suffix != ".ai":
            print(f"\t{file_name} is not an .ai file, skipping")
            continue
        output_file_name = file_name.with_suffix(".ai.txt")
        if output_file_name.exists() and args.if_newer:
            if file_name.stat().st_mtime > output_file_name.stat().st_mtime:
                print(f"\t{output_file_name} is old, replace")
            else:
                print(
                    f"\tskip: {output_file_name} exists and is newer than original document."
                )
                continue
        did_something = True
        extraction.set_file(file_name)
        with open(file_name, "rb") as file_in:
            ai_data = file_in.read()
            try:
                extracted_data = extract_ai_privatedata(ai_data)
            except Exception as e:
                # traceback.print_exc()
                print(f"\tbroken extraction: {e}")
                extracted_data = None
        if extracted_data is None:
            returncode = 1
            print("\tfail:", file_name)
        else:
            try:
                with warnings.catch_warnings():  # from http://stackoverflow.com/questions/14463277/ddg#14463362
                    warnings.simplefilter("ignore")
                    parser.parse_string(extracted_data)
            except Exception as e:
                print(f"\tbroken parsing: {e}")
                # traceback.print_exc()
            if len(ai_data) == len(extracted_data):
                print(
                    f"\tskip un-zip operation: extracted data equals the source file."
                )
                continue
            with open(
                output_file_name, "w", encoding=ENCODING, newline="\n"
            ) as file_out:
                file_out.write(extracted_data.replace("\r\n", "\n").replace("\r", "\n"))
            extraction.write_file()
            print(f"\tnew: {output_file_name}")

    if not did_something:
        print(f"Nothing to do! Add AI files to {HERE}.")
    if returncode != 0:
        print(
            "We could not process at least one file. Please consider adding that file to the tests/data/ai directory."
        )


if __name__ == "__main__":
    exit(main())
