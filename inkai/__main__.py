# SPDX-FileCopyrightText: 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

try:
    import inkex  # check inkex
    from . import errors  # check that we are a package
except ImportError:
    import os
    import sys

    # This is suggested by https://docs.python-guide.org/writing/structure/.
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
    try:
        from inkai.cmd import main
    except ImportError:
        from inkai.run_in_virtual_env import main
else:
    # All modules are installed and we are in a package
    # seems like we should be executed.
    from .cmd import main

main()
