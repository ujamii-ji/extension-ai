# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Benchmarks for parsing the data hierarchy."""

import time
from ..parser.ai import AdobeIllustratorParser
from ..parser.data_hierarchy import DataHierarchyParser
import os
from ..extraction import ENCODING

HERE = os.path.dirname(__file__) or "."
BENCHMARK_FILE = os.path.join(
    HERE, "..", "..", "tests", "data", "grammar", "document_data.txt"
)


def measure(parser, seconds=3):
    print("Measuring...")
    string = get_data()
    start = time.time()
    end = start + seconds
    calls = 0
    while time.time() < end:
        parser.parse_string(string, parse_all=True)
        calls += 1
    duration = time.time() - start
    print(f"{parser} completed {calls / duration} calls/second.")


def get_data():
    """Return data to measure."""
    with open(BENCHMARK_FILE, encoding=ENCODING) as file:
        return file.read()


def main():
    """Compare the different parsers in performance."""
    measure(AdobeIllustratorParser().benchmark_pyparsing_parsed_data_hierarchy_section)
    measure(DataHierarchyParser())


if __name__ == "__main__":
    main()
