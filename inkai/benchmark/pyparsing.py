# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Benchmarks for pyparsing."""

import pyparsing as pp
import time


command_fast: pp.ParserElement = pp.NoMatch()
command_or: pp.ParserElement = pp.NoMatch()
command_look_ahead: pp.ParserElement = pp.NoMatch()
inner: pp.ParserElement = pp.ZeroOrMore(pp.Char("a"))
letters = "xcvbnmasdfghjklwer1tyuiopz"
for c in letters:
    sub = inner + c
    command_fast = command_fast | sub
    command_or = command_or | sub
    command_look_ahead = command_look_ahead | (
        pp.SkipTo(c + pp.LineEnd()) + c
    ).set_parse_action(
        lambda p: list(inner.parse_string(p[0], parse_all=True)) + [p[-1]]
    )

command_fast.set_name("command_fast")
command_or.set_name("command_or")
command_look_ahead.set_name("command_look_ahead")
command_look_up_regex = (
    pp.Regex(f"(.*)([{letters}])", as_match=True)
    .set_parse_action(lambda p: list(inner.parse_string(p[0][1])) + [p[0][2]])
    .set_name("command_look_up_regex")
)
command_look_up_skip_to = (
    (pp.SkipTo(pp.Char(letters) + pp.LineEnd()) + pp.Char(letters))
    .set_parse_action(lambda p: list(inner.parse_string(p[0])) + [p[1]])
    .set_name("command_look_up_skip_to")
)

commands = [
    command_fast,
    command_or,
    command_look_ahead,
    command_look_up_regex,
    command_look_up_skip_to,
]


def measure_command(command: pp.ParserElement, seconds=3, steps=100):
    """Measure how fast a command is."""
    s = "a " * 200 + "z"  # z is last
    start = time.time()
    end = start + seconds
    calls = 0
    while time.time() < end:
        for i in range(steps):
            command.parse_string(s, parse_all=True)
            calls += 1
    duration = time.time() - start
    print(f"Command {command} completed {calls / duration} calls/second.")


def measure_caching(count=10000):
    """This measures if there is a cache between calls."""
    cmds = [(pp.Literal(str(i)), str(i)) for i in range(count)]
    t = time.time()
    for c, s in cmds:
        c.parse_string(s)
    print(f"separate commands {count / (time.time() -t)} calls/sec")
    t = time.time()
    for i in range(count):
        cmds[0][0].parse_string(cmds[0][1])
    print(f"same command on one string commands {count / (time.time() -t)} calls/sec")


def main():
    """Measure the time different commands take for the same task."""
    print("Measuring ...")
    for command in commands:
        measure_command(command)
    measure_caching()


if __name__ == "__main__":
    main()

__all__ = ["measure_caching", "measure_command", "commands"]
