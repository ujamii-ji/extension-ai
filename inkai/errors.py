# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Error classes."""


from .parse_actions.errors import *
from .modular_parser.errors import *
