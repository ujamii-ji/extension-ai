<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Text

Text is stored within an `AI11_TextDocument`, i.e. 

```
%AI11_BeginTextDocument
/AI11TextDocument : /ASCII85Decode ,
%[... blob ...]~>
7893 7770 /RulerOrigin ,
;
/AI11UndoFreeTextDocument : /ASCII85Decode ,
%[... blob ...]~>
;
%AI11_EndTextDocument
```

The individiual lines of the blob start with `%`. After Adobe-ASCII-85-decoding the blob, 
the text document is described by the following grammar 
(which is in this form used as unit test for the actual parser):

```
textdocument := object
object       := (flag + value)*
flag         := r"/(\d\d?|[\w\d]+)"
value        := subobject | list | number | 
                text | boolean | datatype 
subobject    := "<<" + object + ">>"
list         := "[" + value* + "]"
number       := int | float
text         := Anything between "(" and ")", possibly multiline; may include
                out-of-charset characters, non-quoting parentheses are 
                escaped with "\". 
                Uids are formatted in standard hex 8-4-4-4-12 representation.
                Other strings start with '\u00fe\u00ff', and are encoded UTF-16.
boolean      := "true" | "false"
datatype     := r"/[\w\d]+"
```

which gives rise to a deeply nested, hierarchical data structure that contains 
the actual text information, described below.

The `UndoFreeTextDocument` does not appear relevant for parsing the text.

## Text instance in the main document

### Simple text

```
/AI11Text :
0 /FreeUndo ,
0 /FrameIndex ,
0 /StoryIndex ,
2 /TextAntialiasing ,
;
```

`/StoryIndex` references the `Story`. `/FreeUndo` appears to be always 0. 

The actual text position is stored in the `/1 /1 /1 /0 /0` list, see below. `/FrameIndex` is only relevant for Area Type.

### Area Type and Text-on-path

The shape-inside is stored in `/ConfiningPath`. Click-dragging creates a non-live rectangle, 
however arbitrary shapes, including live shapes, may be used as confing objects.

```
/AI11Text :
0 /FreeUndo ,
0 /FrameIndex ,
1 /StoryIndex ,
/Art :
X=
1 Ap
4 As
<path object>
[<live shape definition>]
X+
; /ConfiningPath ,
2 /TextAntialiasing ,
;
```

If the Area Type consists of multiple connected areas, this section may be repeated (with incremented `/FrameIndex`).

Text-on-path has exactly the same syntax, the distinction between Text on Path and Area Type 
happens in the `/Frame` object (whether `/2 /11` is defined or not).

## Structure of the `/AI11TextDocument`

```
/98: ?
/0: Dict, definitions
  /1: Dict, contains a list of used fonts
    /0: List[FontDefinition]
    /1: ? - could be the index of the default font?
  /2: Dict, ?
  /3: Dict, ? has something to do with Japanese scripts
  /4: Dict, ?
  /9: Dict, enumeration and itemization type definitions
    /0: List[ListingStyle]
    /1: List, one entry for each default enumeration style
  /5: Dict, Character Styles defined in the document
    /0 List
      /0 Object
        /97: UUID
        /0: Name in the Window -> Type -> Character Style dialog
        /5: int, 0 (?)
        /6: CharacterStyle
  /6: Dict, Paragraph Styles defined in the document (analogous to /5)
  /8: Dict
    /0 List
      /0: Frame
/1: Dict
  /0: Dict, futher settings, references hunspell, references Adobe InvisFont
  /1: List[Story]
  /2: CharacterStyle
  /3: ParagraphStyle
```

The data types are further described below. In particular, the following lists are relevant for later index-based lookups:

- `/0 /1 /0`: contains the `FontDefinition`s
- `/0 /5 /0`: contains the `CharacterStyle`s
- `/0 /6 /0`: contains the `ParagraphStyle`s
- `/0 /8 /0`: contains the `Frame`s
- `/0 /9 /0`: contains the `ListingStyle`s

### `Story`

```
/0: content
  /0: actual text, characters are at least two bytes long (i.e. UTF-16). 
      Emojis?
      Paragraphs are created with \r, newlines with \u03.
  /5: Dict, contains overrides of the paragraph styles
      If multiple styles are used in a text element, their scope is given in
      "number of affected characters".
    /0: List
      /0: Dict 
        /0: int (index in the default paragraph styles) or dict, in which case:
          /5: ParagraphStyle, Overrides of the paragraph style 
      /1: Number of affected characters
  /6: Dict, contains overrides of the character styles.
      If multiple styles are used in a text element, their scope is given in
      "number of affected characters".
    /0: List
      /0: Dict
        /0: int (index in the default character styles) or dict, in which case:
          /6: CharacterStyle, Overrides of the character style
      /1: Number of affected characters

  /10: Dict (small), probably some settings
/1: layout information
  /0: List of Dicts
    /0: index of Frame. Note that flowtexts can have multiple defined.
        This appears to be duplicated in the main file ("/FrameIndex")
```


### `CharacterStyle`

Settings accessible only through the Window -> Type -> Character Style dialog are marked with `[*]`.

Undefined attributes are inherited from the [Normal Character Style]. 

```
/0 Font [index in the font list]
/1 Font size [pt]
/5 Line spacing [pt]
/6: Horizontal Scaling [factor]
/7: Vertical Scaling [factor]
/8: Tracking [percent]
/9: Baseline shift [pt]
/10: Character rotation [degrees, counterclockwise]
/11: 2: Optical Kerning 3: Metrics - Roman Kerning
/12: 1: Small caps, 2: All caps
/13: 0: normal, 1: Superscript, 2: Subscript
/14: OpenType Features: Position. 1: Superscript/Superior, 2: Subscript/Inferior,
            3: Numerator, 4: Denominator
/15: 0: no strikethrough, 1: Strikethrough
/16: 0: no underline, 1: Underline
/18: OpenType Features: Standard Ligatures [bool]
/19: OpenType Features: Discretionary Ligures [bool]
/20: OpenType Features: Contextual Alternates [bool]
/23: OpenType Features: Fractions [bool]
/24: OpenType Features: Ordinals [bool]
/25: OpenType Features: Swash [bool]
/26: OpenType Features: Titling Alternates [bool]
/28: OpenType Features: Stylistic Alternates [bool]
/30: OpenType Features: Figure. 0: Default Figure, 1: Tabular Lining, 
            2: Proportional Old Style, 3: Proportional Lining, 4: Tabular Old Style
/35: [*] 1: Standard Vertical Roman Alignment 
/38: "Language" [enum]
/53: Fill Color: Object of type /CAITextPaint
  /0: Object
    /1: List of components. 
       RGB documents: [1 R G B, values between 0 and 1]
       CMYK documents: [1 C M Y K, values between 0 and 1]
/54: Stroke Color: Object of type /CAITextPaint
/55: Opacity: Object of type /CAITextBlender
  /0: Blend mode. 1: Multiply, 2: Screen, 3: Overlay, 
            4: Soft Light, 5: Hard Light, 6: Color Dodge, 
            7: Color Burn, 8: Darken, 9: Lighten,
            10: Difference, 11: Exclusion, 12: Hue, 
            13: Saturation, 14: Color, 15: Luminosity
  /1: Opacity [0-1]
  /2: 1: Isolate Blending enabled
  /3: 1: Knockout group enabled, 
      2: Knockout group "half enabled" (minus in checkbox)
  /4: 1: "Opacity Mask and define Knockout shape"
/59: [*] Overprint Fill [bool]
/60: [*] Overprint Stroke [bool]
/61: Stroke caps. 0: Butt, 1: Round, 2: Square 
/62: Stroke linejoin. 0: Miter Join, 1: Round join, 2: Bevel join
/63: Stroke width [px]. Default: 1px (usually /54 is not defined)
/64: Stroke miterlimit [factor of stroke width]
/66: Stroke dasharray [Array of px values]
/85: OpenType Features: Stylistic Sets 
     [binary mask, 1 = Set 1, 2 = Set 2, 4 = Set 3 etc.]
/89: Font size [pt]
```

### `FontDefinition`

```
/99 Datatype: /CoolTypeFont
/97: UUID
/0: Dict
  /0: Font name, encoded in UTF-16. Also includes variant (e.g. "-Italic")
      as well as the weight for variable font: "_535.000wght". 
  /2: Int, ?
  /4: List[int] ?
  /5: Font version
```
# `ListingStyle`

All `[pt]` values here are understood to be relative to the font size.
```
/0: Wrapper Object
  /97: UUID
  /0: Name, either predefined ("kPredefinedEmptyCircleBulletListStyleTag") 
            or random-custom ("Custom_1680435898608793")
  /5: List[ListingStyleLevel]
    /0: int, always 1 (?)
    /1: left indent [pt]
    /2: first line indent [pt], usually negative
    /3: 0
    /5: Object
      /99: Type of sequence generator. Types: 
           /AlphabeticSequenceGenerator, 
           /ArabicNumberSequenceGenerator, 
           /BulletSequenceGenerator
           /RomanNumeralSequenceGenerator
      /0: Character(s) before enumeration symbol
      /1: Character(s) after enumeration symbol
      /3: EnumerationChar. Meaning depends on type of sequence generator
        For /AlphabeticSequenceGenerator: 
          Either 64 (upper-case) or 96 (lower case). 
          This is the character code one before the first enumeration 
          symbol (A = 65, a = 97).
        For /RomanNumeralSequenceGenerator:
          Either 64 (upper-case) or 96 (lower case). 
        For /BulletSequenceGenerator: indicates type of bullet used, 
          encountered values:
            ' "' - U+2022 center dot
            '%\aa' - filled square
            '%\e6' - empty center circle
            '%\ab' - empty center square
        For /ArabicNumberSequenceGenerator: 0

    /6: int (?)
    /7: bool (?)
```


### `ParagraphStyle`

All `[pt]` values here are understood to be relative to the font size.

```
/0: Justification. 0: align left, 1: align right, 2: align center
                   3: justify, last line aligned left
                   4: justify, last line aligned right
                   5: justify, last line center-aligned
                   6: justify all lines
/1: First-line left indent [pt]
/2: Left indent [pt]
/3: Right indent [pt]
/4: Space before paragraph [pt]
/5: Space after paragraph [pt]
/7: Auto Leading [float]
/9: hypenation enabled [bool]
/10: Hypenate words longer than [int] letters
/11: Hypenate after first [int] letters
/12: Hypenate before last [int] letters
/13: Hyphen limit: [int] letters
/14: Hyphenation zone [pt]
/16: Float between 0: Better Spacing, 0.5: standard, 1: Fewer Hyphens
/17: Justification (Word spacing): List[float]: [Minimum, Desired, Maximum]  
/18: Justification (Letter spacing): List[float]: [Minimum, Desired, Maximum]  
/19: Justification (Glyph spacing): List[float]: [Minimum, Desired, Maximum]  
/20: Single-word justification: 0: align Left, 1: align right, 2: align center, 
                                6: Full justify 
/21: Roman Hanging Punctuation [bool]
/29: Composer: true: Adobe Every-Line Composer, 
               false: Adobe Single Line Composer
/30: Tabs
  /0: List of tabs
    /0: Position [pt]
    /1: Type. 0: left-justified tab, 1: Center-justified tab, 
          2: Right-justified tab, 3: Decimal-justified tab
    /2: Leader [str]
    /3: for decimal-justified tabs: character to align at [str]
/31 float?
/32: CharacterStyle, Default character style for this paragraph style.
/36: Bullets/Enumeration [index in the enumeration and itemization type defs], 
     no enumeration: "/nil"
/37: Bullets/Enumeration level [0-based index]
```

### `Frame`

All `[pt]` values here are understood to be relative to the font size.
```
/97: UUID
/0: Position of the start of the baseline of the first character, 
   in canvas coordinates. 
/1: Dict
  /0: List of canvas coordinates, only for text-in-shape
/2: Dict
  /1: Orientation. 2: Vertical (top to bottom), also applies to Text on Path
  /2: Transform [a b c d e f]. Used for translate, rotate and shear.
      Scale transforms are performed by changing the font size 
      (and scale_x / scale_y if uneven scaling is desired).
  /3: (Area type): Number of rows
  /4: (Area type): Number of columns
  /5: (Area type): Row/column order: false: Row-major, true: Column-major
  /6: (Text on path): List[float], specifies the start and end position on the path.
      A value of 3.2 indicates "at t=0.2 on the bezier segment between Node 3 and Node 4".
  /7: (Area type): Row gutter
  /8: (Area type): Column gutter
  /9: (Area type): Inset spacing [pt]
  /10: (Area type): Settings for the first baseline
    /0: First baseline: 0: Fixed, Unspecified: Ascent, 2: Cap height, 3: Leading, 
                        4: By x, 5: Em Box Height, 6: Legacy
    /1: Minimum width of first baseline [pt]
  /11: Settings for text on path
    /0: Flipped [bool], default: false
    /1: Effect type: 0: Rainbow (default), 1: Skew, 2: Ribbon, 3: 3D Stairstep, 4: Gravity,
    /2: Align to: Unspecified: Baseline, 0: Ascender, 1: Descender, 2: Center
    /4: Spacing [pt]
    /18: Spacing [pt]
  /13: (Area type): Vertical Alignment: Unspecified: Top, 1: Center, 2: Bottom, 3: Justify

```

## Implementation in Inkscape SVG

Inkscape generally supports texts, Area Text and Text on path very well.
The majority of features of the text document are supported by SVG2 / CSS Fonts Level 4, 
although not all of it may be supported by Inkscape, or even browsers 
(browsers often lack SVG support of CSS features they support for normal websites). 
Such features have low priority for the importer.

Text elements will consist of multiple `<tspan>`s, which have different styles applied to them. 
This is governed by the character / paragraph style settings for each `Story`. 

The default character and paragraph styles should be implemented as CSS rules that affect all text-like elements, 
if it is possible to specify the property independent of font size (e.g. in percent). Overriden properties are
specified on the `<tspan>`/`<text>`.
The distinction between character and paragraph style doesn't exist in Inkscape and is not required.

Refer to the [`<text>` SVG specification](https://www.w3.org/TR/SVG2/text.html#Introduction) 
and in particular [`shape-inside`](https://www.w3.org/TR/SVG2/text.html#TextShapeInside) for Area Text 
and [`<textPath>`](https://www.w3.org/TR/SVG2/text.html#TextPathElement) for Text-on-path, 
and to the [`CSS Fonts specification`](https://drafts.csswg.org/css-fonts/).

### Known unsupported features

Even SVG2 texts (which are not supported by browsers) don't really have advanced paragraph layouting capabilities. 
That means that we won't be able to support Enumeration / Itemization, 
(anything except [basic](https://www.w3.org/TR/css-text-3/#text-indent-property)) indentation, tab stops etc. 
Furthermore, SVG `shape-inside` [does not support](https://gitlab.com/inkscape/inbox/-/issues/760) vertical alignment in Inkscape.

SVG `shape-inside` flowed texts don't support multi-row / multi-column arrangements. 
A possible very-low-priority workaround would be splitting the `/ConfiningPath` into multiple 
subpaths according to the row/column arrangement 
(keeping the fact in mind that `Inset Spacing` is not applied to rows/columns, 
but `shape-padding` is applied to all subpaths of the `shape inside`).

