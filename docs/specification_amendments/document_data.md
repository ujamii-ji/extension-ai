<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Document data

The document data starts with `%AI9_BeginDocumentData` and ends with `%AI9_EndDocumentData`. 
The data structure was added in AI9, but attributes were added in later versions as well.

It contains:

- Page setup
- some grid settings
- PDF export settings
- Document settings
- `AI12 Document Profile data` - binary, ASCII85 encoded
- ...

## Data structure

All lines start with `%` additional to what's shown here, and this section describes the syntax encountered between the enclosing `%AI9_BeginDocumentData` and `%AI9_EndDocumentData`.

Data at the leaf nodes has the format `_data /DataType (key) ,` (terminated with comma). Known data types: `String`, `Int`, `Bool`, `Real`, `UnicodeString`, `RealPoint`, `RealMatrix` `RealPointRelToROrigin`. Newlines are allowed, and are e.g. found when data contains a space (e.g. `RealPoint`), in this case a newline is inserted after the datatype:

```
_0 0 /RealPointRelToROrigin
_ (PositionPoint1) ,
```

Data is stored in containers. 

Containers are opened with `_/ContainerType : [/descriptor ],` and terminated with `_; [(key) ], ` (`[]` indicating optional entry)

Known `ContainerType`'s:
* Leaf nodes are always direct children of `/Dictionary`. However, a dictionary may also contain other containers; in this case, a key must be specified on the child container, which is optional otherwise. As `/descriptor`, a dictionary may specify `/NotRecorded`.
* The top-level container is `/Document`, which is a otherwise identical to a dictionary.
* An `/Array` is a group in which elements are not qualified by key, but by order. An example is the group of children on an XML element.
* An `/XMLNode` is a special dictionary with the keys `xmlnode-children` (an `/Array`), `xmlnode-attributes` (a `/Dictionary` which might also contain other `XMLNode`s, might be similar to XAML?), and the values `xmlnode-nodename` (string), `xmlnode-nodevalue` (string; the text of the XML element) and `xmlnode-nodetype` (int; values 1, 2, and 9 encountered).
* `/Binary` has the encoding set as descriptor, e.g. `/ASCII85Decode` (Adobe variant of ASCII85). Unlike all other data, the lines only start with `%`, not `%_`.

## Retrieving all Operators

```
for i in `grep -sEhro  '(^| |_)/[A-Z][a-zA-Z]*( |$)' tests/data/ai/*.txt | grep -iEo '[^ _]+' | sort | uniq`; do  echo '- [ ] `'$i'`  '; echo '  Example:'; echo '  ```'; grep -rsEh "(^| |_)$i( |$)" tests/data/ai/*.txt | sed 's/^%_//' | sed 's/ +$//' | sort | uniq | head -n 10 | sed 's/^/  /g'; echo '  ```'; done 
```

The command above yields this list of operators from extracted files:

- [ ] `/ActiveStyle`  
  Example:
  ```
  /ActiveStyle :
  ```
- [x] `/Array`  
  Example:
  ```
  /Array :
  ```
- [ ] `/Art`  
  Example:
  ```
  /Art :
  ```
- [x] `/ArtDictionary`  
  Example:
  ```
  /ArtDictionary :
  /ArtDictionary :
  ```
- [ ] `/BasicFilter`  
  Example:
  ```
  /BasicFilter :
  ```
- [x] `/Binary`  
  Example:
  ```
  /Binary : /ASCII85Decode ,
  ```
- [ ] `/BlendStyle`  
  Example:
  ```
  /BlendStyle : 0 0.52 0 0 0 Xy
  /BlendStyle : 0 0.6 0 0 0 Xy
  /BlendStyle : 0 0.74 0 0 0 Xy
  /BlendStyle : 0 1 0 0 0 Xy
  /BlendStyle : 0 1 1 0 0 Xy
  /BlendStyle : 1 0.45 0 0 0 Xy
  /BlendStyle : 1 0.47 0 0 0 Xy
  /BlendStyle : 1 1 0 0 0 Xy
  /BlendStyle : 2 0.51 0 0 0 Xy
  /BlendStyle : 2 0.7 0 0 0 Xy
  ```
- [x] `/Bool`  
  Example:
  ```
  0 /Bool (AI10 flattener outline strokes) ,
  0 /Bool (AI10 flattener outline text) ,
  0 /Bool (AI11 document isolate blending) ,
  0 /Bool (AI11 document knockout group) ,
  0 /Bool (AI11 EPS Outline Text) ,
  0 /Bool (AI12 AI Outline Text) ,
  0 /Bool (AI13PatternEnableGuides) ,
  0 /Bool (AI15 Document PixelPerfect) ,
  0 /Bool (AI15PixelPerfectSymbolEnablekey) ,
  0 /Bool (AI16 AI Anti Aliasing) ,
  ```
- [ ] `/CompoundFilter`  
  Example:
  ```
  /CompoundFilter :
  ```
- [ ] `/ConfiningPath`  
  Example:
  ```
  ; /ConfiningPath ,
  ```
- [ ] `/Def`  
  Example:
  ```
   /Def ;
  /Def ; 
  ; /Def ;
  ```
- [ ] `/DeviceRGB`  
  Example:
  ```
  /DeviceRGB XN
  ```
- [ ] `/Dict`  
  Example:
  ```
  ; /Dict ;
  ```
- [x] `/Dictionary`  
  Example:
  ```
  /Dictionary :
  /Dictionary : /NotRecorded ,
  /Dictionary : /NotRecorded ,
  ```
- [ ] `/Document`  
  Example:
  ```
  /Document :
  ```
- [ ] `/Execution`  
  Example:
  ```
  /Execution ;
  ```
- [ ] `/FillOrStroke`  
  Example:
  ```
  2 /FillOrStroke ,
  ```
- [ ] `/FillStyle`  
  Example:
  ```
  /FillStyle : 0 1 0 0 0 Xy
  /FillStyle : 0 O
  ```
- [ ] `/Filter`  
  Example:
  ```
  (Adobe Drop Shadow) 1 0 /Filter ,
  (Adobe Fuzzy Mask) 1 0 /Filter ,
  (Adobe Offset Path) 1 0 /Filter ,
  (Adobe Round Corners) 1 0 /Filter ,
  (Adobe Stroke Offset) 1 0 /Filter ,
  (Adobe Transform) 1 0 /Filter ,
  (Blend Style Filter) 0 0 /Filter ,
  (Chain Style Filter) 0 0 /Filter ,
  (Conduit Filter) 0 0 /Filter ,
  (Fill Style Filter) 0 0 /Filter ,
  ```
- [ ] `/FrameIndex`  
  Example:
  ```
  0 /FrameIndex ,
  ```
- [ ] `/FreeUndo`  
  Example:
  ```
  0 /FreeUndo ,
  ```
- [x] `/Int`  
  Example:
  ```
  0 /Int (AI10 compound shape mode) ,
  0 /Int (/attributes/AI10 flattener outline text) ,
  0 /Int (/attributes/AI11PDF_ColorBars) ,
  0 /Int (/attributes/AI11PDF_FastWebView) ,
  0 /Int (/attributes/AI11PDF_FlattenTransparency) ,
  0 /Int (/attributes/AI11PDF_PageInfo) ,
  0 /Int (/attributes/AI11PDF_PrinterMarkType) ,
  0 /Int (/attributes/AI11PDF_RegMarks) ,
  0 /Int (/attributes/AI11PDF_TrimMarks) ,
  0 /Int (/attributes/AI12PDF_Trapped) ,
  ```
- [ ] `/KnownStyle`  
  Example:
  ```
  /KnownStyle :
  ```
- [x] `/Name`  
  Example:
  ```
  (AbenddÃ¤mmerung) /Name ,
  (Abgerundete Ecken 2 Pt.) /Name ,
  (Anon 195) /Name ,
  (Anon d+3FScT4a+0=) /Name ,
  (Anon) /Name ,
  ([Default]) /Name ,
  (Interaktiv X spiegeln) /Name ,
  (Laub_GS) /Name ,
  (Pompadour_GS) /Name ,
  (Schlagschatten) /Name ,
  ```
- [x] `/NotRecorded`  
  Example:
  ```
  /Dictionary : /NotRecorded ,
  /Dictionary : /NotRecorded ,
  ; /NotRecorded ,
  ```
- [ ] `/Paint`  
  Example:
  ```
  /Paint ;
  ```
- [ ] `/Part`  
  Example:
  ```
   /Part ,
  ```
- [ ] `/PluginFileName`  
  Example:
  ```
  (DropShadow.aip) /PluginFileName ,
  (FuzzyEffect.aip) /PluginFileName ,
  (OffsetPath.aip) /PluginFileName ,
  (Round.aip) /PluginFileName ,
  (StrokeOffset.aip) /PluginFileName ,
  (Transform.aip) /PluginFileName ,
  ```
- [x] `/Real`  
  Example:
  ```
  0.24 /Real (Raster Art Original Scale) ,
  0.25 /Real (/attributes/AI11PDF_TrimMarkWeight) ,
  0.25 /Real (/attributes/pgmk.deflinewidth) ,
  0.75 /Real (opac) ,
  0 /Real (ai::Rectangle::Angle) ,
  0 /Real (ai::Rectangle::CornerRadius::0) ,
  0 /Real (ai::Rectangle::CornerRadius::1) ,
  0 /Real (ai::Rectangle::CornerRadius::2) ,
  0 /Real (ai::Rectangle::CornerRadius::3) ,
  0 /Real (/attributes/AI11PDF_BleedBottom) ,
  ```
- [x] `/RealMatrix`  
  Example:
  ```
  1 0 0 1 39342.5586 37062.6875 /RealMatrix
  1 0 0 1 39406.5586 37077.2266 /RealMatrix
  1 0 0 -1 39406.5586 53548.4453 /RealMatrix
  -1 0 0 1 55766.9844 37062.6875 /RealMatrix
  -1 0 0 1 55830.9844 37077.2266 /RealMatrix
  -1 0 0 -1 55830.9844 53548.4453 /RealMatrix
  ```
- [ ] `/Recorded`  
  Example:
  ```
  ; /Recorded ,
  ```
- [ ] `/RulerOrigin`  
  Example:
  ```
  7893 7770 /RulerOrigin ,
  7895 8612 /RulerOrigin ,
  ```
- [ ] `/SimpleStyle`  
  Example:
  ```
  /SimpleStyle :
  ```
- [ ] `/StoryIndex`  
  Example:
  ```
  0 /StoryIndex ,
  ```
- [x] `/String`  
  Example:
  ```
  (0.509492) /String (BBAccumRotation) ,
  (14.0.0) /String (kAIFullDocumentVersionStr) ,
  (-1.837222) /String (BBAccumRotation) ,
  (-1.837222) /String (BBAccumRotation) ,
  (2.080288) /String (BBAccumRotation) ,
  (27.1.1) /String (kAIFullDocumentVersionStr) ,
  (2f56b345-ae09-4763-bcb8-5a769858b3d2) /String (ArtboardUUID) ,
  (-3.141592) /String (BBAccumRotation) ,
  (-3.141592) /String (BBAccumRotation) ,
  (-3.141593) /String (BBAccumRotation) ,
  /String  # empty
  ```
- [ ] `/StrokeStyle`  
  Example:
  ```
  /StrokeStyle : 0 1 0 0 0 Xy
  /StrokeStyle : 0 R
  ```
- [ ] `/SVGFilter`  
  Example:
  ```
  /SVGFilter :
  ```
- [ ] `/TextAntialiasing`  
  Example:
  ```
  2 /TextAntialiasing ,
  ```
- [ ] `/Title`  
  Example:
  ```
  (Ecken abrunden) /Title ,
  (Konturverschiebung: Live-Effekt) /Title ,
  (Pfad verschieben) /Title ,
  (Schlagschatten) /Title ,
  (Transformieren) /Title ,
  (Weiche Kante) /Title ,
  ```
- [x] `/UnicodeString`  
  Example:
  ```
  (0 0 0 0 0  0 0 0 0 0  0 0 0 1 0  0 0 0 1 0) /UnicodeString (xmlnode-nodevalue) ,
  (0 0 0 0 0  0 0 0 1 0  0 0 0 1 0  0 0 0 1 0) /UnicodeString (xmlnode-nodevalue) ,
  (0.05) /UnicodeString (xmlnode-nodevalue) ,
  (0.1) /UnicodeString (xmlnode-nodevalue) ,
  (0.2) /UnicodeString (xmlnode-nodevalue) ,
  (0.4) /UnicodeString (xmlnode-nodevalue) ,
  (.05) /UnicodeString (xmlnode-nodevalue) ,
  (0.5) /UnicodeString (xmlnode-nodevalue) ,
  (0689a395-1393-413f-9ee3-c1f8e56062b5) /UnicodeString (AI24 ImageAlphaRawDataUUID) ,
  (0.7) /UnicodeString (xmlnode-nodevalue) ,
  /UnicodeString # empty
  ```
- [ ] `/Visible`  
  Example:
  ```
  1 /Visible ,
  ```
- [x] `/XMLNode`  
  Example:
  ```
  /XMLNode :
  ```
- [x] `/XMLUID`  
  Example:
  ```
  /XMLUID : (Ebene_1) ; (AI10_ArtUID) ,
  /XMLUID : (Layer_1) ; (AI10_ArtUID) ,
  ```


## Page setup

The page setup is stored in an `/Array` with key `ArtboardArray`, which itself is a direct child of `/Document`. Each page is a `/Dictionary`.

```
%_/Array :
%_/Dictionary :
%_(Page 1) /UnicodeString (Name) ,
%_1 /Real (PAR) ,
%_0 /Bool (IsArtboardSelected) ,
%_0 /Int (DisplayMark) ,
%_7231 7651 /RealPoint
%_ (RulerOrigin) ,
%_0 /Bool (IsArtboardDefaultName) ,
%_0 0 /RealPointRelToROrigin
%_ (PositionPoint1) ,
%_(9bd9a3f4-3094-4e20-a475-f003b8aa0da8) /String (ArtboardUUID) ,
%_1920 -1080 /RealPointRelToROrigin
%_ (PositionPoint2) ,
%_; ,
%_/Dictionary :
%_(Page 2) 
[...]
%_; ,
%_; (ArtboardArray) ,
```

* `PositionPoint1` specifies the top-left corner, `PositionPoint2` the bottom-right corner 
in drawing coordinates (`/RealPointRelToROrigin` - relative to (global) ruler origin)
* `IsArtboardSelected`: currently active page
* `DisplayMark`: binary mask. 1 = Show center mark, 2 = Show cross hairs, 4 = show video save areas.
*  `RulerOrigin` specifies the origin of the *artboard ruler* in canvas coordinates.
* `PAR`, ?

AI only has document-wide bleeds (`BleedLeftValue` etc. in the root `/Document` container), specified in px. These should be added to all pages (requires Inkscape 1.3). Margins are only supported through guides (can be added by converting an object (Right click -> Make guides)).