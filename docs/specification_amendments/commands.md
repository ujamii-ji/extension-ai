<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->
# Overview of operators

List obtained by running

```
 egrep -aore "%_\w\w?$|\s\w\w?$|^\w.?|^.\w?$" *.ai.txt | sort | uniq
```

on the extracted data and removing false positives.

## `A`: Locked

See specification.

## `Ae`: Group expanded

```
isExpanded Ae
```

Always occurs before an `u`, `*u` or `q` operator. If 1, the group / clip group is displayed as expanded in the Layers panel.

Added in CC Legacy.

## `AE`: Layer expanded

```
isExpanded AE
```

If 1, the layer is displayed as expanded in the Layers panel.

Example:

```
%AI5_BeginLayer
1 1 1 1 0 0 1 4 255 79 255 0 50 0 Lb
(Sublayer 2_1) Ln
1 AE
...
```

Added in CC Legacy.

## `Ap`: Show Path center point

See specification.

## `As`: Number of nodes

```
<int> As
```

Appears to indicate the number of nodes of a (sub)path. 
For a closed path, this is equal to the number of following path segments, not counting the initial moveto command. 
For an open path, this is equal to the number of following path segments, including the initial moveto command.

Example:

```
4 As
0 O
0.1317 0.2608 0.6353 0.0439 k
4.53906 1.3418 m
4.53906 3.91895 2.44922 6.00977 -0.129883 6.00977 c
-2.70898 6.00977 -4.79883 3.91895 -4.79883 1.3418 c
-4.79883 -1.2373 -2.70898 -3.32813 -0.129883 -3.32813 c
2.44922 -3.32813 4.53906 -1.2373 4.53906 1.3418 c
f
```

Added in v2020.

## `b` / `B`: Close, fill and stroke path / Fill and stroke path

See specification.

## `Bb`: Begin gradient instance

See specification.

## `BB`: End gradient instance

See specification.

## `Bc`: Define gradient instance cap

See specification.

## `Bd` / `BD`: Begin / Terminate gradient definition

See specification.

## `Bg`: Define gradient instance geometry

```
flag (name) xOrigin yOrigin angle length a b c d tx ty Bg new_flag Bg
```

Example:
```
2 (Gradient Name) 0 0 0 1 1 0 0 1 0 0 1 Bg
```

The specification has only `flag name xOrigin yOrigin angle length a b c d tx ty Bg`. Since `1 0 0 1 0 0` looks like a unit transformation matrix, there is one extra flag at the end of (until now) unknown origin. All test files have a `1` as value of this flag, and changing it doesn't change the gradient settings in the UI.

The flag was added in CS4.

## `Bh`: Define gradient instance highlight

See specification.

## `Bm`: Set gradient matrix 

See specification.

## `Bn`: Number of cached gradients

See specification.

## `%_Br`: Gradient Ramp

See specification.

## `Bs` and `%_BS`: Gradient stop definition

Since V8, `%_Bs` has been replaced by `Data %_BS` and `%_Data Bs`. They have a similar syntax as
`%_Bs`. Apparently, the `%_BS` operator is ignored - at least neither deleting nor changing it 
changes the way the file opens. Deleting the `Bs` operator deletes the stop, and changing the data
is reflected when opening the file.

```
colorSpec colorType midpoint ramppoint %_BS # ignored on open, might be a fallback for older versions
%_colorSpec colorType midpoint ramppoint Bs
```

For named colors, the CMYK and RGB value defined in `%_BS` may differ 
from the one defined in `Bs` in the third significant digit.

Two new color types have been added:

* `colortype=5` (named / normed colors): parameters seem to be identical to `colortype=4`

* `colortype=6` (color with opacity) takes the following parameters: 
    ```
    sub_color_spec subtype opacity # where subtype=0..5
    ```
    This color type allows to specify opacity in addition the color. It is used by default for freely-defined and named colors. This color type takes other color types as subarguments, e.g. `subtype=2` (specifying that the color data is `C M Y K R G B`, `subtype=0` (color data is a greyscale percentage)) or the new `subtype=5`.

Example:

```
0.5 0 0.7 6 80.4707 73.8792 %_BS  # 50% grey with 70% opacity
%_0.5 0 0.7 6 80.4707 73.8792 Bs
0 0 0 0 1 1 1 2 0.5 6 50 81.6546 %_BS # white with 50% opacity
%_0 0 0 0 1 1 1 2 0.5 6 50 81.6546 Bs
0.166 0.183 0.923 0.0235 0.858 0.752 0.121 2 1 6 50 82.3054 %_BS
%_0.166 0.183 0.923 0.0235 0.858 0.752 0.121 2 1 6 50 82.3054 Bs
```

```
0.136 0.099 0.951 0.003 0.909 0.827 0 2 1 6 50 29.524 %_BS
%_0.138 0.101 0.952 0.004 0.908 0.826 0 (PANTONE P 1-16 C) 0 1 5 1 6 50 29.524 Bs
```

## `c` and `C`: Curveto

See specification.

## `d`: set dash

See specification.

## `D`: Winding Order

See specification.

## `E`: End pattern definition

See specification.

## `f` / `F`: Close & fill path / Fill path

See specification.

## `g` / `G`: Fill / Stroke setgray

See specification.

## `h`: Close path

See specification.

## `j`: linejoin

See specification.

## `l` / `L`: lineto

See specification.

## `Lb` / `Ln` / `LB`: Layer begin / Layer name / Layer end

See specification.

## `m` / `M`: moveto

See specification.

## `n` / `N`: (Close) path, neither filled nor stroked

See specification.

## `Np`: Identify nonprinting objects

See specification.

## `O`: Fill overprint

See specification.

## `p` / `P`: Fill pattern / Stroke pattern reference

See specification.

## `Pb` / `PB`: Begin Palette / End Palette

See specification.

## `Pc`: Palette cell

See specification.

## `Pg`: Palette group

```
1 (Group name) 1 Pg
```

Palettes can contain groups. 

Example:
``` 
%AI5_BeginPalette
0 0 Pb
0.911711 0.786862 0.619532 0.974487 0 0 0 ([Registration]) 0 1 Xz
([Registration])
Pc
... other entries
1 (Grays) 1 Pg
0.911711 0.786862 0.619532 0.974487 0 0 0 Xa
(R=0 G=0 B=0)
Pc
0.762295 0.667903 0.606867 0.828611 0.101961 0.101961 0.101961 Xa
(R=26 G=26 B=26)
Pc
PB
%AI5_EndPalette
```

The meaning of the flags is not yet known; none of the example files show values other than 1.

Added in CS3.

## `q`/ `Q`: group / ungroup from group that contains clipping

See specification.

## `R`: Stroke overprint

See specification.

## `s` / `S`: stroke closed path / stroke path

See specification.

## `u` / `U`: group / ungroup

See specification.

## `*u` / `*U`: Begin / end compound path

See specification.

## `v` / `V`: curveto

See specification.

## `w`: Set line width

See specification.

## `W`: Set clip

See specification.

## `X=` / `X+`: ?

TODO

## `Xa`: Fill color / `XA`: Stroke color

```
C M Y K R G B Xa
```

Unlike in the specification, `Xa` is used to specify both RGB and CYMK. All values are floats between 0 and 1. In turn, `k` and `K` are not used anymore.

Example:

```
Pc
0.726 0.666 0.652 0.782 0.101 0.101 0.101 Xa
(R=26 G=26 B=26)
```

Changed in v9.

## `Xd`: ?

```
0|1 0|1 Xd
```

TODO

## `XD`: ?

Example:
```
(007F647F) Xt 0 XD
```

TODO

## `XG`: Image Link Operator

See specification.

## `Xh`: ?

```
[ a b c d e f ] int int 0 Xh
```
TODO: Related to raster images.

## `XH`: ?

```
XH
```

TODO: Related to raster images.

## `XI`: Image definition

Parameters have changed. TODO

## `Xk`: Named color instance

```
C M Y K R G B (name) tint type Xk
```

Example:

```
0.001 0.995 0.973 0.002 0.890 0.022 0.075 (FOCOLTONE 1076) 0 1 Xk
```

## `Xm`: Level 3 linear gradient matrix

See specification.

## `XN`: Color space for raster images

```
/DeviceCMYK|/DeviceRGB XN
```

Added in CS3.

## `XP`: ??

Example:

```
(Adobe ArtOnPath Brush Tool) 1 0 39 XP
%040302010000803F00000000000000010100000043686172636F616C202D
%204665617468657200
(Adobe PatternOnPath Brush Tool) 1 0 31 XP
%040302010000803F00000000000000010000000044656E696D205365616D
%00
```

TODO


## `XR`: Fill rule

See specification.

## `Xw`: ??

```
0|1 Xw
```

## `XW`: ??

```
number (Name) XW
```

Name: References an ArtStyle, used for e.g. markers. May be empty.

Number: Values 0, 1, 6, 9

Added in v9.

## `Xy`: ??

```
int float 0 0 0 Xy
```
TODO

Added in v9.

## `Xz` / `XZ`: ??

```
float float float float 0 0 0 ([Name]) float 1 Xz/XZ
```

TODO

Added in v9.


## `y` / `Y`: curveto

See specification.