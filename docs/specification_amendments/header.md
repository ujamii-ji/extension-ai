<!--
SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>

SPDX-License-Identifier: GPL-2.0-or-later
-->

## Header

See page 15 AI Spec - 2.1.1 Header Comments.

The header was extended in several ways.

### BeginData

This contains a block of data.

```
%%BeginData: <arguments>
...
%%EndData
```

Example from ai7:
```
%%BeginData: 5441 Hex Bytes
%0000330000660000990000CC0033000033330033660033990033CC0033FF
...
%F82752A8FD71FFA87D27FD04F82727527DA8FD39FFFF
%%EndData
```
