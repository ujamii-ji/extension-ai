<!--
SPDX-FileCopyrightText: 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Adobe Illustrator Extension for [Inkscape]

This is the source code to open Adobe Illustrator files in [Inkscape].
This is an [Inkscape plugin][plugin].
Some features already work.
If you like to try it out, please follow the tutorial below.

You can also
- [view videos about this extension](https://diode.zone/c/inkai/)

## Development Setup
[Development Setup]: #development-setup

This section should get you ready setup so you can develop this plugin.

1. Install Python 3 and `pip3` and `git`. Example for Debian:
   ```
   sudo apt-get install python3-pip git
   ```
2. Clone the repository:
   ```
   git clone https://gitlab.com/inkscape/extras/extension-ai.git
   cd extension-ai
   ```
3. Install the development dependencies:
   ```
   pip install poetry
   poetry install
   poetry run pip install -e .
   ```
4. Install the pre-commit hook:
   ```
   pre-commit install
   ```
5. Run all the tests:
   ```
   tox
   ```

### Testing
[Testing Section]: #testing

We endeavor to cover as much code as possible with tests using test driven development.

There are tests for each **Python version**:
```
tox -e py39
```

These tests check that the **type annotations** are correct:
```
tox -e types
```

These tests check that the **coding style** is black.
The pre-commit hook should do that automatically for you.
```
tox -e black
```

These tests check that the **licensing** complies with the REUSE Specification:
```
tox -e reuse
```

### Reading AI Files

Older files are in a text format. Later files are inside a PDF as text but encoded in an unreadable way. Because of this and for development purposes, it is handy to extract files to look at them. This command should extract all readable files in one of the test folder so you can have a look at them:

```
poetry run python -m inkai.extract tests/data/ai/*.ai
```

You will find `.txt` files appear next to the `.ai` files.
These files contain the plain document and other extracted elements that might be useful for testing and adding features.

### Open Adobe Illustrator Files With Inkscape

This extension can be installed to use with [Inkscape] as a plugin.
Please read the [plugin] manual to understand how to go about this.

1. Follow the [Development Setup].
2. Link or copy the `extension-ai` directory into Inkscape's extensions directory.     
   If you are under Linux, you should be able to do this:
   ```
   ln -st ~/.config/inkscape/extensions/ extension-ai
   ```
3. Open Inkscape.
4. Go to `File` --> `Open`
5. Click on "All Inkscape Files" to see all file formats that can be opened.
   You should see these among them:
   - `Adobe Illustrator Extension (*.ai)`  
   - `Adobe Illustrator Extension Plain Text Test Data (*.ai.txt)`  
     Both of them run the same code.

6. Now, you can choose a file. I recommend `tests/data/inkscape_logo/inkscape_minimized_ai3.ai` as this is tested to work.

### Settings

There are settings inside of Inkscape that might be of interest:
- `Preferences` → `Interface` → Uncheck `Origin at upper left`  
  This will put `x=0,y=0` at the bottom left.

### Adding Test Data

We use test data to ensure that your AI documents work.
However, there is a lot of data in the AI documents and that is not required.
In order to keep the size of the repository small, we use the extracted data to test with.
To add an AI file that does not work, please follow this process:

1. Follow the [Development Setup].
2. Place the `.ai` file in the `tests/data/ai` directory
3. Rename the file so it ends with `.ink.ai`.
4. Run the script therein:
   ```
   poetry run python tests/data/ai/extract.py
   ```
   - `SUCCESS`: Now, your file should have a `.txt` file appear next to it.
   - `ERROR`: This is definitely important to report!

Please, **name your file** according to the content and the issue it refers to:
```
issue_12_rectangle_with_rotation.ai
```

## Code Style & Commits

For the commit messages, please read
[cbeams - How to Write a Git Commit Message](https://cbea.ms/git-commit/).

After installing the pre-commit hook, all the python files should conform to
[black](https://black.readthedocs.io/en/stable/index.html) automatically.

## Documentation

The implementation follows the [Adobe Illustrator File Format Specification].
But this specification is from 1998.
Thus, many features are not described there or changed.
Please have a look at the [documentation] if you are interested in what has changed.


## Licensing

Each file should be annotated with a license.
We use the [reuse] tool for that.

To annotate a file that you contributed to, please run something like this, replacing the `--copyright="..."` with your name and email.
```
poetry run reuse annotate --year 2023 --copyright="Software Freedom Conservancy <info@sfconservancy.org>" --license="GPL-2.0-or-later" path/to/my.file
```

For files that are binary or auto-generated, use `--force-dot-license`.
```
poetry run reuse annotate --year 2023 --copyright="Software Freedom Conservancy <info@sfconservancy.org>" --license="GPL-2.0-or-later" --force-dot-license path/to/my.file
```

Also look at the [Testing Section] to test the compliance.

[reuse]: https://reuse.software/
[Inkscape]: https://inkscape.org/
[plugin]: https://inkscape.org/gallery/=extension/
[Adobe Illustrator File Format Specification]: http://www.idea2ic.com/File_Formats/Adobe%20Illustrator%20File%20Format.pdf
[documentation]: docs/
